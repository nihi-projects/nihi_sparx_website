<?php

// @codingStandardsIgnoreFile

/**
 * @file
 * Local development override configuration feature.
 *
 * To activate this feature, copy and rename it such that its path plus
 * filename is 'sites/default/settings.local.php'. Then, go to the bottom of
 * 'sites/default/settings.php' and uncomment the commented lines that mention
 * 'settings.local.php'.
 *
 * If you are using a site name in the path, such as 'sites/example.com', copy
 * this file to 'sites/example.com/settings.local.php', and uncomment the lines
 * at the bottom of 'sites/example.com/settings.php'.
 */

/**
 * Assertions.
 *
 * The Drupal project primarily uses runtime assertions to enforce the
 * expectations of the API by failing when incorrect calls are made by code
 * under development.
 *
 * @see http://php.net/assert
 * @see https://www.drupal.org/node/2492225
 *
 * If you are using PHP 7.0 it is strongly recommended that you set
 * zend.assertions=1 in the PHP.ini file (It cannot be changed from .htaccess
 * or runtime) on development machines and to 0 in production.
 *
 * @see https://wiki.php.net/rfc/expectations
 */
assert_options(ASSERT_ACTIVE, TRUE);
\Drupal\Component\Assertion\Handle::register();

/**
 * Enable local development services.
 */
$settings['container_yamls'][] = DRUPAL_ROOT . '/sites/development.services.yml';

/**
 * Show all error messages, with backtrace information.
 *
 * In case the error level could not be fetched from the database, as for
 * example the database connection failed, we rely only on this value.
 */
$config['system.logging']['error_level'] = 'verbose';

/**
 * Disable CSS and JS aggregation.
 */
$config['system.performance']['css']['preprocess'] = FALSE;
$config['system.performance']['js']['preprocess'] = FALSE;

/**
 * Disable the render cache.
 *
 * Note: you should test with the render cache enabled, to ensure the correct
 * cacheability metadata is present. However, in the early stages of
 * development, you may want to disable it.
 *
 * This setting disables the render cache by using the Null cache back-end
 * defined by the development.services.yml file above.
 *
 * Only use this setting once the site has been installed.
 */
# $settings['cache']['bins']['render'] = 'cache.backend.null';

/**
 * Disable caching for migrations.
 *
 * Uncomment the code below to only store migrations in memory and not in the
 * database. This makes it easier to develop custom migrations.
 */
# $settings['cache']['bins']['discovery_migration'] = 'cache.backend.memory';

/**
 * Disable Internal Page Cache.
 *
 * Note: you should test with Internal Page Cache enabled, to ensure the correct
 * cacheability metadata is present. However, in the early stages of
 * development, you may want to disable it.
 *
 * This setting disables the page cache by using the Null cache back-end
 * defined by the development.services.yml file above.
 *
 * Only use this setting once the site has been installed.
 */
# $settings['cache']['bins']['page'] = 'cache.backend.null';

/**
 * Disable Dynamic Page Cache.
 *
 * Note: you should test with Dynamic Page Cache enabled, to ensure the correct
 * cacheability metadata is present (and hence the expected behavior). However,
 * in the early stages of development, you may want to disable it.
 */
# $settings['cache']['bins']['dynamic_page_cache'] = 'cache.backend.null';

/**
 * Allow test modules and themes to be installed.
 *
 * Drupal ignores test modules and themes by default for performance reasons.
 * During development it can be useful to install test extensions for debugging
 * purposes.
 */
# $settings['extension_discovery_scan_tests'] = TRUE;

/**
 * Enable access to rebuild.php.
 *
 * This setting can be enabled to allow Drupal's php and database cached
 * storage to be cleared via the rebuild.php page. Access to this page can also
 * be gained by generating a query string from rebuild_token_calculator.sh and
 * using these parameters in a request to rebuild.php.
 */
$settings['rebuild_access'] = TRUE;

/**
 * Skip file system permissions hardening.
 *
 * The system module will periodically check the permissions of your site's
 * site directory to ensure that it is not writable by the website user. For
 * sites that are managed with a version control system, this can cause problems
 * when files in that directory such as settings.php are updated, because the
 * user pulling in the changes won't have permissions to modify files in the
 * directory.
 */
$settings['skip_permissions_hardening'] = TRUE;

// Required for aes/dbee module installation
// $config_directories['active'] = '/var/www/active';

//$conf['cron_safe_threshold'] = 0;

// error_reporting = E_ALL & ~E_DEPRECATED & ~E_STRICT
error_reporting(E_ALL );
ini_set('display_errors', true);
ini_set('display_startup_errors', true);

// Database setup.
$databases['default']['default'] = array (
  'database' => 'sparxnew',
  'username' => 'homestead',
  'password' => 'secret',
  'prefix' => '',
  'host' => 'localhost',
  'port' => '3306',
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'driver' => 'mysql',
);

/*
 * SPARX specific settings.
 * (Converted to defines in parent).
 */
// TEST
//$sparx_settings['SPARX_CFG_API_KEY'] = '96402cc642f81a985c35cde7e6e35423c4c913a582804383151a1cd8e390fb15d6146440606960b49cecc907871973eb5bfc151911c45e8347d5fbec554de80b';
//$sparx_settings['SPARX_CFG_API_ROOT'] = 'http://testmeany.nihi.auckland.ac.nz:3001/apiv3/';
$sparx_settings['SPARX_CFG_LOG_ALL_TESTING'] = TRUE;
$sparx_settings['SPARX_CFG_EMAIL_TO_OVERRIDE'] = 'mattbodley@gmail.com';
$sparx_settings['SPARX_CFG_SMS_TO_OVERRIDE'] = '0211639313';
$sparx_settings['SPARX_CFG_DISABLE_SMS'] = TRUE;
$sparx_settings['SPARX_CFG_DISABLE_AUTO_EMAIL'] = FALSE;
$sparx_settings['SPARX_CFG_BASE_URL'] = 'http://new.sparx.test';
$sparx_settings['SPARX_CFG_CRON_ENABLE'] = TRUE;
$sparx_settings['SPARX_CFG_ALERT_APP_EMAIL'] = 'mattbodley@gmail.com';
$sparx_settings['SPARX_CFG_DISABLE_GEOLOCATION'] = TRUE;

$settings['update_free_access'] = FALSE;

ini_set('max_execution_time', 0);

$class_loader->addPsr4('Drupal\\webprofiler\\', [ __DIR__ . '/../../modules/devel/webprofiler/src']);
$settings['container_base_class'] = '\Drupal\webprofiler\DependencyInjection\TraceableContainer';
