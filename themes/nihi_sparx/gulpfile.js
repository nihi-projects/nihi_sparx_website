'use strict';

// Require packages
var gulp         = require('gulp');
var sass         = require('gulp-sass');
var concat       = require('gulp-concat');
var uglify       = require('gulp-uglify');
var sourcemaps   = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');

// Concatenate all SCSS files in scss, generate sourcemaps, minify it and output to assets/css/app.min.css
gulp.task('build-css', function() {
  return gulp.src('scss/style.scss')
      .pipe(sourcemaps.init())
      .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
      .pipe(autoprefixer({
        overrideBrowserslist: ['last 2 versions'],
        cascade: false
      }))
      .pipe(sourcemaps.write())
      .pipe(gulp.dest('assets/css'));
});

// Concatenate all Javascript files in js, generate sourcemaps, minify it and output to assets/js/app.min.js
gulp.task('build-js', function() {
  return gulp.src([
    'js/**/*.js'
  ])
      .pipe(sourcemaps.init())
      .pipe(concat('app.min.js'))
      // Only uglify if gulp is ran with '--type production'
      .pipe(uglify())
      .pipe(sourcemaps.write())
      .pipe(gulp.dest('assets/js'));
});

// Define the default task that's run on `gulp`
//gulp.task('default', ['build-css','build-js']);
gulp.task('default', gulp.series('build-css','build-js'));

// Configure which files to watch and what tasks to use on file changes
gulp.task('watch', function() {
  gulp.watch('js/**/*.js', gulp.series('build-js'));
  gulp.watch('scss/**/*.scss', gulp.series('build-css'));
});

