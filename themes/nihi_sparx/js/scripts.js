jQuery("document").ready(function($){

    if($( window ).width() > 991){
        $('.topmenu ul.nav li.dropdown').hover(function() {
            $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeIn(200);
        }, function() {
            $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeOut(200);
        });

        $('.topmenu ul.nav .dropdown-toggle').each(function() {
            $(this).removeAttr('data-target');
            $(this).removeAttr('data-toggle');
            $(this).removeAttr('class');
        });
    }

    $('li.dropdown').on('click touchstart', function() {
        var $el = $(this);
        if ($el.hasClass('open')) {
            var $a = $el.children('a.dropdown-toggle');
            if ($a.length && $a.attr('href')) {
                location.href = $a.attr('href');
            }
        }
    });

    $('.header').removeClass('open');
    $('.overlay').removeClass('shadow');
    $('.mobile_header .menu-btn').click(function(e){
        var e=window.event||e;
        $('.header').addClass('open');
        $('.overlay').addClass('shadow');
        e.stopPropagation();
    });

    $('.header .close').click(function(e){
        $('.header').removeClass('open');
        $('.overlay').removeClass('shadow');
    });

    $(window).resize(function(e) {
        if($(window).width() > 991) {
            $('.header').removeClass('open');
            $('.overlay').removeClass('shadow');
        }
    });

    // Enforce home page reload after successful login for menu refresh
    if(GetURLParameter('login') == 'success'){
        //location.reload();
        window.location = window.location.href.split("?")[0];
    }

    // Check your mood slider
    $('#check-mood').slider();


    // Sticky Menu
    $(window).scroll(function(){

        // console.log($(window).scrollTop());

        if ($(window).scrollTop() >= 160) {
            $('.top-bar').addClass('fixed-header');
            $('.navbar').addClass('fixed-header');
        }
        else {
            $('.top-bar').removeClass('fixed-header');
            $('.navbar').removeClass('fixed-header');
        }
    });

    // Function to get URL Parameter
    function GetURLParameter(sParam)
    {
        var sPageURL = window.location.search.substring(1);
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++)
        {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam)
            {
                return sParameterName[1];
            }
        }
    }​


    // Splash page    
    var w = window.matchMedia("(max-width: 414px)");
    var vid = document.getElementById('opening_video');
    var source = document.createElement("source");
    source.id = "splash_video";
    source.setAttribute("type", "video/mp4");
    vid.appendChild(source);
    if (w.matches) {
        vid.pause();
        source.removeAttribute("src");
        source.setAttribute("src", "/modules/sparx_splash/assets/videos/sparx_splash_square.mp4");
        vid.load();
        vid.play();
    }
    else {
        vid.pause();
        source.removeAttribute("src");
        source.setAttribute("src", "/modules/sparx_splash/assets/videos/sparx_splash.mp4");
        vid.load();
        vid.play();
    }
    vid.addEventListener('ended',myHandler,false);
    function myHandler(e) {
        window.location='/home';
    }

});



