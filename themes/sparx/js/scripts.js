jQuery("document").ready(function ($) {


  if($( window ).width() > 991){
    $('.topmenu ul.nav li.dropdown').hover(function() {
      $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeIn(200);
    }, function() {
      $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeOut(200);
    });

    $('.topmenu ul.nav .dropdown-toggle').each(function() {
      $(this).removeAttr('data-target');
      $(this).removeAttr('data-toggle');
      $(this).removeAttr('class');
    });
  }

  $('li.dropdown').on('click touchstart', function() {
    var $el = $(this);
    if ($el.hasClass('open')) {
      var $a = $el.children('a.dropdown-toggle');
      if ($a.length && $a.attr('href')) {
        location.href = $a.attr('href');
      }
    }
  });

  $('.header').removeClass('open');
  $('.overlay').removeClass('shadow');
  $('.mobile_header .menu-btn').click(function(e){
    var e=window.event||e;
    $('.header').addClass('open');
    $('.overlay').addClass('shadow');
    e.stopPropagation();
  });

  $('.header .close').click(function(e){
    $('.header').removeClass('open');
    $('.overlay').removeClass('shadow');
  });

  $(window).resize(function(e) {
    if($(window).width() > 991) {
      $('.header').removeClass('open');
      $('.overlay').removeClass('shadow');
    }
  });
});

