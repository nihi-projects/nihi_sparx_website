<?php

/**
 * @file
 * Bootstrap sub-theme (SPARX).
 */

/**
 * Function to convert strings from http to https.
 */
function _sparx_http_to_https($str) {
  return str_replace("http://", "https://", $str);
}

/**
 * Implements hook_preprocess_html().
 */
function sparx_preprocess_html(&$variables) {

  // Convert shortlink an canonical links to https.
  if (isset($variables['page']['#attached']['html_head']) && is_array($variables['page']['#attached']['html_head'])) {
    foreach ($variables['page']['#attached']['html_head'] as $html_head_idx => $html_head_val) {
      if (!isset($html_head_val[0]['#tag'])) {
        continue;
      }
      if ($html_head_val[0]['#tag'] == "link") {
        if (!isset($html_head_val[0]['#attributes'])) {
          continue;
        }
        if (!isset($html_head_val[0]['#attributes']['rel']) || !isset($html_head_val[0]['#attributes']['href'])) {
          continue;
        }
        if (!in_array($html_head_val[0]['#attributes']['rel'], [
          'shortlink',
          'canonical',
        ])) {
          continue;
        }
        $variables['page']['#attached']['html_head'][$html_head_idx][0]['#attributes']['href'] = _sparx_http_to_https($html_head_val[0]['#attributes']['href']);
      }
    }
  }

  if (isset($variables['page']['#attached']['http_header']) && is_array($variables['page']['#attached']['http_header'])) {
    // Convert Link headers to https.
    foreach ($variables['page']['#attached']['http_header'] as $html_header_idx => $html_header_val) {
      if (is_array($html_header_val)) {
        foreach ($html_header_val as $html_header_val_idx => $html_header_val_val) {
          if (is_string($html_header_val_val)) {
            $variables['page']['#attached']['http_header'][$html_header_idx][$html_header_val_idx] = _sparx_http_to_https($html_header_val_val);
          }
        }
      }
    }
  }

  // Put description for custom mood-quiz page.
  if ($variables['root_path'] == 'mood-quiz') {
    $moodQuizDescription = [
      '#tag' => 'meta',
      '#attributes' => [
        'name' => 'description',
        'content' => 'Everyone feels sad, stressed, angry or down from time to time. Take our Mood Quiz.',
      ],
    ];

    $variables['page']['#attached']['html_head'][] = [$moodQuizDescription, 'description'];
  }
}
