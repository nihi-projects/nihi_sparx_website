<?php

namespace Drupal\sparx_moodquiz\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\sparx_library\SparxContent;

/**
 * The mooz quiz custom step form.
 */
class MoodQuizForm extends FormBase {

  protected const QUESTION_MAX = 9;
  protected const RESPONSE_MAX = 3;

  protected $renderer;

  protected $sContent;
  protected $questions;
  protected $responses;

  /**
   * Constructs a the mod quiz form.
   *
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   * @param \Drupal\sparx_library\SparxContent $sparxContent
   *   The sparx content service.
   */
  public function __construct(RendererInterface $renderer, SparxContent $sparxContent) {
    $this->renderer = $renderer;
    $this->sContent = $sparxContent;
  }

  /**
   * Creates the services to instantiate this class.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The source of the services.
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('renderer'),
      $container->get('sparx_content_service')
    );
  }

  /**
   * This gets the response number of the database content from the scores.
   *
   * @param int $score
   *   The total score of the user (sum of all responses).
   * @param int $lastscore
   *   The last score of the user (last question response).
   *
   * @return int
   *   The response number to use for the content to display.
   */
  private function getResponseNumFromScores($score, $lastscore) {

    // Note: response numbers 0 and 5 are not currently used.
    if ($lastscore > 0) {
      $responseNum = 1;
    }
    elseif ($score <= 4) {
      $responseNum = 2;
    }
    elseif ($score <= 9) {
      $responseNum = 3;
    }
    elseif ($score <= 14) {
      $responseNum = 4;
    }
    else {
      $responseNum = 6;
    }
    return $responseNum;
  }

  /**
   * Gets the unique form ID for this form.
   *
   * @return string
   *   The Form ID.
   */
  public function getFormId() {
    return 'sparx_moodquiz_form';
  }

  /**
   * Builds the multistep webform.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // The responses (options the user can select from).
    $this->responses = [
      0 => t('Not at all'),
      1 => t('Several days'),
      2 => t('More than half the days'),
      3 => t('Nearly every day'),
    ];

    // List of all the questions we will ask.
    $this->questions = [
      1 => t('Over the last two weeks have you been feeling down, depressed, irritable, or hopeless?'),
      2 => t('Over the last two weeks have you had little interest or pleasure in doing things?'),
      3 => t('Over the last two weeks have you had trouble falling asleep, staying asleep, or have you been sleeping too much?'),
      4 => t('Over the last two weeks have you had poor appetite, weight loss, or have you been overeating?'),
      5 => t('Over the last two weeks have you been feeling tired, or have you had little energy?'),
      6 => t('Over the last two weeks have you been feeling bad about yourself – or feeling that you are a failure, or that you have let yourself or your family down?'),
      7 => t('Over the last two weeks have you had trouble concentrating on things like schoolwork, reading, or watching TV?'),
      8 => t('Over the last two weeks have you been moving or speaking so slowly that other people could have noticed? Or the opposite – being so fidgety or restless that you were moving around a lot more than usual?'),
      9 => t('Over the last two weeks have you had thoughts that you would be better off dead, or of hurting yourself in some way?'),
    ];

    // Get current step/init step.
    $step = $this->verifyStep($form_state->get('step'));
    $form_state->set('step', $step);

    // Get/setup wrapper templates for the form.
    $renderableWrapperStart = [
      '#theme' => 'sparx_wrapper_start',
    ];
    $renderableWrapperEnd = [
      '#theme' => 'sparx_wrapper_end',
    ];
    $form['#prefix'] = $this->renderer->render($renderableWrapperStart);
    $form['#suffix'] = $this->renderer->render($renderableWrapperEnd);

    // Setup form settings.
    $form['#multistep'] = TRUE;
    $form['#redirect'] = FALSE;

    if ($step == 0) {

      // Load initial markup.
      $form['initial_markup'] = ['#markup' => $this->sContent->getMoodQuizResponseMarkup(-1)];

      // Setup link to start.
      $form['start_quiz'] = [
        '#type' => 'submit',
        '#value' => t('START'),
      ];
    }
    else {
      // Cycle through all questions.
      if ($step <= self::QUESTION_MAX) {
        // First question has an introduction.
        if ($step == 1) {
          $renderableIntroduction = [
            '#theme' => 'sparx_introduction',
          ];

          $form['intro'] = [
            '#type' => 'item',
            '#markup' => $this->renderer->render($renderableIntroduction),
          ];
        }

        // Create progress bar.
        $renderableProgressBar = [
          '#theme' => 'sparx_progress_bar',
          '#sparx_current_question' => $step,
          '#sparx_start_num' => 1,
          '#sparx_end_num' => self::QUESTION_MAX,
        ];
        $form['progressbar'] = [
          '#type' => 'item',
          '#markup' => $this->renderer->render($renderableProgressBar),
        ];

        // Create responses.
        $form['q' . $step] = [
          '#type' => 'radios',
          '#title' => $this->questions[$step],
          '#options' => $this->responses,
        ];

        // If we are at the end, use submit to complete, otherwise next.
        if ($step == self::QUESTION_MAX) {
          $button_name = 'submit';
          $button_label = t("SUBMIT >");
        }
        else {
          $button_name = 'next';
          $button_label = t("NEXT QUESTION >");
        }
        $form[$button_name] = [
          '#type' => 'submit',
          '#value' => $button_label,
        ];

        // Setup footer.
        $renderableQuestionFooter = [
          '#theme' => 'sparx_questions_footer',
        ];
        $form['final_markup'] = [
          '#type' => 'item',
          '#markup' => $this->renderer->render($renderableQuestionFooter),
        ];
      }
      else {

        // Add up all responses to find the total score.
        $score = 0;
        for ($questionCnt = 1; $questionCnt <= self::QUESTION_MAX; $questionCnt++) {
          $score += $form_state->get('q' . $questionCnt);
        }

        // Get the response for the last question.
        $lastscore = $form_state->get('q9');

        // Use total score and last response score to get the content to show.
        $responseNum = $this->getResponseNumFromScores($score, $lastscore);

        // Setup result header.
        $renderableResultHeader = [
          '#theme' => 'sparx_result_header',
          '#sparx_result_markup' => $this->sContent->getMoodQuizResponseMarkup($responseNum),
        ];
        $form['result'] = [
          '#type' => 'item',
          '#markup' => $this->renderer->render($renderableResultHeader),
        ];

        // Load the results for each question.
        $results = [];
        for ($resultCnt = 1; $resultCnt <= self::QUESTION_MAX; $resultCnt++) {
          $results[$resultCnt] = $this->responses[$form_state->get('q' . $resultCnt)];
        }

        // Setup results table.
        $renderableResultTable = [
          '#theme' => 'sparx_result_table',
          '#sparx_start_num' => 1,
          '#sparx_end_num' => self::QUESTION_MAX,
          '#sparx_questions' => $this->questions,
          '#sparx_responses' => $results,
          '#sparx_max_response_num' => self::RESPONSE_MAX,
          '#sparx_score' => $score,
        ];
        $form['result_table'] = [
          '#type' => 'item',
          '#markup' => $this->renderer->render($renderableResultTable),
        ];
      }
    }

    return $form;
  }

  /**
   * Inits/verifies the step to ensure within bounds.
   *
   * @param int $step
   *   The current proposed step.
   *
   * @return int
   *   The step initialized or restricted.
   */
  private function verifyStep($step) {

    if (is_null($step)) {
      return 0;
    }
    if (!is_numeric($step)) {
      return 0;
    }
    if ($step < 0) {
      return 0;
    }
    if ($step > (self::QUESTION_MAX + 1)) {
      return 0;
    }

    return $step;
  }

  /**
   * Verifies the response a user selected is valid.
   *
   * @param int $response
   *   The response selected by the user.
   *
   * @return int
   *   The verified response (or an empty string).
   */
  private function verifyResponse($response) {

    if (strlen($response) === 0) {
      return "";
    }

    if (!is_numeric($response)) {
      return "";
    }
    if ($response < 0) {
      return "";
    }
    if ($response > self::RESPONSE_MAX) {
      return "";
    }

    return $response;
  }

  /**
   * Validates the form step to make sure value is selected.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    // Get validated step.
    $step = $this->verifyStep($form_state->get('step'));

    // If we are not at the beginning we must have a value.
    if ($step > 0 && strlen($this->verifyResponse($form_state->getValue('q' . $step))) === 0) {
      $form_state->setErrorByName('q' . $step, t('Please select a value.'));
    }
  }

  /**
   * Submits the form and moves to the next step (or finish).
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Get validated step.
    $step = $this->verifyStep($form_state->get('step'));

    // If not at the beginning save the current response.
    if ($step > 0) {
      $form_state->set('q' . $step, $this->verifyResponse($form_state->getValue('q' . $step)));
    }

    // Increase the step to the next one (or finish) and reload.
    $form_state->set('step', $step + 1);
    $form_state->setRebuild(TRUE);
  }

}
