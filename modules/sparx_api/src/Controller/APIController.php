<?php

namespace Drupal\sparx_api\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Serialization\Json;
use Drupal\user\UserAuth;

use Drupal\sparx_library\SparxAPI;
use Drupal\sparx_library\SparxUser;
use Drupal\sparx_library\SparxValidation;
use Drupal\sparx_library\SparxAudit;

use Drupal\sparx_library\Traits\SparxAuditTrait;
use Drupal\sparx_library\Traits\SparxSettingsTrait;

use Drupal\smart_ip\SmartIp;
use Drupal\smart_ip\SmartIpLocation;
use Symfony\Component\HttpFoundation\IpUtils;

/**
 * Controller to allow the app to login and register users.
 */
class APIController extends ControllerBase {

  use SparxAuditTrait;
  use SparxSettingsTrait;

  protected $request;
  protected $userAuth;
  protected $sUser;
  protected $params;
  protected $sValid;
  protected $sAudit;

  protected $responseEncoding;

  /**
   * Constructs a new APIController object.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request
   *   An instance of Symfony\Component\HttpFoundation\RequestStack.
   * @param \Drupal\user\UserAuth $userAuth
   *   The authentication service to use.
   * @param \Drupal\sparx_library\SparxUser $sparxUser
   *   The user data service to represent the user model.
   * @param \Drupal\sparx_library\SparxValidation $sparxValidation
   *   The validation service to use for the data.
   * @param \Drupal\sparx_library\SparxAudit $sparxAudit
   *   The logging service to use.
   */
  public function __construct(RequestStack $request, UserAuth $userAuth, SparxUser $sparxUser, SparxValidation $sparxValidation, SparxAudit $sparxAudit) {

    // Set Drupal services.
    $this->request = $request->getCurrentRequest();
    $this->userAuth = $userAuth;
    $this->params = $this->request->request;

    // Set Sparx services.
    $this->sUser = $sparxUser;
    $this->sValid = $sparxValidation;
    $this->sAudit = $sparxAudit;
  }

  /**
   * Creates the static version of this object.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container.
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this  class.
    return new static(
    // Load the service required to construct this class.
      $container->get('request_stack'),
      $container->get('user.auth'),
      $container->get('sparx_user_service'),
      $container->get('sparx_validation_service'),
      $container->get('sparx_audit_service')
    );
  }

  /**
   * Test function to test thing on the server easily.
   */
  public function test() {

    return new Response();
  }

  /**
   * Checks to see if JSON is detected and uses these over HTTP POST.
   */
  private function checkJson() {

    $allHeaders = $this->request->headers->all();
    $this->aLogMessageJson(SA_PUBLIC_API, t('All headers data follows:'), $allHeaders);

    if (strpos($this->request->headers->get('Content-Type'), 'application/json') === 0) {
      $this->aLog(SA_PUBLIC_API, t('Detected application/json header'));
      $data = Json::decode($this->request->getContent());
      $this->request->request->replace(is_array($data) ? $data : []);
    }
    else {
      $this->aLog(SA_PUBLIC_API, t('Did not detect application/json, defaulting to form-data'));
    }
  }

  /**
   * Sets the response header (JSON).
   *
   * @param \Symfony\Component\HttpFoundation\Response $response
   *   The HTTP response object.
   */
  private function setResponseContentType(Response &$response) {

    $response->headers->set('Content-Type', 'application/json');
  }

  /**
   * Checks to ensure that the required fields are set in submission.
   *
   * @param array $requiredArr
   *   The array with the required keys.
   * @param array $submitKeys
   *   The keys submitted by the client.
   *
   * @return mixed
   *   FALSE if no error, otherwise the error string to send back to the client.
   */
  private function getRequiredError(array $requiredArr, array $submitKeys) {

    $errorStr = "";
    foreach ($requiredArr as $required) {
      if (!in_array($required, $submitKeys)) {
        $errorStr = t("Missing required argument @argument", ['@argument' => $required]);
        break;
      }
    }
    if (strlen($errorStr)) {
      return $errorStr;
    }

    return FALSE;
  }

  /**
   * Returns a successful response (200) back to the client.
   *
   * @param \Symfony\Component\HttpFoundation\Response $response
   *   The response object.
   * @param string $successStr
   *   The string to send back.
   */
  private function responseSuccess(
    Response &$response,
    $successStr) {

    return $this->responseCommon($response, $successStr, Response::HTTP_OK, NULL);
  }

  /**
   * Returns an error back to the client.
   *
   * @param \Symfony\Component\HttpFoundation\Response $response
   *   The response object.
   * @param string $errorStr
   *   The error string to send back.
   * @param int $errorCode
   *   The error code to send back (default is 400).
   * @param string $errorStrHeader
   *   This is the header override to send back, for backwards compatibility
   *   (as some errors have special characters). Defaults to NULL.
   * @param string $sensitiveStrOverride
   *   This is the text to log in case of sensitive information.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The updated response object to return to the client.
   */
  private function responseError(
    Response &$response,
    $errorStr,
    $errorCode = Response::HTTP_BAD_REQUEST,
    $errorStrHeader = NULL,
    $sensitiveStrOverride = NULL) {
    $this->aLog(SA_PUBLIC_API, (!is_null($sensitiveStrOverride) ? $sensitiveStrOverride : $errorStr) . ' (' . $errorCode . ')');
    return $this->responseCommon($response, $errorStr, $errorCode, $errorStrHeader);
  }

  /**
   * Returns the response, used by success and error types.
   *
   * @param \Symfony\Component\HttpFoundation\Response $response
   *   The response object.
   * @param string $str
   *   The string to display (should be encoded for raw output).
   * @param int $code
   *   The response code number.
   * @param string $strHeader
   *   The error code to send back override as defaults to $str.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The updated response object to return to the client.
   */
  private function responseCommon(
    Response &$response,
    $str,
    $code,
  $strHeader) {

    // This fixes a bug in the app with error display.
    $response->setContent($str);
    $response->setStatusCode($code, $code !== Response::HTTP_OK ? (": " . (!is_null($strHeader) ? $strHeader : $str)) : NULL);

    return $response;
  }

  /**
   * Logs the paramaters for debugging.
   *
   * This checks the global flag (as shouldn't do this on the prod server).
   * Removes sensitive information from the log if necessary.
   */
  private function logDebugParams() {

    $allParams = $this->params->all();

    if (!$this->isLogAllTestingFlag()) {
      if (isset($allParams['password'])) {
        unset($allParams['password']);
      }
      if (isset($allParams['email'])) {
        unset($allParams['email']);
      }
      if (isset($allParams['mobile'])) {
        unset($allParams['mobile']);
      }
    }

    $this->aLogJson(SA_PUBLIC_API, $allParams);
  }

  /**
   * Endpoint to generate a token (session) for users.
   */
  public function token() {

    $this->aLog(SA_PUBLIC_API, t('Received token request'));

    // Ensure we are in a clean session.
    if (!\Drupal::currentUser()->isAnonymous()) {
      user_logout();
    }

    // Array of "required" keys as per old API.
    $requiredArr = ['username', 'password'];

    // Create a new response to send back.
    $response = new Response();

    // Load JSON if we have this header.
    $this->checkJson();
    $this->logDebugParams();

    $this->setResponseContentType($response);

    // Check required variables are set.
    $requiredError = $this->getRequiredError($requiredArr, $this->params->keys());
    if ($requiredError !== FALSE) {
      // 401 error.
      return $this->responseError($response, $requiredError, Response::HTTP_UNAUTHORIZED);
    }

    // Simple username and password auth.
    $username = $this->params->get('username');
    $password = $this->params->get('password');

    // Wrap in try/catch for any random errors.
    try {
      $this->aSetUsername($username);
      $this->aLog(SA_PUBLIC_API, t('Authenticating'));

      // Check via Drupal if credentials are OK.
      $userId = $this->userAuth->authenticate($username, $password);
      if (!$userId) {
        // 401 error.
        return $this->responseError($response, t('User login unsuccessful'), Response::HTTP_UNAUTHORIZED);
      }
      $this->aSetUserId($userId);

      $user = $this->sUser->getById($userId);
      if (is_null($user->getId())) {
        $this->aLog(SA_PUBLIC_API, t('Could not load user'));
        return $this->responseError($response, t('User login unsuccessful'), Response::HTTP_UNAUTHORIZED);
      }
      else {
        $this->aLog(SA_PUBLIC_API, t('Loaded user'));
      }

      // Attempt to get the token to start the game.
      $token = sparxAPI::getUserToken($this->getGameUsernameWithOverride($user->getUsername()));
      if (!$token) {
        // 403 error.
        return $this->responseError($response, t('Unable to get token'), Response::HTTP_FORBIDDEN);
      }

      // Successfully created token, return with URL containing token.
      $this->aLog(SA_PUBLIC_API, t('Sending token to client'));
      return $this->responseSuccess($response, $this->getTokenUrl($token));

      // Catch any exceptions bubbling up.
    }
    catch (\Exception $e) {

      // Save for analysis.
      watchdog_exception('sparx_api_apicontroller_token', $e);

      // Have a clean error for the user.
      return $this->responseError($response, t('User login unsuccessful'), Response::HTTP_UNAUTHORIZED);
    }
  }

  /**
   * Gets the full URL for the back end API with token.
   *
   * @param string $token
   *   The token to incorporate into the URL.
   *
   * @return string
   *   The URL of the full API with token.
   */
  private function getTokenUrl($token) {
    return sparxAPI::getGameApiRoot() . 'config?sid=' . $token;
  }

  /**
   * Endpoint to register a new user.
   */
  public function register() {

    $this->aLog(SA_PUBLIC_API, t('Received register request'));

    // Ensure we have a clean session.
    if (!\Drupal::currentUser()->isAnonymous()) {
      user_logout();
    }

    // Creates the response object to send back to the client.
    $response = new Response();

    // Load JSON if we have this header.
    $this->checkJson();
    $this->logDebugParams();

    $this->setResponseContentType($response);

    // Check to see if we are allowed for that IP address, error if not.
    if ($this->sValid->isIpDisabled()) {

      $this->aLog(SA_PUBLIC_API, t('IP address not allowed'));

      return $this->responseError($response, t('Please register on the website'));
    }

    // Array of "required" keys as per old API.
    $requiredArr = [
      'username',
      'password',
      'email',
      'emailallow',
      'whoareyou',
      'region',
      'findout',
      'research',
    ];

    // Check required variables are set.
    $requiredError = $this->getRequiredError($requiredArr, $this->params->keys());
    if ($requiredError !== FALSE) {
      // 401 error.
      return $this->responseError($response, $requiredError, Response::HTTP_UNAUTHORIZED);
    }

    // Wrap entire process in try/catch in case other error.
    try {

      // Required fields.
      $params = [
        'username' => trim($this->params->get('username')),
        'whoAreYou' => $this->params->get('whoareyou'),
        'password' => trim($this->params->get('password')),
        'email' => trim($this->params->get('email')),
        'emailAllow' => $this->params->get('emailallow'),
        'region' => $this->params->get('region'),
        'findOut' => trim($this->params->get('findout')),
        'research' => trim($this->params->get('research')),
        'whoAreYouOther' => trim($this->params->get('whoareyouother', '')),
        'mobile' => trim($this->params->get('mobile', '')),
        'findOutOther' => trim($this->params->get('findoutother', '')),
      ];

      // "Optional" fields setup and defaults (clear if not valid).
      if ($this->sValid->isYoungPerson($params['whoAreYou'])) {
        $params['age'] = trim($this->params->get('age', ''));
        $ethnicityRaw = $this->params->get('ethnicity', []);
        $params['ethnicity'] = [];
        // Fixes bug with spelling in the app.
        foreach ($ethnicityRaw as $ethnicityRawItem) {
          if ($ethnicityRawItem == "chinesse") {
            $params['ethnicity'][] = "chinese";
          }
          else {
            $params['ethnicity'][] = $ethnicityRawItem;
          }
        }

        $params['ethnicityOther'] = trim($this->params->get('ethnicityother', ''));
        $params['gender'] = $this->params->get('gender', '');
      }
      else {
        $params['age'] = NULL;
        $params['ethnicityOther'] = $params['gender'] = '';
        $params['ethnicity'] = [];
      }

      // Validate username.
      $usernameError = $this->sValid->getValidateUsernameError($params['username'], TRUE);
      if ($usernameError) {
        // Htmlentities for backwards compatibility and fixes.
        return $this->responseError($response,
          htmlentities(strip_tags($usernameError), ENT_NOQUOTES),
          Response::HTTP_BAD_REQUEST,
          strip_tags($usernameError));
      }

      // Validate username is unique.
      if ($this->sValid->isUsernameTaken($params['username'])) {
        return $this->responseError($response, t('Username already in use'));
      }

      // Validate whoAreYou (has data).
      if ($this->sValid->noLength($params['whoAreYou'])) {
        return $this->responseError($response, t('Missing Who are you value'));
      }

      // Validate whoAreYou value.
      if (!$this->sValid->validateWhoAreYou($params['whoAreYou'])) {
        $this->aLogJson(SA_PUBLIC_API, $params['whoAreYou'], 'whoAreYou');
        return $this->responseError($response, t('Invalid Who are you value'));
      }

      // Process different fields if is a young person.
      if ($this->sValid->isYoungPerson($params['whoAreYou'])) {
        // Validate age.
        if ($this->sValid->noLength($params['age']) || !$this->sValid->validateAge($params['age'])) {
          $this->aLogJson(SA_PUBLIC_API, $params['age'], 'age');
          return $this->responseError($response, t('Missing or invalid age'));
        }
        // Validate ethnicity is the right type and has data.
        if ($this->sValid->notArray($params['ethnicity']) || $this->sValid->arrayEmpty($params['ethnicity'])) {
          return $this->responseError($response, t('Missing ethnicity'));
        }
        // Validate ethnicity is actually a valid textual type.
        if (!$this->sValid->validateEthnicity($params['ethnicity'], TRUE)) {
          $this->aLogJson(SA_PUBLIC_API, $params['ethnicity'], 'ethnicity');
          return $this->responseError($response, t('Invalid ethnicity'));
        }
        // Validate gender is set.
        if ($this->sValid->noLength($params['gender'])) {
          return $this->responseError($response, t('Missing gender'));
        }
        // Validate gener value is valid textual type.
        if (!$this->sValid->validateGender($params['gender'])) {
          $this->aLogJson(SA_PUBLIC_API, $params['gender'], 'gender');
          return $this->responseError($response, t('Invalid gender value'));
        }
      }

      // Validate email, must be set.
      if ($this->sValid->noLength($params['email'])) {
        return $this->responseError($response, t('Missing email address'));
      }

      // Validate email is correct.
      if (!$this->sValid->validateEmail($params['email'])) {
        if ($this->isLogAllTestingFlag()) {
          // Sensitive data (email).
          $this->aLogJson(SA_PUBLIC_API, $params['email'], 'email');
        }
        $errorStr = htmlentities(strip_tags($this->sValid->invalidEmailCompatibilityStr($params['email'])), ENT_NOQUOTES);
        // Htmlentities for backwards compatibility and fixes.
        return $this->responseError($response,
          $errorStr,
          Response::HTTP_BAD_REQUEST,
          strip_tags($this->sValid->invalidEmailCompatibilityStr($params['email'])),
          $this->isLogAllTestingFlag() ? NULL : str_replace(htmlentities($params['email'], ENT_NOQUOTES), t('[HIDDEN_EMAIL]'), $errorStr));
      }

      // Validate email is not already taken.
      if ($this->sValid->isEmailTaken($params['email'])) {
        return $this->responseError($response, t('Email already in use'));
      }

      // Uses different than normal website register function.
      if (!$this->sValid->validateApiEmailAllow($params['emailAllow'])) {
        $this->aLogJson(SA_PUBLIC_API, $params['emailAllow'], 'emailAllow');
        return $this->responseError($response, t('Invalid value for email allow'));
      }

      // Ensure password is set to something.
      if ($this->sValid->noLength($params['password'])) {
        return $this->responseError($response, t('Missing password'));
      }

      // Different from the normal website register as includes the prefix.
      if ($this->sValid->hasLength($params['mobile'])) {
        // Check mobile correct characters (numbers).
        if (!$this->sValid->validateMobile($params['mobile'])) {
          if ($this->isLogAllTestingFlag()) {
            $this->aLogJson(SA_PUBLIC_API, $params['mobile'], 'mobile (1)');
          }
          return $this->responseError($response, t('Invalid mobile'));
        }
        // Check to make sure not less than just the prefix selection.
        if (strlen($params['mobile']) < $this->sValid->getMobilePrefixLength()) {
          if ($this->isLogAllTestingFlag()) {
            $this->aLogJson(SA_PUBLIC_API, $params['mobile'], 'mobile (2)');
          }
          return $this->responseError($response, t('Invalid mobile'));
        }
        else {
          // More than just prefix so check prefix to make sure valid.
          $mobilePrefix = substr($params['mobile'], 0, $this->sValid->getMobilePrefixLength());
          if (!$this->sValid->validateMobilePrefix($mobilePrefix)) {
            if ($this->isLogAllTestingFlag()) {
              $this->aLogJson(SA_PUBLIC_API, $params['mobile'], 'mobile (3)');
            }
            return $this->responseError($response, t('Invalid mobile'));
          }
        }
      }

      // Validate region has a value.
      if ($this->sValid->noLength($params['region'])) {
        return $this->responseError($response, t('Missing region'));
      }

      // Validate region is within bounds.
      if (!$this->sValid->validateRegion($params['region'])) {
        $this->aLogJson(SA_PUBLIC_API, $params['region'], 'region');
        return $this->responseError($response, t('Invalid region'));
      }

      // Validate findOut has a value.
      if ($this->sValid->noLength($params['findOut'])) {
        return $this->responseError($response, t('Missing value for how did you find out about sparx'));
      }

      // Validate findOut value is within bounds.
      if (!$this->sValid->validateFindOut($params['findOut'])) {
        $this->aLogJson(SA_PUBLIC_API, $params['findOut'], 'findOut');
        return $this->responseError($response, t('Invalid value for how did you find out about sparx'));
      }

      // Clear find out other to blank if not selected.
      if (!$this->sValid->isFindOutOther($params['findOut'])) {
        $params['findOutOther'] = '';
      }

      // Validate contact permission is correct value.
      if (!$this->sValid->validateApiContactPermission($params['research'])) {
        $this->aLogJson(SA_PUBLIC_API, $params['research'], 'research');
        return $this->responseError($response, t('Invalid value for can we contact you for research'));
      }

      $this->aLog(SA_PUBLIC_API, t('Saving user'));

      // Create user.
      $sUser = $this->sUser->getNew();
      $sUser->setUsername($params['username']);
      $sUser->setPassword($params['password']);
      $sUser->setEmail($params['email']);
      $sUser->setWhoAreYou($params['whoAreYou']);
      $sUser->setWhoAreYouOther($params['whoAreYouOther']);
      $sUser->setGender($params['gender']);
      $sUser->setEmailAllow($this->sValid->isApiEmailAllow($params['emailAllow']) ? TRUE : FALSE);
      $sUser->setTxtAllow((strlen($params['mobile']) > $this->sValid->getMobilePrefixLength()) ? TRUE : FALSE);
      $sUser->setMobile((strlen($params['mobile']) > $this->sValid->getMobilePrefixLength()) ? $params['mobile'] : '');
      $sUser->setWhereDoYouLive($params['region']);
      $sUser->setHowDidYouFindOutAboutSparx($params['findOut']);
      $sUser->setHowDidYouFindOutOther($params['findOutOther']);
      $sUser->setContactPermission($this->sValid->isApiContactPermission($params['research']) ? TRUE : FALSE);
      $sUser->setRegSource(SPARX_USER_MODEL_REG_SOURCE_API);
      $sUser->setEthnicGroupArrStr($params['ethnicity'], TRUE);
      $sUser->setEthnicGroupOther($params['ethnicityOther']);
      $sUser->setAge($params['age']);
      $sUser->setRegUserAgent($this->request->headers->get('User-agent'));

      $saveResult = $sUser->save();

      if ($saveResult !== TRUE) {
        if (strcmp((string) $saveResult, SparxAPI::REGISTER_NAMETAKEN) === 0) {
          return $this->responseError($response, t('Username already in use'));
        }
        elseif (strcmp((string) $saveResult, SparxAPI::REGISTER_FAILURE) === 0) {
          return $this->responseError($response, t('Unable to create SPARX user'));
        }
        else {
          return $this->responseError($response, t('Unable to create user'));
        }
      }

      $this->aSetUserId($sUser->getId());
      $this->aLog(SA_PUBLIC_API, t('User successfully registered, sending welcome email'));

      $this->sendWelcomeEmail($sUser->getUsername(), $sUser->getEmail());

      // Attempt to get the token to start the game.
      $token = sparxAPI::getUserToken($params['username']);
      if (!$token) {
        // 403 error.
        return $this->responseError($response, t('Unable to get token'), Response::HTTP_FORBIDDEN);
      }

      $this->aLog(SA_PUBLIC_API, t('Sending token to client'));

      // Successfully created token, return with URL containing token.
      return $this->responseSuccess($response, $this->getTokenUrl($token));

      // Return clean error to client if any.
    }
    catch (\Exception $e) {

      // Save for analysis.
      watchdog_exception('sparx_api_apicontroller_register', $e);

      // Have a clean error for the user.
      return $this->responseError($response, t('Unable to create user'));
    }
  }

}
