<?php

namespace Drupal\sparx_register_profile\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {

    // This is disabled for now, saved to be enabled later if necessary.
    /*
    // Edit user - disable Drupal standard part.
    if ($route = $collection->get('entity.user.edit_form')) {
    $route->setRequirement('_permission', 'administer users');
    }

    // View user - disable Drupal standard part.
    if ($route = $collection->get('entity.user.canonical')) {
    $route->setRequirement('_permission', 'administer users');
    } */
  }

}
