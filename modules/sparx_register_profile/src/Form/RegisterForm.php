<?php

namespace Drupal\sparx_register_profile\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Access\AccessResult;

use Drupal\smart_ip\SmartIpLocation;

use Drupal\sparx_library\SparxUser;
use Drupal\sparx_library\SparxValidation;
use Drupal\sparx_library\SparxAPI;
use Drupal\sparx_library\SparxAudit;

use Drupal\sparx_library\Traits\SparxAuditTrait;
use Drupal\sparx_library\Traits\SparxSettingsTrait;

/**
 * Implements the register form.
 */
class RegisterForm extends FormBase {

  use SparxAuditTrait;
  use SparxSettingsTrait;

  protected $renderer;
  protected $currentUser;
  protected $sValid;
  protected $sUser;
  protected $smartIpLocation;
  protected $sAudit;

  /**
   * Constructs a renderer service.
   *
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user service.
   * @param \Drupal\sparx_library\SparxValidation $sparxValidation
   *   Sparx validation service.
   * @param \Drupal\sparx_library\SparxUser $sparxUser
   *   Sparx user service.
   * @param \Drupal\smart_ip\SmartIpLocation $smartIpLocation
   *   The GEO location service.
   * @param \Drupal\sparx_library\SparxAudit $sparxAudit
   *   Sparx audit service.
   */
  public function __construct(RendererInterface $renderer, AccountProxyInterface $current_user, SparxValidation $sparxValidation, SparxUser $sparxUser, SmartIpLocation $smartIpLocation, SparxAudit $sparxAudit) {
    $this->renderer = $renderer;
    $this->currentUser = $current_user;
    $this->sValid = $sparxValidation;
    $this->sUser = $sparxUser;
    $this->smartIpLocation = $smartIpLocation;
    $this->sAudit = $sparxAudit;
  }

  /**
   * Creates this class.
   *
   * @return mixed
   *   Container for this class.
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('renderer'),
      $container->get('current_user'),
      $container->get('sparx_validation_service'),
      $container->get('sparx_user_service'),
      $container->get('smart_ip.smart_ip_location'),
      $container->get('sparx_audit_service')
    );
  }

  /**
   * Gets the unique form ID of this form.
   *
   * @return string
   *   The form ID.
   */
  public function getFormId() {

    return 'sparx_register_profile_register_form';
  }

  /**
   * Checks access for this form.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   Whether allowed or forbidden access.
   */
  public function access() {

    if (!$this->currentUser->isAnonymous()) {
      // Return 403 Access Denied page.
      return AccessResult::forbidden();
    }
    return AccessResult::allowed();
  }

  /**
   * Checks to see if the form is disabled geolocation-wise.
   *
   * @return bool
   *   Whether or not the form should be disabled.
   */
  private function isDisabled() {

    return $this->sValid->isIpDisabled();
  }

  /**
   * Builds the register form.
   *
   * @param array $form
   *   The Drupal form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The Drupal form state.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Show disabled form message if necessary.
    if ($this->isDisabled()) {
      $this->aLog(SA_REGISTER, "Building form in disabled mode");
      // Get message and put in error message.
      $renderableOnlyAvailableWithinNZ = [
        '#theme' => 'sparx_only_available_within_new_zealand',
      ];
      drupal_set_message($this->renderer->render($renderableOnlyAvailableWithinNZ), "error");
    }

    // Setup Initial markup.
    $form['initial_markup'] = [
      '#type' => 'item',
      '#theme' => 'sparx_sign_up_text',
    ];

    // Username.
    $renderableUsername = [
      '#theme' => 'sparx_form_commented_element',
      '#sparx_fieldname' => t('Username *'),
      '#sparx_description' => t('We recommend you do not use your real name.'),
    ];
    $form['username'] = [
      '#type' => 'textfield',
      '#title' => $this->renderer->render($renderableUsername),
      '#attributes' => ['placeholder' => t('Type your username here...')],
      '#size' => 40,
      '#maxlength' => 60,
      '#disabled' => $this->isDisabled(),
    ];

    // Who are you.
    $renderableWhoAreYou = [
      '#theme' => 'sparx_form_commented_element',
      '#sparx_fieldname' => t('Who are you? *'),
      '#sparx_description' => t('While it has been proven that SPARX benefits young people aged 12 to 19 years, anybody can use it.'),
    ];
    $form['who_are_you'] = [
      '#type' => 'select',
      '#title' => $this->renderer->render($renderableWhoAreYou),
      '#options' => [
        'youngperson' => t('Young person'),
        'familymember' => t('Family member'),
        'healthprofessional' => t('Health professional'),
        'other' => t('Other'),
      ],
      '#empty_value' => '',
      '#empty_option' => t('Please select'),
      '#disabled' => $this->isDisabled(),
    ];

    // Who are you (Other).
    $form['who_are_you_other'] = [
      '#type' => 'textfield',
      '#title' => t('Who are you? (Other) *'),
      '#size' => 40,
      '#maxlength' => 60,
      '#states' => [
        'visible' => [
          ':input[name="who_are_you"]' => ['value' => 'other'],
        ],
      ],
      '#disabled' => $this->isDisabled(),
    ];

    // Age.
    $form['age'] = [
      '#type' => 'textfield',
      '#title' => t('How old are you? *'),
      '#attributes' => [
        'placeholder' => t('Type your age here...'),
      ],
      '#size' => 40,
      '#maxlength' => 3,
      '#states' => [
        'visible' => [
          ':input[name="who_are_you"]' => [
            'value' => 'youngperson',
          ],
        ],
      ],
      '#disabled' => $this->isDisabled(),
    ];

    // Ethnic group.
    $form['ethnic_group'] = [
      '#type' => 'checkboxes',
      '#title' => t('What is your main ethnic group? *'),
      '#options' => [
        'nzeuropean' => t('NZ European'),
        'maori' => t('Māori'),
        'samoan' => t('Samoan'),
        'cookislandmaori' => t('Cook Island Māori'),
        'tongan' => t('Tongan'),
        'niuean' => t('Niuean'),
        'chinese' => t('Chinese'),
        'indian' => t('Indian'),
        'other' => t('Other'),
      ],
      '#multiple' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="who_are_you"]' => ['value' => 'youngperson'],
        ],
      ],
      '#disabled' => $this->isDisabled(),
    ];

    // Ethnic group (Other).
    $form['ethnic_group_other'] = [
      '#type' => 'textfield',
      '#title' => t('What is your main ethnic group? (Other) *'),
      '#size' => 40,
      '#maxlength' => 60,
      '#states' => [
        'visible' => [
          ':input[name="who_are_you"]' => ['value' => 'youngperson'],
          ':input[name="ethnic_group[other]"]' => ['checked' => TRUE],
        ],
      ],
      '#disabled' => $this->isDisabled(),
    ];

    // Gender.
    $form['gender'] = [
      '#type' => 'select',
      '#title' => t('Gender *'),
      '#options' => [
        'male' => t('Male'),
        'female' => t('Female'),
        'transgender' => t('Transgender'),
        'intersex' => t('Intersex'),
        'undisclosed' => t('Undisclosed'),
      ],
      '#empty_value' => '',
      '#empty_option' => t('Please select'),
      '#states' => [
        'visible' => [
          ':input[name="who_are_you"]' => [
            'value' => 'youngperson',
          ],
        ],
      ],
      '#disabled' => $this->isDisabled(),
    ];

    // Email.
    $form['email'] = [
      '#type' => 'textfield',
      '#title' => t('Email address *'),
      '#attributes' => ['placeholder' => t('Type your email here...')],
      '#size' => 40,
      '#maxlength' => 255,
      '#disabled' => $this->isDisabled(),
    ];
    $form['retype_email'] = [
      '#type' => 'textfield',
      '#title' => t('Retype email'),
      '#title_display' => 'invisible',
      '#attributes' => ['placeholder' => t('Retype your email here...')],
      '#size' => 40,
      '#maxlength' => 255,
      '#disabled' => $this->isDisabled(),
    ];

    // Email allow.
    $form['email_allow'] = [
      '#type' => 'checkbox',
      '#title' => t('We may email you information on where to get help and SPARX reminders every now and then. Unclick if you do not want to get these emails. You will still receive an initial welcome email.'),
      '#default_value' => 1,
      '#disabled' => $this->isDisabled(),
    ];

    // Password.
    $form['pass'] = [
      '#type' => 'password_confirm',
      '#title' => t('Password'),
      '#title_display' => 'invisible',
      '#attributes' => ['placeholder' => t('Type your password here...')],
      '#size' => 12,
      '#disabled' => $this->isDisabled(),
    ];

    // Mobile.
    $form['mobile_markup'] = [
      '#type' => 'item',
      '#theme' => 'sparx_mobile_markup_text',
    ];
    /*    $form['mobile_prefix'] = [
    '#type' => 'select',
    '#title' => t('Mobile prefix'),
    '#title_display' => 'invisible',
    '#options' => [
    '020' => t('020'),
    '021' => t('021'),
    '022' => t('022'),
    '027' => t('027'),
    ],
    '#disabled' => $this->isDisabled(),
    ]; */
    $form['mobile'] = [
      '#type' => 'textfield',
      '#title' => t('Mobile'),
      '#title_display' => 'invisible',
      '#attributes' => ['placeholder' => t('Type your mobile number here...')],
      '#size' => 20,
      '#maxlength' => 20,
      '#disabled' => $this->isDisabled(),
    ];

    // Where do you live (region).
    $form['where_do_you_live'] = [
      '#type' => 'select',
      '#title' => 'Where do you live? *',
      '#options' => [
        'northland' => t('Northland'),
        'auckland' => t('Auckland'),
        'waikato' => t('Waikato'),
        'bay_of_plenty' => t('Bay of Plenty'),
        'gisborne' => t('Gisborne'),
        'hawkes_bay' => t("Hawke's Bay"),
        'taranaki' => t('Taranaki'),
        'manawatu_wanganui' => t('Manawatu-Wanganui'),
        'wellington' => t('Wellington'),
        'tasman_nelson' => t('Tasman/Nelson'),
        'marlborough' => t('Marlborough'),
        'west_coast' => t('West Coast'),
        'canterbury' => t('Canterbury'),
        'otago' => t('Otago'),
        'southland' => t('Southland'),
      ],
      '#empty_value' => '',
      '#empty_option' => t('Please select'),
      '#disabled' => $this->isDisabled(),
    ];

    // How did you find out about SPARX.
    $form['how_did_you_find_out_about_sparx'] = [
      '#type' => 'select',
      '#title' => t('How did you find out about SPARX? *'),
      '#options' => [
        'counsellor' => 'From a counsellor',
        'doctor_nurse' => 'From a doctor/nurse',
        'school' => 'At school or from a school guidance counsellor',
        'youth_worker' => 'From a youth worker',
        'friend' => 'Through a friend',
        'facebook' => 'Facebook',
        'google' => 'Google',
        'advertisement' => 'Advertisement',
        'media' => 'Media',
        'other' => 'Other',
      ],
      '#empty_value' => '',
      '#empty_option' => 'Please select',
      '#disabled' => $this->isDisabled(),
    ];

    // How did you find out about SPARX (Other).
    $form['how_did_you_find_out_other'] = [
      '#type' => 'textfield',
      '#title' => t('How did you find out about SPARX? (Other) *'),
      '#size' => 40,
      '#maxlength' => 60,
      '#states' => [
        'visible' => [
          ':input[name="how_did_you_find_out_about_sparx"]' => ['value' => 'other'],
        ],
      ],
      '#disabled' => $this->isDisabled(),
    ];

    // Contact permissions and "Agree to terms".
    $form['underline'] = [
      '#type' => 'item',
      '#theme' => 'sparx_underline',
    ];
    $form['contact_permission'] = [
      '#type' => 'checkbox',
      '#title' => t('Can we contact you for SPARX research, feedback or to be involved in focus groups in the future?'),
      '#default_value' => 0,
      '#disabled' => $this->isDisabled(),
    ];
    $renderableExtraHelp = [
      '#theme' => 'sparx_extra_help',
    ];
    $form['extra_help'] = [
      '#type' => 'checkbox',
      '#title' => $this->renderer->render($renderableExtraHelp),
      '#default_value' => 0,
      '#disabled' => $this->isDisabled(),
    ];
    $renderableAgreeTerms = [
      '#theme' => 'sparx_agree_terms',
    ];
    $form['agree_terms'] = [
      '#type' => 'checkbox',
      '#title' => $this->renderer->render($renderableAgreeTerms),
      '#default_value' => 0,
      '#disabled' => $this->isDisabled(),
    ];

    // Tips before they start.
    $form['before_you_start_markup'] = [
      '#type' => 'item',
      '#theme' => 'sparx_before_start_text',
    ];

    // Sign up!
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => t('SIGN UP FOR SPARX'),
      '#disabled' => $this->isDisabled(),
    ];

    return $form;
  }

  /**
   * Logs the form values for debugging.
   *
   * @param array $formValues
   *   The processed form values.
   */
  private function logFormValuesDebugParams(array $formValues) {

    $cleanFormValues = $formValues;

    if (!$this->isLogAllTestingFlag()) {
      if (isset($cleanFormValues['password'])) {
        unset($cleanFormValues['password']);
      }
      if (isset($cleanFormValues['email'])) {
        unset($cleanFormValues['email']);
      }
      if (isset($cleanFormValues['retypeEmail'])) {
        unset($cleanFormValues['retypeEmail']);
      }
      if (isset($cleanFormValues['mobile'])) {
        unset($cleanFormValues['mobile']);
      }
    }

    $this->aLogJson(SA_REGISTER, $cleanFormValues);
  }

  /**
   * Validates the form.
   *
   * @param array $form
   *   The Drupal form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The Drupal form state.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    // If POST and not permitted as per geolocation.
    // Skip as we will reload it in the submit form process.
    if ($this->isDisabled()) {
      $this->aLog(SA_REGISTER, t("Validation detected in disabled mode"));
      return;
    }

    $this->aLog(SA_REGISTER, t("Validating form"));

    // Get the form values as a single processed array.
    $formValues = $this->getFormValues($form_state);

    // Log the form data for debugging.
    $this->logFormValuesDebugParams($formValues);

    // Username.
    if ($this->sValid->noLength($formValues['username'])) {
      $form_state->setErrorByName('username', t('Please enter a username.'));
    }
    else {
      $error = $this->sValid->getValidateUsernameError($formValues['username']);
      if (!is_null($error)) {
        $form_state->setErrorByName('username', $error);
      }
      if ($this->sValid->isUsernameTaken($formValues['username'])) {
        $form_state->setErrorByName('username', t('The username %name is already taken. Please select a different one.', ['%name' => $formValues['username']]));
      }
    }

    // Who are you.
    if ($this->sValid->noLength($formValues['whoAreYou'])
      || !$this->sValid->validateWhoAreYou($formValues['whoAreYou'])) {
      $form_state->setErrorByName('who_are_you', t("Please select a valid 'Who are you?' answer."));
    }

    if ($this->sValid->isWhoAreYouOther($formValues['whoAreYou'])
      && $this->sValid->noLength($formValues['whoAreYouOther'])) {
      $form_state->setErrorByName('who_are_you_other', t("Please select a valid 'Who are you? (Other)' answer."));
    }

    // Young person specific question expansion.
    if ($this->sValid->isYoungPerson($formValues['whoAreYou'])) {
      // Age.
      if ($this->sValid->noLength($formValues['age']) ||
        !$this->sValid->validateAge($formValues['age'])) {
        $form_state->setErrorByName('age', t('Please enter a valid age.'));
      }
      // Ethnic group.
      if ($this->sValid->notArray($formValues['ethnicGroup'])
        || !$this->sValid->validateEthnicity($formValues['ethnicGroup'])) {
        $form_state->setErrorByName('ethnic_group', t('Please tell us your main ethnic group.'));
      }
      if ($this->sValid->notArray($formValues['ethnicGroup'])
        || ($this->sValid->isEthnicityOther($formValues['ethnicGroup'])
        && $this->sValid->noLength($formValues['ethnicGroupOther']))) {
        $form_state->setErrorByName('ethnic_group_other', t('Please tell us your main ethnic group (Other).'));
      }
      // Gender.
      if (($this->sValid->noLength($formValues['gender']))
        || !$this->sValid->validateGender($formValues['gender'])) {
        $form_state->setErrorByName('gender', t('Please select a gender.'));
      }
    }
    // Email.
    $strPleaseValidEmail = t('Please enter a valid email address.');
    if ($this->sValid->noLength($formValues['email'])) {
      $form_state->setErrorByName('email', $strPleaseValidEmail);
    }
    elseif (strcmp($formValues['email'], $formValues['retypeEmail']) !== 0) {
      $form_state->setErrorByName('email', t('Email confirmation should match email address.'));
    }
    else {
      if (!$this->sValid->validateEmail($formValues['email'])) {
        $form_state->setErrorByName('email', $strPleaseValidEmail);
      }
      else {
        if ($this->sValid->isEmailTaken($formValues['email'])) {
          $form_state->setErrorByName('email', t('The e-mail address %email is already registered. <a href="@password">Have you forgotten your password?</a>', ['%email' => $formValues['email'], '@password' => $this->url('user.pass')]));
        }
      }
    }

    // Validate password.
    if ($this->sValid->noLength($formValues['password'])) {
      $form_state->setErrorByName('pass', t('Please enter a valid password.'));
    }

    // Validate mobile.
    if ($this->sValid->hasLength($formValues['mobile'])) {
      if (!$this->sValid->validateMobilePrefix($formValues['mobilePrefix'])) {
        $form_state->setErrorByName('mobile', t('The mobile number prefix is invalid, it must be one of: %prefixes.', ['%prefixes' => $this->sValid->getValidMobilePrefixList()]));
      }
      elseif (!$this->sValid->validateMobile($formValues['mobile'])) {
        $form_state->setErrorByName('mobile', t('Please enter a valid mobile number.'));
      }
      elseif (!$this->sValid->validateMobileLength($formValues['mobile'])) {
        $form_state->setErrorByName('mobile', t('The length of the mobile number you enter is invalid it needs to be between %minlength and %maxlength digits.', ['%minlength' => $this->sValid->getMobileMinLength(), '%maxlength' => $this->sValid->getMobileMaxLength()]));
      }
    }

    // Validate location.
    if ($this->sValid->noLength($formValues['whereDoYouLive']) || !$this->sValid->validateRegion($formValues['whereDoYouLive'])) {
      $form_state->setErrorByName('where_do_you_live', t('Please select where you live.'));
    }

    // Validate how they found out about SPARX.
    if ($this->sValid->noLength($formValues['foundOutAbout'])
      || !$this->sValid->validateFindOut($formValues['foundOutAbout'])) {
      $form_state->setErrorByName('how_did_you_find_out_about_sparx', t('Please choose how you found out about SPARX.'));
    }
    if ($this->sValid->isFindOutOther($formValues['foundOutAbout'])
      && $this->sValid->noLength($formValues['foundOutAboutOther'])) {
      $form_state->setErrorByName('how_did_you_find_out_other', t('Please enter how you found out about SPARX (Other).'));
    }

    // Make sure agree to terms checkboxes are checked.
    if (!$formValues['extraHelp']) {
      $form_state->setErrorByName('extra_help', t('Please tick to confirm that you know you need to get extra help if SPARX is not helping you.'));
    }
    if (!$formValues['agreeTerms']) {
      $form_state->setErrorByName('agree_terms', t('Please agree to the Terms of use.'));
    }

    $this->aLog(SA_REGISTER, t("Validated form"));

    // Log errors for debugging.
    $cleanFormErrors = $form_state->getErrors();
    if (!SPARX_CFG_LOG_ALL_TESTING) {
      if (isset($cleanFormErrors['email'])) {
        $cleanFormErrors['email'] = str_replace($formValues['email'], t('[HIDDEN_EMAIL]'), $cleanFormErrors['email']);
      }
    }

    $this->aLogJson(SA_REGISTER, $cleanFormErrors);
  }

  /**
   * Gets the form values from the form state interface.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state interface for the form.
   *
   * @return array
   *   The processed form values.
   */
  private function getFormValues(FormStateInterface &$form_state) {

    // Common values.
    $formValues = [
      'username' => trim($form_state->getValue('username')),
      'whoAreYou' => $form_state->getValue('who_are_you'),
      'whoAreYouOther' => trim($form_state->getValue('who_are_you_other')),
      'email' => trim($form_state->getValue('email')),
      'emailAllow' => $form_state->getValue('email_allow') ? TRUE : FALSE,
      'retypeEmail' => trim($form_state->getValue('retype_email')),
      'password' => $form_state->getValue('pass'),
      'mobile' => trim($form_state->getValue('mobile')),
      'whereDoYouLive' => trim($form_state->getValue('where_do_you_live')),
      'foundOutAbout' => trim($form_state->getValue('how_did_you_find_out_about_sparx')),
      'contactPermission' => $form_state->getValue('contact_permission') ? TRUE : FALSE,
      'extraHelp' => $form_state->getValue('extra_help') ? TRUE : FALSE,
      'agreeTerms' => $form_state->getValue('agree_terms') ? TRUE : FALSE,
    // 'mobilePrefix' => $form_state->getValue('mobile_prefix'),.
    ];
    $formValues['mobilePrefix'] = substr($formValues['mobile'], 0, $this->sValid->getMobilePrefixLength());

    // Young person specific values.
    if ($this->sValid->isYoungPerson($formValues['whoAreYou'])) {
      $formValuesYoungPerson = [
        'age' => trim($form_state->getValue('age')),
        'gender' => $form_state->getValue('gender'),
        'ethnicGroup' => $form_state->getValue('ethnic_group'),
        'ethnicGroupOther' => trim($form_state->getValue('ethnic_group_other')),
      ];
    }
    else {
      $formValuesYoungPerson = [
        'age' => NULL,
        'gender' => '',
        'ethnicGroup' => [],
        'ethnicGroupOther' => '',
      ];
    }

    // Find out other.
    if ($this->sValid->isFindOutOther($formValues['foundOutAbout'])) {
      $formValues['foundOutAboutOther'] = trim($form_state->getValue('how_did_you_find_out_other'));
    }
    else {
      $formValues['foundOutAboutOther'] = '';
    }

    return array_merge($formValues, $formValuesYoungPerson);
  }

  /**
   * Submits the form after validation.
   *
   * @param array $form
   *   The Drupal form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The Drupal form state.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // If POST and not permitted as per geolocation, reload the page.
    if ($this->isDisabled()) {
      $this->aLog(SA_REGISTER, t("Submit form detected in disabled mode"));
      $form_state->setRedirect('sparx_register_profile.register_form');
      return;
    }

    $this->aLog(SA_REGISTER, t("Processing form submit"));

    // Get all data values (all validated).
    $formValues = $this->getFormValues($form_state);

    // Log the form data for debugging.
    $this->logFormValuesDebugParams($formValues);

    // Format mobile number for database.
    /*if (strlen($formValues['mobile']) > 0) {
    $fullMobileNumber = $formValues['mobilePrefix'] . $formValues['mobile'];
    }
    else {
    $fullMobileNumber = '';
    }*/

    $this->aLog(SA_REGISTER, t("Saving new user"));

    // Create user.
    $sUser = $this->sUser->getNew();
    $sUser->setUsername($formValues['username']);
    $sUser->setPassword($formValues['password']);
    $sUser->setEmail($formValues['email']);
    $sUser->setWhoAreYou($formValues['whoAreYou']);
    $sUser->setWhoAreYouOther($formValues['whoAreYouOther']);
    $sUser->setGender($formValues['gender']);
    $sUser->setEmailAllow($formValues['emailAllow']);
    $sUser->setTxtAllow(strlen($formValues['mobile']) ? TRUE : FALSE);
    $sUser->setMobile(strlen($formValues['mobile']) ? $formValues['mobile'] : "");
    $sUser->setWhereDoYouLive($formValues['whereDoYouLive']);
    $sUser->setHowDidYouFindOutAboutSparx($formValues['foundOutAbout']);
    $sUser->setHowDidYouFindOutOther($formValues['foundOutAboutOther']);
    $sUser->setContactPermission($formValues['contactPermission']);
    $sUser->setRegSource(SPARX_USER_MODEL_REG_SOURCE_WEB);
    $sUser->setEthnicGroupArrStr($formValues['ethnicGroup']);
    $sUser->setEthnicGroupOther($formValues['ethnicGroupOther']);
    $sUser->setAge($formValues['age']);
    $saveResult = $sUser->save();

    // Process save result.
    if ($saveResult !== TRUE) {
      if ($saveResult == SparxAPI::REGISTER_NAMETAKEN) {
        $this->aLog(SA_REGISTER, t("Username in use within the game error"));
        drupal_set_message(t("We have had an unexpected error. The username is in use within the game. Please select another username."), 'error');
      }
      elseif ($saveResult == SparxAPI::REGISTER_FAILURE) {
        $this->aLog(SA_REGISTER, t("Registration failure"));
        drupal_set_message(t("We have had an unexpected error. The username is in use within the game. Please select another username."), 'error');
      }
      else {
        $this->aLog(SA_REGISTER, t("Error within user model follows"));
        $this->aLog(SA_REGISTER, $saveResult);
        drupal_set_message($saveResult, 'error');
      }
      return;
    }
    else {
      $this->aLog(SA_REGISTER, t("New user saved, sending welcome email"));
    }

    $this->sendWelcomeEmail($sUser->getUsername(), $sUser->getEmail());

    $this->aLog(SA_REGISTER, t("Registration complete, redirecting to registration complete page"));

    // Should be logged in, and ready to go!
    $form_state->setRedirect('sparx_register_profile.registration_complete');
  }

}
