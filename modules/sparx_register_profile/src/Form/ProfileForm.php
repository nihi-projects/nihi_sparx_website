<?php

namespace Drupal\sparx_register_profile\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Render\RendererInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\sparx_library\SparxUser;
use Drupal\sparx_library\SparxValidation;
use Drupal\sparx_library\SparxAudit;

use Drupal\sparx_library\Traits\SparxAuditTrait;
use Drupal\sparx_library\Traits\SparxSettingsTrait;

/**
 * Implements an profile form.
 */
class ProfileForm extends FormBase {

  use SparxAuditTrait;
  use SparxSettingsTrait;

  protected $renderer;
  protected $sValid;
  protected $sUser;
  protected $sAudit;

  /**
   * Constructs a renderer service.
   *
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   * @param \Drupal\sparx_library\SparxValidation $sparxValidation
   *   Sparx validation service.
   * @param \Drupal\sparx_library\SparxUser $sparxUser
   *   Sparx user service.
   * @param \Drupal\sparx_library\SparxAudit $sparxAudit
   *   Sparx audit service.
   */
  public function __construct(RendererInterface $renderer, SparxValidation $sparxValidation, SparxUser $sparxUser, SparxAudit $sparxAudit) {
    $this->renderer = $renderer;
    $this->sValid = $sparxValidation;
    $this->sUser = $sparxUser;
    $this->sAudit = $sparxAudit;
  }

  /**
   * Creates this class.
   *
   * @return mixed
   *   Container for this class.
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('renderer'),
      $container->get('sparx_validation_service'),
      $container->get('sparx_user_service'),
      $container->get('sparx_audit_service')
    );
  }

  /**
   * Gets the unique form ID of this form.
   *
   * @return string
   *   The form ID.
   */
  public function getFormId() {
    return 'sparx_register_profile_profile_form';
  }

  /**
   * Checks access for this form.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   Whether allowed or forbidden access.
   */
  public function access() {

    if (\Drupal::currentUser()->isAnonymous()) {
      // Return 403 Access Denied page.
      return AccessResult::forbidden();
    }
    return AccessResult::allowed();
  }

  /**
   * Builds the profile form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $this->aLog(SA_PROFILE, "Building form");

    // Load currently logged in user.
    $sUser = $this->sUser->getById($this->currentUser()->id());

    // Setup header.
    $renderableInitialMarkup = [
      '#theme' => 'sparx_user_profile_header',
      '#sparx_username' => $sUser->getUsername(),
    ];
    $form['initial_markup'] = [
      '#type' => 'item',
      '#markup' => $this->renderer->render($renderableInitialMarkup),
    ];

    // Setup fields.
    $form['email'] = [
      '#type' => 'textfield',
      '#title' => t('Email address *'),
      '#size' => 40,
      '#maxlength' => 255,
      '#default_value' => $sUser->getEmail(),
    ];
    /*    $form['mobile_prefix'] = [
    '#type' => 'select',
    '#title' => t('Mobile prefix'),
    '#title_display' => 'invisible',
    '#options' => [
    '020' => t('020'),
    '021' => t('021'),
    '022' => t('022'),
    '027' => t('027'),
    ],
    '#default_value' => $sUser->getMobilePrefix(),
    ]; */
    $form['mobile'] = [
      '#type' => 'textfield',
      '#title' => t('Mobile number'),
      '#size' => 20,
      '#maxlength' => 20,
      '#default_value' => $sUser->getMobile(),
    ];
    $form['pass_markup'] = [
      '#type' => 'item',
      '#theme' => 'sparx_reset_password_header',
    ];
    $form['pass'] = [
      '#type' => 'password_confirm',
      '#title' => t('Password'),
      '#title_display' => 'invisible',
      '#size' => 12,
    ];
    $form['notifications'] = [
      '#type' => 'item',
      '#markup' => t('Notifications'),
    ];
    $form['txt_allow'] = [
      '#type' => 'checkbox',
      '#title' => t('Sparx reminders via txt. Unclick if you do not want to get these txts.'),
      '#default_value' => $sUser->getTxtAllow(),
    ];
    $form['email_allow'] = [
      '#type' => 'checkbox',
      '#title' => t('Sparx reminders via email. Unclick if you do not want to get these emails.'),
      '#default_value' => $sUser->getEmailAllow(),
    ];

    // Final update button.
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => t('UPDATE PROFILE'),
    ];

    return $form;
  }

  /**
   * Gets the form values from the form state interface.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state interface for the form.
   *
   * @return array
   *   The processed form values.
   */
  private function getFormValues(FormStateInterface &$form_state) {

    $formValues = [
      'email' => trim($form_state->getValue('email')),
      'mobile' => trim($form_state->getValue('mobile')),
    // 'mobilePrefix' => $form_state->getValue('mobile_prefix'),.
      'password' => $form_state->getValue('pass'),
      'emailAllow' => $form_state->getValue('email_allow') ? TRUE : FALSE,
      'txtAllow' => $form_state->getValue('txt_allow') ? TRUE : FALSE,
    ];

    $formValues['mobilePrefix'] = substr($formValues['mobile'], 0, $this->sValid->getMobilePrefixLength());

    return $formValues;
  }

  /**
   * Logs the form values for debugging.
   *
   * @param array $formValues
   *   The processed form values.
   */
  private function logFormValuesDebugParams(array $formValues) {

    $cleanFormValues = $formValues;

    if (!$this->isLogAllTestingFlag()) {
      if (isset($cleanFormValues['password'])) {
        unset($cleanFormValues['password']);
      }
      if (isset($cleanFormValues['email'])) {
        unset($cleanFormValues['email']);
      }
      if (isset($cleanFormValues['mobile'])) {
        unset($cleanFormValues['mobile']);
      }
      if (isset($cleanFormValues['mobilePrefix'])) {
        unset($cleanFormValues['mobilePrefix']);
      }
    }

    $this->aLogJson(SA_PROFILE, $cleanFormValues);
  }

  /**
   * Validates the form.
   *
   * @param array $form
   *   The Drupal form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The Drupal form state.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    // Common email address error.
    $strPleaseValidEmail = t('Please enter a valid email address.');

    $this->aLog(SA_PROFILE, t("Validating form"));

    // Get the form values as a single processed array.
    $formValues = $this->getFormValues($form_state);

    // Log the form data for debugging.
    $this->logFormValuesDebugParams($formValues);

    // Validate email.
    if ($this->sValid->noLength($formValues['email'])) {
      $form_state->setErrorByName('email', $strPleaseValidEmail);
    }
    elseif (!$this->sValid->validateEmail($formValues['email'])) {
      $form_state->setErrorByName('email', $strPleaseValidEmail);
    }
    else {
      if ($this->sValid->isEmailTaken($formValues['email'], $this->currentUser()->id())) {
        $form_state->setErrorByName('email', t('The e-mail address %email is already registered.', ['%email' => $formValues['email']]));
      }
    }

    // Validate mobile.
    if ($this->sValid->hasLength($formValues['mobile'])) {
      if (!$this->sValid->validateMobilePrefix($formValues['mobilePrefix'])) {
        $form_state->setErrorByName('mobile', t('The mobile number prefix is invalid, it must be one of: %prefixes.', ['%prefixes' => $this->sValid->getValidMobilePrefixList()]));
      }
      elseif (!$this->sValid->validateMobile($formValues['mobile'])) {
        $form_state->setErrorByName('mobile', t('Please enter a valid mobile number.'));
      }
      elseif (!$this->sValid->validateMobileLength($formValues['mobile'])) {
        $form_state->setErrorByName('mobile', t('The length of the mobile number you enter is invalid it needs to be between %minlength and %maxlength digits.', ['%minlength' => $this->sValid->getMobileMinLength(), '%maxlength' => $this->sValid->getMobileMaxLength()]));
      }
    }

    $this->aLog(SA_PROFILE, t("Validated form"));

    // Log errors for debugging.
    $cleanFormErrors = $form_state->getErrors();
    if (!$this->isLogAllTestingFlag()) {
      if (isset($cleanFormErrors['email'])) {
        $cleanFormErrors['email'] = str_replace($formValues['email'], '[HIDDEN_EMAIL]', $cleanFormErrors['email']);
      }
    }
    $this->aLogJson(SA_PROFILE, $cleanFormErrors);
  }

  /**
   * Submits the form after validation.
   *
   * @param array $form
   *   The Drupal form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The Drupal form state.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $this->aLog(SA_PROFILE, t("Processing form submit"));

    // Get the form values as a single processed array.
    $formValues = $this->getFormValues($form_state);

    // Log the form data for debugging.
    $this->logFormValuesDebugParams($formValues);

    $this->aLog(SA_PROFILE, t("Saving existing user"));

    // Load the user object and update with the new details.
    $sUser = $this->sUser->getById($this->currentUser()->id());

    $sUser->setEmail($formValues['email']);

    // Only save mobile if have input.
    $sUser->setMobile(strlen($formValues['mobile']) ? $formValues['mobile'] : '');

    // Only update password if something typed.
    if ($this->sValid->hasLength($formValues['password'])) {
      $sUser->setPassword($formValues['password']);
    }

    // Setup permissions for contact.
    $sUser->setEmailAllow($formValues['emailAllow']);
    $sUser->setTxtAllow($formValues['txtAllow']);

    // Save in DB.
    $saveResult = $sUser->save();

    if ($saveResult !== TRUE) {

      // Capture error.
      $this->aLog(SA_PROFILE, [
        t("Error within user model follows"),
        $saveResult,
      ]);

      drupal_set_message($saveResult, 'error');

      return;
    }

    $this->aLog(SA_PROFILE, t("Existing user saved"));

    // Send updated.
    drupal_set_message('Profile updated.');
  }

}
