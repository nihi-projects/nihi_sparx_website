<?php

namespace Drupal\sparx_register_profile\Controller;

use Drupal\Core\Access\AccessResult;

use Drupal\Core\Controller\ControllerBase;

/**
 * Page displayed after a user registers.
 */
class RegistrationCompleteController extends ControllerBase {

  /**
   * Builds the content to be displayed from template.
   */
  public function content() {
    $build = [
      '#theme' => 'sparx_registration_complete',
    ];

    return $build;
  }

  /**
   * Checks access for this controller.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   Whether allowed or forbidden access.
   */
  public function access() {

    if (\Drupal::currentUser()->isAnonymous()) {
      // Return 403 Access Denied page.
      return AccessResult::forbidden();
    }
    return AccessResult::allowed();
  }

}
