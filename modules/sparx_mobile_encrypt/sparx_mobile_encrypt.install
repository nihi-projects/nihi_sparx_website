<?php

/**
 * @file
 * Install and uninstall functions for the sparx_mobile_encrypt module.
 */

use Drupal\encrypt\Entity\EncryptionProfile;

/**
 * Implements hook_install().
 */
function sparx_mobile_encrypt_install() {

  $encryptionProfile = _sparx_mobile_encrypt_encryption_profile();

  $userRecords = _sparx_mobile_encrypt_get_user_records();

  foreach ($userRecords as $userRecord) {
    if (_sparx_mobile_encrypt_is_empty($userRecord->mobile)) {
      $newMobileValue = '';
    }
    else {
      $newMobileValue = Drupal::service('encryption')->encrypt($userRecord->mobile, $encryptionProfile);
    }
    _sparx_mobile_encrypt_update_user_record($userRecord->uid, ['mobile' => $newMobileValue]);
  }
}

/**
 * Implements hook_uninstall().
 */
function sparx_mobile_encrypt_uninstall() {

  $encryptionProfile = _sparx_mobile_encrypt_encryption_profile();

  $userRecords = _sparx_mobile_encrypt_get_user_records();

  foreach ($userRecords as $userRecord) {
    if (_sparx_mobile_encrypt_is_empty($userRecord->mobile)) {
      $newMobileValue = '';
    }
    else {
      $newMobileValue = Drupal::service('encryption')->decrypt($userRecord->mobile, $encryptionProfile);
    }
    _sparx_mobile_encrypt_update_user_record($userRecord->uid, ['mobile' => $newMobileValue]);
  }
}

/**
 * Checks to see if field is empty.
 *
 * @param string $str
 *   String to check.
 *
 * @return bool
 *   TRUE if empty, FALSE if not.
 */
function _sparx_mobile_encrypt_is_empty($str) {

  return (is_null($str) || !strlen(trim($str)));
}

/**
 * Loads encryption profile.
 *
 * @return Drupal\encrypt\Entity\EncryptionProfile
 *   The encryption profile form the define at the top of this file.
 */
function _sparx_mobile_encrypt_encryption_profile() {

  return EncryptionProfile::load(SPARX_CFG_ENCRYPT_PROFILE);
}

/**
 * Gets user records from additional table.
 *
 * @return \Drupal\Core\Database\StatementInterface|null
 *   A prepared statement, or NULL if the query is not valid.
 */
function _sparx_mobile_encrypt_get_user_records() {

  $connection = \Drupal::service('database');

  $userRecords = $connection->select('sparxuser', 'usr')
    ->fields('usr', ['uid', 'mobile'])->execute();

  return $userRecords;
}

/**
 * Updates user record fields.
 *
 * @param int $uid
 *   The User ID to update.
 * @param array $fields
 *   A key/value of the field/values to update.
 */
function _sparx_mobile_encrypt_update_user_record($uid, array $fields) {

  $connection = \Drupal::service('database');

  $connection->update('sparxuser')
    ->fields($fields)
    ->condition('uid', $uid, '=')
    ->execute();
}
