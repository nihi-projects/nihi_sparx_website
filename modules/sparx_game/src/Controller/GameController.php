<?php

namespace Drupal\sparx_game\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Session\AccountProxyInterface;

use Drupal\sparx_library\SparxAPI;

use Drupal\sparx_library\Traits\SparxSettingsTrait;

/**
 * Controller to launch the game.
 */
class GameController extends ControllerBase {

  use SparxSettingsTrait;

  /**
   * Constructs the controller.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   */
  public function __construct(AccountProxyInterface $current_user) {

    $this->currentUser = $current_user;
  }

  /**
   * Creates the static instance.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container object.
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('current_user')
    );
  }

  /**
   * To check whether user using mobile devices (including tablet).
   *
   * @return bool
   *   TRUE if using mobile device, false if not.
   */
  private function isMobile() {
    $useragent = $_SERVER['HTTP_USER_AGENT'];

    return preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i', $useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4));
  }

  /**
   * To check whether user using Microsoft Edge Browser.
   *
   * @return bool
   *   TRUE if using Microsoft Edge Browser, false if not.
   */
  private function isEdge() {
    $useragent = $_SERVER['HTTP_USER_AGENT'];

    return preg_match('/Edge/i', $useragent);
  }

  /**
   * Displays the content page for the game.
   */
  public function content() {

    // Find the username of the user we need to load.
    $username = $this->getGameUsernameWithOverride($this->currentUser->getAccountName());

    // Get root API URL.
    $gameAPI = sparxAPI::getGameApiRoot();

    // Get the token and level from the API.
    $token = sparxAPI::getUserToken($username);
    $level = sparxAPI::getUserLevel($username);

    // Display the error to the user if it is not going to work.
    $errorText = NULL;
    if ($this->isMobile()) {
      $errorText = t('SPARX is currently only compatible with PCs and Macs. A SPARX app is due for release soon.');
    }
    elseif ($this->isEdge()) {
      $errorText = t('SPARX is currently not compatible with the browser Microsoft Edge. Please try using an alternative browser e.g. Chrome, Firefox, or Safari.');
    }
    elseif (!$token || !$level) {
      $errorText = t("You don't appear to be registered to play the game. Please contact us so we can address this problem.");
    }

    if (!is_null($errorText)) {
      // Build simple error template.
      $build = [
        '#markup' => $errorText,
        '#attached' => [
          'library' => [
            'sparx_game/sparx_game_libraries_css',
          ],
        ],
      ];
    }
    else {
      // Build complex game launcher using Unity.
      $build = [
        '#theme' => 'sparx_launch_game',
        '#attached' => [
          'drupalSettings' => [
            'sparx_game' => [
              'WebGLPath' => '/libraries/sparx_webgl',
              'WebPlayerPath' => '/libraries/sparx_webplayer',
              'token' => $token,
              'level' => $level,
              'gameAPI' => $gameAPI,
            ],
          ],
          'library' => [
            'sparx_game/sparx_game_libraries_js',
            'sparx_game/sparx_game_libraries_css',
          ],
        ],
      ];
    }

    return $build;
  }

  /**
   * Checks access for this controller.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   Returns allowed or forbidden access.
   */
  public function access() {

    $roles = $this->currentUser->getRoles();

    if ($this->currentUser->isAuthenticated() && !in_array('administrator', $roles)) {
      return AccessResult::allowed();
    }
    // Return 403 Access Denied page.
    return AccessResult::forbidden();
  }

}
