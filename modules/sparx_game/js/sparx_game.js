/**
 * @file
 * Sparx game loader.
 */

var usingwebGL = false;      /* variable used by the code below -- don't change this */
var gameConfigUrl;
var gameInstance;

function unityGameLoaded(objectName) {
  if (usingwebGL) {
    gameInstance.SendMessage(objectName, "setConfig", gameConfigUrl);
  }
else {
    unityObj.getUnity().SendMessage(objectName, "setConfig", gameConfigUrl);
  }
}

function gameLevelEnd() {
  location.reload();
}

function gamelog(msg) {
  console.log(msg);
}

(function ($, Drupal, drupalSettings) {

  $(document).ready(function () {
    /*
     * Fix requested for scrolling issue by Stickmen
     * Also prevent backspace from functioning as browser back key on the game page - as users may use it when typing in notebook
     */
    onkeydown = function (e) {
      if ((e.keyCode >= 37 && e.keyCode <= 40) || e.keyCode == 8) {
        e.preventDefault();
      }
    };

    /* If we're running Chrome above version 42, we won't be able to use Unity, so need to try HTML5 instead */

    gametoken = drupalSettings.sparx_game.token;
    gameAPI = drupalSettings.sparx_game.gameAPI;
    gameConfigUrl = gameAPI + 'config?sid=' + gametoken;

    var webgl_path = drupalSettings.sparx_game.WebGLPath;
    var webplayer_path = drupalSettings.sparx_game.WebPlayerPath;
    var chromeVersion = window.navigator.userAgent.match(/Chrome\/(\d+)\./);
    // 2017-04-21 Andy, Adding checking on Firefox v52, that not suuport WebPlayer anymore.
    var firefoxVersion = window.navigator.userAgent.match(/Firefox\/(\d+)\./);

    if ((chromeVersion && chromeVersion[1] && parseInt(chromeVersion[1], 10) >= 42) ||
        (firefoxVersion && firefoxVersion[1] && parseInt(firefoxVersion[1], 10) >= 52)) {
      usingwebGL = true;
      jQuery("#unityPlayer").find(".missing").remove();
      var $webGL = jQuery("#unityGL").show();
      var progressscript = document.createElement('script');
      progressscript.src = webgl_path + "/TemplateData/UnityProgress.js";
      document.getElementsByTagName('head')[0].appendChild(progressscript);
	  var loderscript = document.createElement('script');
      loderscript.src = webgl_path + "/Build/UnityLoader.js";
      document.getElementsByTagName('head')[0].appendChild(loderscript);
	  loderscript.onload = function () {
		gameInstance = UnityLoader.instantiate("gameContainer", webgl_path + "/Build/SoW2_WebGL.json", {onProgress: UnityProgress});
	  };
	  //$.getScript(webgl_path + "/Build/UnityLoader.js");
    }
else {
      unityFile = webplayer_path + "/SoW2_WebPlayer_v2.unity3d";
      unityObj = new UnityObject2({
        width: 960,
        height: 600,
        params: {
          enableDebugging: "0",
          disableFullscreen: true
        }
      });
      var $missingScreen = jQuery("#unityPlayer").find(".missing").hide();
      var putGame = function (status) {
        switch (status) {
          case "unsupported":
          case "broken":
          case "missing":
            $missingScreen.find("a").click(function (e) {
              e.stopPropagation();
              e.preventDefault();
              unityObj.installPlugin();
              return false;
            });
            $missingScreen.show();
            break;

          case "installed":
            $missingScreen.remove();
            break;
        }
      }
      unityObj.observeProgress(function (progress) {
        putGame(progress.pluginStatus);
      });
      unityObj.initPlugin(jQuery("#unityPlayer")[0], unityFile);
    }
  });
})(jQuery, Drupal, drupalSettings);
