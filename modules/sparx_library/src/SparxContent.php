<?php

namespace Drupal\sparx_library;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

use Drupal\sparx_library\Traits\SparxAuditTrait;

/**
 * Provides easy access to content used by the system.
 */
class SparxContent {

  use StringTranslationTrait;
  use SparxAuditTrait;

  protected $database;
  protected $entityTypeManager;
  protected $nodeStorage;
  protected $adherencePeriods = [14, 21];

  /**
   * Creates this class.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database handler.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The handler for the entities.
   */
  public function __construct(Connection $database, EntityTypeManagerInterface $entityTypeManager) {

    $this->database = $database;
    $this->entityTypeManager = $entityTypeManager;

    $this->nodeStorage = $entityTypeManager->getStorage('node');
  }

  /**
   * Creates a static instance of this class.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container object.
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this content class.
    return new static(
      // Load the service required to construct this class.
      $container->get('database'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Generic function to get the node entity content of a type and condition.
   *
   * @param string $type
   *   The type of the entity.
   * @param array $condition
   *   A key/value pair of conditions for the entity.
   *
   * @return mixed
   *   NULL if no content value, otherwise the string content.
   */
  private function getNodeEntityType($type, array $condition = []) {

    $query = $this->nodeStorage->getQuery();
    $query->condition('type', $type);

    foreach ($condition as $conditionKey => $conditionValue) {
      $query->condition($conditionKey, $conditionValue);
    }
    $contentIds = $query->execute();
    $contentId = current($contentIds);

    if ($contentId) {
      $content = $this->nodeStorage->load($contentId);
      return $content->get('body')->getValue()[0]['value'];
    }
    else {
      return NULL;
    }
  }

  /**
   * Gets the periods that are used to send adherence emails.
   *
   * @return array
   *   List of periods (in days).
   */
  public function getAdherencePeriods() {
    return $this->adherencePeriods;
  }

  /**
   * Gets the message sent indicating possible spam.
   *
   * @return string
   *   The textual message.
   */
  public function getSpamSystemMessage() {
    return t("Possible excess messaging, please check the log messages.");
  }

  /**
   * Gets the messent send indicating system failure.
   *
   * @return string
   *   The textual message.
   */
  public function getFailureSystemMessage() {
    return t("Possible system failure, please check the log messages.");
  }

  /**
   * Gets the messent send indicating internal error.
   *
   * @return string
   *   The textual message.
   */
  public function getFailureInternalMessage() {
    return t("Possible internal error, please check the log messages.");
  }

  /**
   * Gets the response markup for the mood quiz textual responses.
   *
   * @param int $responseNum
   *   The response number in the field_response_number.
   *
   * @return string
   *   The string of all the page content.
   */
  public function getMoodQuizResponseMarkup($responseNum) {

    $content = $this->getNodeEntityType('moodquiz_response', ['field_response_number' => $responseNum]);
    if ($content) {
      return $content;
    }
    else {
      throw new \Exception("Page content not found");
    }
  }

  /**
   * Gets the adherence email content for the period.
   *
   * @param int $days
   *   The days that classifies the content (field_days)
   *
   * @return string
   *   The content of the adherence email.
   */
  public function getAdherenceEmail($days) {

    $content = $this->getNodeEntityType('adherence_email', ['field_days' => $days]);
    if ($content) {
      return $content;
    }
    else {
      throw new \Exception("Adherence Email not found");
    }
  }

  /**
   * Gets the adherence txt content for the period.
   *
   * @param int $days
   *   The days that classifies the content (field_days)
   *
   * @return string
   *   The content of the adherence txt.
   */
  public function getAdherenceTxt($days) {

    $content = $this->getNodeEntityType('adherence_txt', ['field_days' => $days]);
    if ($content) {
      return $content;
    }
    else {
      throw new \Exception("Adherence Txt not found");
    }
  }

  /**
   * Gets the mood email content.
   *
   * @return string
   *   The content of the adherence email.
   */
  public function getMoodEmail() {

    $moodEmailContent = $this->getNodeEntityType('mood_email');
    if ($moodEmailContent) {
      return $moodEmailContent;
    }
    else {
      throw new \Exception("Mood email not found");
    }
  }

  /**
   * Gets the mood txt content.
   *
   * @return string
   *   The content of the adherence txt.
   */
  public function getMoodTxt() {

    $moodEmailContent = $this->getNodeEntityType('mood_txt');
    if ($moodEmailContent) {
      return $moodEmailContent;
    }
    else {
      throw new \Exception("Mood txt not found");
    }
  }

}
