<?php

namespace Drupal\sparx_library;

use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Serialization\Json;

// The text types that can be used in the database.
// API's.
define('SA_INTERNAL_API', 'internal_api');
define('SA_PUBLIC_API', 'public_api');

// Forms.
define('SA_REGISTER', 'register_form');
define('SA_PROFILE', 'profile_form');

// Cron.
define('SA_MOOD_MSG_CRON', 'mood_messages_cron');
define('SA_ADH_MSG_CRON', 'adherence_messages_cron');
define('SA_SYS_MSG_CRON', 'system_messages_cron');

// Queues.
define('SA_MOOD_MSG_QUEUE', 'mood_messages_queue');
define('SA_ADH_MSG_QUEUE', 'adherence_messages_queue');
define('SA_SYS_MSG_QUEUE', 'system_messages_queue');

// Models.
define('SA_USER_MODEL', 'user_model');

// Specific tracking for system checking.
define('SA_MSG_SEND_TRACK', 'message_send_track');
define('SA_SYS_FAIL_TRACK', 'system_failure_track');
define('SA_USR_FAIL_TRACK', 'user_failure_track');

/**
 * Class for logging what is going on.
 */
class SparxAudit {

  protected $database;
  protected $request;
  protected $currentUser;
  protected $time;

  protected $type;
  protected $uid;
  protected $username;

  /**
   * Constructs the class.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database object.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request
   *   The HTTP request object.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user object.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time interface object for timestamps.
   */
  public function __construct(Connection $database, RequestStack $request, AccountProxyInterface $current_user, TimeInterface $time) {

    // Setup services.
    $this->database = $database;
    $this->request = $request->getCurrentRequest();
    $this->currentUser = $current_user;
    $this->time = $time;
  }

  /**
   * Creates a static instance of the class.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container data for this service.
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this content class.
    return new static(
    // Load the service required to construct this class.
      $container->get('database'),
      $container->get('request_stack'),
      $container->get('current_user'),
      $container->get('datetime.time')
    );
  }

  /**
   * Sets the type of the log.
   *
   * @param string $type
   *   The text type logged in the database.
   */
  public function setType($type) {
    $this->type = $type;
  }

  /**
   * Sets the user ID for the user to log.
   *
   * @param int $uid
   *   NULL for no user, otherwise the User ID of the user.
   */
  public function setUserId($uid) {
    $this->uid = $uid;
  }

  /**
   * Sets the username of the user to log.
   *
   * @param string $username
   *   NULL for no username, otherwise the username of the user.
   */
  public function setUsername($username) {
    $this->username = $username;
  }

  /**
   * Sets the user ID and username of the user to log.
   *
   * @param int $uid
   *   NULL for no user, otherwise the User ID of the user.
   * @param string $username
   *   NULL for no username, otherwise the username of the user.
   */
  public function setUser($uid, $username) {
    $this->setUserId($uid);
    $this->setUsername($username);
  }

  /**
   * Resets the user ID and username to NULL.
   */
  public function resetUser() {
    $this->setUser(NULL, NULL);
  }

  /**
   * Logs JSON data to the database.
   *
   * @param string $type
   *   The text type logged in the database.
   * @param mixed $data
   *   The data logged in the database.
   * @param string $description
   *   Optional description to prefix the JSON.
   */
  public function logJson($type, $data, $description = '') {

    // Header so we know it's JSON encoded.
    $jsonStr = 'JSON: ';
    // Add optional prefix if set.
    $prefix = (strlen($description)) ? ($description . ' ' . $jsonStr) : $jsonStr;
    // Send it to the database.
    $this->log($type, $prefix . Json::encode($data));
  }

  /**
   * Logs a message and then another row of JSON data.
   *
   * @param string $type
   *   The text type logged in the database.
   * @param string $message
   *   The string message logged to the database.
   * @param mixed $data
   *   The data logged in the database.
   * @param string $description
   *   Optional description to prefix the JSON.
   */
  public function logMessageJson($type, $message, $data, $description = '') {

    // First send the normal log.
    $this->log($type, $message);
    // Now send the JSON log.
    $this->logJson($type, $data, $description);
  }

  /**
   * Sends a log to the database.
   *
   * @param string $type
   *   The text type logged in the database.
   * @param mixed $messages
   *   The message logged to the database.
   *   If its a string, just that message
   *   If its an array then all elements in the array as messages.
   */
  public function log($type, $messages) {
    if (is_array($messages)) {
      // Cycle through as array of messages.
      foreach ($messages as $message) {
        $this->sendToDatabase($type, $message);
      }
    }
    else {
      // Just send one message.
      $this->sendToDatabase($type, $messages);
    }
  }

  /**
   * Sends data to the database, uses the User ID/username if available.
   *
   * @param string $type
   *   The text type logged in the database.
   * @param string $message
   *   The string message logged to the database.
   */
  private function sendToDatabase($type, $message) {
    try {
      // Insert it directly into the database.
      $this->database->insert('sparxaudit')
        ->fields([
          // Use normal timestamp for simple comparisons.
          'timestamp' => $this->time->getCurrentTime(),
          // Save microtime as well for detailed analysis.
          'microtime' => $this->time->getCurrentMicroTime(),
          // Store standard type & message.
          'type' => $type,
          'message' => $message,
          // UID if we have one, otherwise the current logged in user.
          'uid' => $this->uid ? $this->uid : $this->currentUser->id(),
          // Username if we have one, otherwise the current logged in username.
          'name' => $this->username ? $this->username : $this->currentUser->getAccountName(),
          // Store the IP address.
          'hostname' => $this->request->getClientIp(),
        ])->execute();
    }
    catch (\Exception $e) {
      // Hopefully this never happens.
      watchdog_exception('sparx_library_sparxaudit', $e);
    }
  }

}
