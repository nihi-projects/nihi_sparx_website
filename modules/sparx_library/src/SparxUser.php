<?php

namespace Drupal\sparx_library;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

use Drupal\sparx_library\Traits\SparxAuditTrait;
use Drupal\sparx_library\Traits\SparxCryptTrait;

/**
 * Class to manage the users.
 */
class SparxUser {

  use StringTranslationTrait;

  use SparxAuditTrait;
  use SparxCryptTrait;

  protected $database;
  protected $entityTypeManager;
  protected $sAudit;

  /**
   * Creates a new SparxUser class.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The manager for the entities.
   * @param \Drupal\sparx_library\SparxAudit $sparxAudit
   *   The audit logger.
   */
  public function __construct(Connection $database, EntityTypeManagerInterface $entityTypeManager, SparxAudit $sparxAudit) {

    $this->database = $database;
    $this->entityTypeManager = $entityTypeManager;
    $this->sAudit = $sparxAudit;
  }

  /**
   * Creates a static version of the class.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container for the class.
   *
   * @return mixed
   *   The static container of objects.
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this content class.
    return new static(
    // Load the service required to construct this class.
      $container->get('database'),
      $container->get('entity_type.manager'),
      $container->get('sparx_audit_service')
    );
  }

  /**
   * Checks to see if an email is taken.
   *
   * @param string $email
   *   The email address to check.
   *
   * @return mixed
   *   FALSE if no user, otherwise the UID of the user.
   */
  public function isEmailTaken($email) {

    $users = $this->entityTypeManager->getStorage('user')
      ->loadByProperties(['mail' => $email]);

    if (count($users) > 1) {
      // More than one email address, so alert.
      $this->aLog(SA_USR_FAIL_TRACK, t("SparxUser - Checking for email address found more than one user with the same email: @email", ['@email' => $this->encrypt($email)]));
    }

    $user = $users ? reset($users) : FALSE;

    if ($user) {
      return $user->id();
    }
    else {
      return FALSE;
    }
  }

  /**
   * Checks to see if the username is taken.
   *
   * @param string $username
   *   The username to check.
   *
   * @return bool
   *   TRUE if exists, otherwise FALSE.
   */
  public function isUsernameTaken($username) {

    $query = $this->database->query("SELECT `uid`
FROM `users_field_data` 
WHERE `name` = :name",
      [':name' => $username]);
    $result = $query->fetchAll();

    $record = reset($result);

    return !empty($record);
  }

  /**
   * Gets a user by the username.
   *
   * @param string $username
   *   The username of the user to get.
   *
   * @return mixed
   *   NULL if no user found otherwise the SparxUserModel.
   */
  public function getByUsername($username) {

    $query = $this->database->query("SELECT `uid`
FROM `users_field_data` 
WHERE `name` = :name",
      [':name' => $username]);
    $result = $query->fetchAll();

    $record = reset($result);

    if (empty($record)) {
      return NULL;
    }
    else {
      return new SparxUserModel($record->uid);
    }
  }

  /**
   * Loads a SparxUserModel.
   *
   * @return \Drupal\sparx_library\SparxUserModel
   *   A user model loaded with the user data.
   */
  public function getById($uid) {
    return new SparxUserModel($uid);
  }

  /**
   * Gets a new SparxUserModel.
   *
   * @return \Drupal\sparx_library\SparxUserModel
   *   A blank user model.
   */
  public function getNew() {
    return new SparxUserModel();
  }

}
