<?php

namespace Drupal\sparx_library;

use Drupal\user\Entity\User;

use MMSoap;

use Drupal\Core\StringTranslation\StringTranslationTrait;

use Drupal\sparx_library\Traits\SparxAuditTrait;
use Drupal\sparx_library\Traits\SparxCryptTrait;
use Drupal\sparx_library\Traits\SparxSettingsTrait;

// Registration source types.
define('SPARX_USER_MODEL_REG_SOURCE_API', 1);
define('SPARX_USER_MODEL_REG_SOURCE_WEB', 0);

/**
 * Class SparxUserModel defines the user data model.
 */
class SparxUserModel {

  use StringTranslationTrait;

  use SparxAuditTrait;
  use SparxCryptTrait;
  use SparxSettingsTrait;

  protected $database;
  protected $sAudit;
  protected $sValid;

  protected $id = NULL;
  protected $username;
  protected $password = NULL;
  protected $email;
  protected $whoAreYou;
  protected $whoAreYouOther;
  protected $age;
  protected $ethnicGroup;
  protected $ethnicGroupOther;
  protected $gender;
  protected $emailAllow;
  protected $mobile;
  protected $txtAllow;
  protected $whereDoYouLive;
  protected $howDidYouFindOutAboutSparx;
  protected $howDidYouFindOutOther;
  protected $contactPermission;
  protected $regSource;
  protected $regSourceType;
  protected $regMobileUserAgent;
  protected $status;

  /**
   * Constructs the class.
   *
   * @param mixed $uid
   *   The User ID of the user to load or NULL if none.
   */
  public function __construct($uid = NULL) {

    // Setup database and services required.
    $this->database = \Drupal::database();
    $this->sAudit = \Drupal::service('sparx_audit_service');
    $this->sValid = \Drupal::service('sparx_validation_service');

    if (!is_null($uid)) {
      $this->loadUser($uid);
    }
  }

  /**
   * Sets the active status for the user (enabled/disabled).
   *
   * @param bool $status
   *   TRUE if active, else FALSE.
   */
  public function setActive($status) {
    $this->status = $status;
  }

  /**
   * Checks if the user is active.
   *
   * @return mixed
   *   TRUE if active, otherwise FALSE (NULL if not set).
   */
  public function isActive() {
    return $this->status;
  }

  /**
   * Sets the User ID of the user.
   *
   * @param int $id
   *   The User ID of the user.
   */
  public function setId($id) {
    $this->id = $id;
  }

  /**
   * Gets the User ID of the user.
   *
   * @return mixed
   *   The User ID of the user (NULL if not set).
   */
  public function getId() {
    return $this->id;
  }

  /**
   * Sets the username of the user.
   *
   * @param string $username
   *   The username of the user.
   */
  public function setUsername($username) {
    $this->username = $username;
  }

  /**
   * Gets the username of the user.
   *
   * @return mixed
   *   The username of the user (NULL if not set).
   */
  public function getUsername() {
    return $this->username;
  }

  /**
   * Sets the password of the user.
   *
   * @param string $password
   *   The password of the user.
   */
  public function setPassword($password) {
    $this->password = $password;
  }

  /**
   * Gets the password of the user.
   *
   * @return mixed
   *   The password of the user (NULL if not set).
   */
  private function getPassword() {
    return $this->password;
  }

  /**
   * Checks to see if the password has changed.
   *
   * @return bool
   *   TRUE if changed, else false.
   */
  private function hasPasswordChanged() {
    return !is_null($this->password);
  }

  /**
   * Sets the email of the user.
   *
   * @param string $email
   *   The email address of the user.
   */
  public function setEmail($email) {
    $this->email = $email;
  }

  /**
   * Gets the email of the user.
   *
   * @return string
   *   The email address of the user.
   */
  public function getEmail() {
    return $this->email;
  }

  /**
   * Sets the Who are you value of the user.
   *
   * @param string $whoAreYou
   *   The type of user.
   */
  public function setWhoAreYou($whoAreYou) {

    $this->whoAreYou = $whoAreYou;
  }

  /**
   * Gets the Who are you value of the user.
   *
   * @return string
   *   The type of user.
   */
  public function getWhoAreYou() {

    return $this->whoAreYou;
  }

  /**
   * Sets how the user found out about SPARX.
   *
   * @param string $howDidYouFindOutAboutSparx
   *   How the user found out about SPARX.
   */
  public function setHowDidYouFindOutAboutSparx($howDidYouFindOutAboutSparx) {

    $this->howDidYouFindOutAboutSparx = $howDidYouFindOutAboutSparx;
  }

  /**
   * Gets how the user found out about SPARX.
   *
   * @return string
   *   How the user found out about SPARX.
   */
  public function getHowDidYouFindOutAboutSparx() {

    return $this->howDidYouFindOutAboutSparx;
  }

  /**
   * Gets how the "other" variable for how the user found out about SPARX.
   *
   * @return string
   *   How the user found out about SPARX (other).
   */
  public function getHowDidYouFindOutOther() {

    return $this->howDidYouFindOutOther;
  }

  /**
   * Sets how the "other" variable for how the user found out about SPARX.
   *
   * @param string $howDidYouFindOutOther
   *   How the user found out about SPARX (other).
   */
  public function setHowDidYouFindOutOther($howDidYouFindOutOther) {

    $this->howDidYouFindOutOther = $howDidYouFindOutOther;
  }

  /**
   * Gets who the user is the "other" category.
   *
   * @return string
   *   How the user found out about SPARX (other).
   */
  public function getWhoAreYouOther() {

    return $this->whoAreYouOther;
  }

  /**
   * Sets who the user is the "other" category.
   *
   * @param string $whoAreYouOther
   *   How the user found out about SPARX (other).
   */
  public function setWhoAreYouOther($whoAreYouOther) {

    $this->whoAreYouOther = $whoAreYouOther;
  }

  /**
   * Gets the age of the user.
   *
   * @return int
   *   The age of the user.
   */
  public function getAge() {

    return $this->age;
  }

  /**
   * Sets the age of the user.
   *
   * @param int $age
   *   The age of the user.
   */
  public function setAge($age) {

    $this->age = $age;
  }

  /**
   * Gets the ethnic group of the user.
   *
   * @return string
   *   A comma seperated list of ethnic groups.
   */
  public function getEthnicGroup() {

    return $this->ethnicGroup;
  }

  /**
   * Sets the ethnic group of the user.
   *
   * @param string $ethnicGroup
   *   A comma seperated list of ethnic groups.
   */
  public function setEthnicGroup($ethnicGroup) {

    $this->ethnicGroup = $ethnicGroup;
  }

  /**
   * Sets the ethnic group value as a string from an array.
   *
   * @param array $ethnicGroup
   *   The array of values.
   * @param bool $bCompatibilityMode
   *   TRUE if needs compatibility (via API).
   *   FALSE if from the website Drupal-style.
   */
  public function setEthnicGroupArrStr(array $ethnicGroup, $bCompatibilityMode = FALSE) {

    if ($bCompatibilityMode) {
      $this->setEthnicGroup(implode(", ", array_values($ethnicGroup)));
    }
    else {
      $this->setEthnicGroup(implode(", ", array_keys($ethnicGroup)));
    }
  }

  /**
   * Sets the ethnic group other value.
   *
   * @param string $ethnicGroupOther
   *   The value of the other type for ethnic group.
   */
  public function setEthnicGroupOther($ethnicGroupOther) {

    $this->ethnicGroupOther = $ethnicGroupOther;
  }

  /**
   * Gets the ethnic group other value.
   *
   * @return string
   *   The value of the other type for ethnic group.
   */
  public function getEthnicGroupOther() {

    return $this->ethnicGroupOther;
  }

  /**
   * Sets the gender of the user.
   *
   * @param string $gender
   *   The gender of the user.
   */
  public function setGender($gender) {

    $this->gender = $gender;
  }

  /**
   * Gets the gender of the user.
   *
   * @return string
   *   The gender of the user.
   */
  public function getGender() {

    return $this->gender;
  }

  /**
   * Gets whether allowed to send emails.
   *
   * @return bool
   *   TRUE if OK, FALSE if not.
   */
  public function getEmailAllow() {

    return $this->emailAllow;
  }

  /**
   * Sets whether allowed to send emails.
   *
   * @param bool $emailAllow
   *   TRUE if OK, FALSE if not.
   */
  public function setEmailAllow($emailAllow) {

    $this->emailAllow = $emailAllow;
  }

  /**
   * Gets the mobile number of the user.
   *
   * @returns string
   *   The number number of the user (or empty string if none or invalid).
   */
  public function getMobile() {
    // Ensure retrieving the correct valid data.
    if (!strlen($this->getMobilePrefix()) || !strlen($this->getMobileRemaining())) {
      return '';
    }

    return $this->mobile;
  }

  /**
   * Gets the mobile number prefix of the user.
   *
   * @return string
   *   Returns the prefix if set (or empty string if none or invalid).
   */
  public function getMobilePrefix() {
    if (strlen($this->mobile)) {
      $prefix = substr($this->mobile, 0, 3);
      if (in_array($prefix, ['020', '021', '022', '027'])) {
        return $prefix;
      }
    }

    return '';
  }

  /**
   * Gets the mobile digits after the prefix.
   *
   * @return string
   *   The remaining digits if set (or empty string if none or invalid).
   */
  public function getMobileRemaining() {
    // Ensure retrieving the correct valid data.
    if (!strlen($this->getMobilePrefix())) {
      return '';
    }
    if (strlen($this->mobile) && (strlen($this->mobile) > 3)) {
      return substr($this->mobile, 3);
    }

    return '';
  }

  /**
   * Sets the mobile number.
   *
   * @param string $mobile
   *   The mobile number to set.
   */
  public function setMobile($mobile) {

    $this->mobile = $mobile;
  }

  /**
   * Gets whether allowed to send txt messages.
   *
   * @return bool
   *   TRUE if allowed, FALSE if not.
   */
  public function getTxtAllow() {

    return $this->txtAllow;
  }

  /**
   * Sets whether allowed to send txt messages.
   *
   * @param bool $txtAllow
   *   TRUE if allowed, FALSE if not.
   */
  public function setTxtAllow($txtAllow) {

    $this->txtAllow = $txtAllow;
  }

  /**
   * Gets where the user lives.
   *
   * @return string
   *   Where the user lives.
   */
  public function getWhereDoYouLive() {

    return $this->whereDoYouLive;
  }

  /**
   * Sets where the user lives.
   *
   * @param string $whereDoYouLive
   *   Where the user lives.
   */
  public function setWhereDoYouLive($whereDoYouLive) {

    $this->whereDoYouLive = $whereDoYouLive;
  }

  /**
   * Gets whether OK for contact permission.
   *
   * @return bool
   *   TRUE if OK, FALSE if not.
   */
  public function getContactPermission() {

    return $this->contactPermission;
  }

  /**
   * Sets whether OK for contact permission.
   *
   * @param bool $contactPermission
   *   TRUE if OK, FALSE if not.
   */
  public function setContactPermission($contactPermission) {

    $this->contactPermission = $contactPermission;
  }

  /**
   * Gets the registration source.
   *
   * @return int
   *   The registration source of the user.
   */
  public function getRegSource() {

    return $this->regSource;
  }

  /**
   * Sets the registration mobile user agent.
   *
   * @param string $regMobileUserAgent
   *   The registration mobile user agent.
   */
  public function setRegUserAgent($regMobileUserAgent) {

    $this->regMobileUserAgent = $regMobileUserAgent;
  }

  /**
   * Gets the registration mobile user agent.
   *
   * @return string
   *   The registration mobile user agent.
   */
  public function getRegUserAgent() {

    return $this->regMobileUserAgent;
  }

  /**
   * Sets the registration source.
   *
   * @param int $regSource
   *   The registration source of the user.
   */
  public function setRegSource($regSource) {

    $this->regSource = $regSource;
  }

  /**
   * Set the reg source type (saved) type (iOS/Android/unknown) for reading.
   *
   * @param mixed $regSourceType
   *   One of "ios", "android", or "null.
   */
  public function setRegSourceSaveType($regSourceType) {

    if (empty($regSourceType)) {
      $this->regSourceType = "unknown";
    }

    if (in_array($regSourceType, ['ios', 'android'])) {
      $this->regSourceType = $regSourceType;
    }
  }

  /**
   * Get the reg source type (saved) type for reading.
   *
   * @return mixed
   *   "ios", "android", or "null.
   */
  public function getRegSourceSaveType() {

    return empty($this->regSourceType) ? "unknown" : $this->regSourceType;
  }

  /**
   * Gets the mobile type (iOS/Andriod or NULL).
   *
   * @return string
   *   Return "ios", "andriod", or NULL.
   */
  public function getRegSourceType() {

    if (empty($this->regMobileUserAgent)) {
      return NULL;
    }

    $lowerUserAgent = strtolower($this->regMobileUserAgent);

    if (strpos($lowerUserAgent, "darwin") !== FALSE) {
      return "ios";
    }
    if (strpos($lowerUserAgent, "android") !== FALSE) {
      return "android";
    }

    return NULL;
  }

  /**
   * Loads the user from the database.
   *
   * @param int $uid
   *   The User ID of the user.
   *
   * @return bool
   *   TRUE (otherwise exception)
   */
  private function loadUser($uid) {

    $user = User::load($uid);
    if (empty($user)) {
      // We are getting this from Drupal - not supposed to be an error.
      // If a user is deleted, uid may exist and load the user.
      // Just in case will log it (user issue).
      $this->aLog(SA_USR_FAIL_TRACK, t("SparxUserModel - User uid @uid not found", ['@uid' => $uid]));
      return FALSE;
    }

    // Update Audit log stream with user details.
    $this->aSetUserId($uid);
    $this->aSetUsername($user->getUsername());

    // Set this object with the data from Drupal.
    $this->setId($uid);
    $this->setEmail($user->getEmail());
    $this->setUsername($user->getUsername());
    $this->setActive($user->isActive());

    // If it is an admin then doesn't have corresponding sparxuser data.
    $roles = $user->getRoles();
    $isAdmin = in_array('administrator', $roles);

    if (!$isAdmin) {
      // Get corresponding data for user as supposed to be in sparxuser.
      $query = $this->database->query("SELECT * FROM `sparxuser` WHERE uid = :id", [':id' => $uid]);
      $result = $query->fetchAll();
      $record = reset($result);
    }
    else {
      // No corresponding data for admins (below will ignore saves).
      $record = NULL;
    }

    if (empty($record)) {
      if (!$isAdmin) {
        // If we don't have the corresponding data then alert
        // as it will be able to update additional data.
        $this->aLog(SA_USR_FAIL_TRACK, t("SparxUserModel - User loaded with no corresponding sparxuser data"));
      }
    }
    else {
      // Set this object with the data from sparxuser.
      $this->setWhoAreYou($record->who_are_you);
      $this->setWhoAreYouOther($record->who_are_you_other);
      $this->setAge($record->age);
      $this->setEthnicGroup($record->ethnic_group);
      $this->setGender($record->gender);
      $this->setEmailAllow($record->email_allow ? TRUE : FALSE);
      $this->setMobile(strlen($record->mobile) ? $this->decrypt($record->mobile) : '');
      $this->setTxtAllow($record->txt_allow ? TRUE : FALSE);
      $this->setWhereDoYouLive($record->where_do_you_live);
      $this->setHowDidYouFindOutAboutSparx($record->how_did_you_find_out_about_sparx);
      $this->setHowDidYouFindOutOther($record->how_did_you_find_out_other);
      $this->setContactPermission($record->contact_permission ? TRUE : FALSE);
      $this->setRegSource($record->reg_source);
      $this->setRegSourceSaveType($record->reg_type);
    }

    // Always TRUE if we got here, exception if user not in Drupal.
    return TRUE;
  }

  /**
   * Saves the new or updated data in the database.
   *
   * @return mixed
   *   Return TRUE if success, otherwise text error message.
   */
  public function save() {

    // Check to see if we are updating or creating a new record.
    $uid = $this->getId();

    // Updating a record.
    if (!is_null($uid)) {

      // Make it as atomic as can be.
      $transaction = $this->database->startTransaction();

      try {
        // Save Drupal details to DB.
        $user = User::load($uid);
        $user->setEmail($this->getEmail());
        if ($this->hasPasswordChanged()) {
          $user->setPassword($this->getPassword());
        }
        $user->save();

        // Save additional information in sparxuser database.
        $this->database->update('sparxuser')
          ->fields([
            'mobile' => strlen($this->getMobile()) ? $this->encrypt($this->getMobile()) : '',
            'txt_allow' => $this->getTxtAllow() ? '1' : '0',
            'email_allow' => $this->getEmailAllow() ? '1' : '0',
          ])
          ->condition('uid', $uid, '=')
          ->execute();
      }
      catch (\Exception $e) {
        // Just didn't work so log error and return with user error.
        $transaction->rollback();
        watchdog_exception('sparx_library_sparxusermodel', $e);

        return t("We have had an unexpected error. Please notify us so we can address the problem.");
      }

      // Success.
      return TRUE;

    }
    // New user.
    else {

      // New user in back end try this first as single source of "truth".
      $apiResult = SparxAPI::registerUser($this->getUsername());
      if ($apiResult !== SparxAPI::REGISTER_SUCCESS) {
        if ($apiResult === SparxAPI::REGISTER_NAMETAKEN) {
          // Name already exists.
          return SparxAPI::REGISTER_NAMETAKEN;
        }
        else {
          // Something went wrong with the back end.
          return SparxAPI::REGISTER_FAILURE;
        }
      }

      // Make it as atomic as can be.
      $transaction = $this->database->startTransaction();

      try {
        // Add in Drupal.
        $user = User::create();

        // Mandatory settings.
        $user->setPassword($this->getPassword());
        $user->enforceIsNew();
        $user->setEmail($this->getEmail());
        $user->setUsername($this->getUsername());

        // Optional settings.
        $language = SPARX_CFG_DEFAULT_LANGUAGE;
        $user->set("init", 'email');
        $user->set("langcode", $language);
        $user->set("preferred_langcode", $language);
        $user->set("preferred_admin_langcode", $language);
        $user->activate();

        // Save user.
        $user->save();

        // Just created the user so is active.
        $this->setActive(TRUE);

        // Store elements into database.
        $this->database->insert('sparxuser')
          ->fields([
            'uid' => $user->id(),
            'who_are_you' => $this->getWhoAreYou(),
            'who_are_you_other' => $this->getWhoAreYouOther(),
            'age' => $this->getAge(),
            'ethnic_group' => $this->getEthnicGroup(),
            'ethnic_group_other' => $this->getEthnicGroupOther(),
            'gender' => $this->getGender(),
            'email_allow' => $this->getEmailAllow() ? 1 : 0,
            'mobile' => strlen($this->getMobile()) ? $this->encrypt($this->getMobile()) : '',
            'txt_allow' => $this->getTxtAllow() ? 1 : 0,
            'where_do_you_live' => $this->getWhereDoYouLive(),
            'how_did_you_find_out_about_sparx' => $this->getHowDidYouFindOutAboutSparx(),
            'how_did_you_find_out_other' => $this->getHowDidYouFindOutOther(),
            'contact_permission' => $this->getContactPermission() ? 1 : 0,
            'reg_source' => $this->getRegSource(),
            'reg_user_agent' => $this->getRegUserAgent(),
            'reg_type' => $this->getRegSourceType(),
          ])->execute();
      }
      catch (\Exception $e) {
        // Just didn't work so log error and return with user error.
        $transaction->rollback();
        watchdog_exception('sparx_library_sparxuser', $e);

        return t("We have had an unexpected error. Please notify us so we can address the problem.");
      }

      // Set the new user ID in this model.
      $this->setId($user->id());

      // Do the login for the user.
      $uid = \Drupal::service('user.auth')
        ->authenticate($this->getUsername(), $this->getPassword());

      // Finish the login in Drupal session mechanism.
      $dUser = \Drupal::entityManager()->getStorage('user')->load($uid);

      user_login_finalize($dUser);

      return TRUE;
    }
  }

  /**
   * Sends an SMS to the user.
   *
   * @param string $message
   *   The message to send.
   *
   * @return bool
   *   TRUE if sent, FALSE if error or not sent.
   */
  public function sendSms($message) {

    // Checks overrides set.
    if ($this->checkSmsOverride()) {
      $this->aLog(SA_USER_MODEL, t("Skipping sending sms due to overrides not set"));
      return FALSE;
    }

    // Get target mobile.
    $mobile = $this->getMobileWithOverride($this->getMobile());
    $newMessage = $this->getSmsMessageWithOverride($this->getUsername(), $message);

    // If no mobile then return FALSE.
    if (!strlen($mobile)) {
      $this->aLog(SA_USER_MODEL, t("Could not send SMS as no mobile number loaded"));
      return FALSE;
    }

    // If mobile is wrong length the return FALSE.
    if (!$this->sValid->validateMobileLength($mobile)) {
      $this->aLog(SA_USER_MODEL, t("Could not send SMS as mobile number is incorrect length (%length characters)", ["%length" => strlen($mobile)]));
      return FALSE;
    }

    // Get full mobile with country code.
    $fullMobile = $this->getMobileWithCountryCode($mobile);

    $this->aLog(SA_USER_MODEL, t("Sending SMS message: %message", ["%message" => $newMessage]));

    // Check overrides set for disabling SMS.
    if ($this->checkSmsDisableFlag()) {
      $this->aLog(SA_USER_MODEL, t("Skipping sending sms due to disable flag not set"));
      return FALSE;
    }
    // Don't do it if SMS is disabled.
    elseif ($this->isSmsDisabledFlag()) {
      $this->aLog(SA_USER_MODEL, t("Skipping sending sms as disable flag is set"));
      return FALSE;
    }

    // Create new MMSoap class (legacy).
    $soap = new MMSoap(SPARX_CFG_MM_USER, SPARX_CFG_MM_PWD, []);

    // Send message via SOAP.
    $response = $soap->sendMessages([$fullMobile], $newMessage);
    if (is_soap_fault($response)) {
      // Check for internal errors.
      $this->aLog(SA_USER_MODEL, t('Soap fault error'));
      return FALSE;
    }
    else {
      // Log response and return status.
      $result = $response->getResult();
      $this->aLogJson(SA_USER_MODEL, $result);
      return ($result->failed == 0);
    }
  }

}
