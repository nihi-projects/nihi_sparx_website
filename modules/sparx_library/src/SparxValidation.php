<?php

namespace Drupal\sparx_library;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

use Drupal\smart_ip\SmartIp;
use Drupal\smart_ip\SmartIpLocation;

use Symfony\Component\HttpFoundation\IpUtils;

/**
 * SparxValidation class for validating data in the system.
 */
class SparxValidation {

  use StringTranslationTrait;

  protected const MOBILE_PREFIX_LEN = 3;
  protected const MOBILE_PREFIX_LIST = ['020', '021', '022', '027'];

  protected $sparxUser;
  protected $smartIpLocation;

  /**
   * Constructs validation service.
   *
   * @param \Drupal\sparx_library\SparxUser $sparxUser
   *   The sparx user service.
   * @param \Drupal\smart_ip\SmartIpLocation $smartIpLocation
   *   The GEO location service.
   */
  public function __construct(SparxUser $sparxUser, SmartIpLocation $smartIpLocation) {

    $this->sparxUser = $sparxUser;
    $this->smartIpLocation = $smartIpLocation;
  }

  /**
   * Creates a static version of this class.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container to get the services.
   *
   * @return mixed
   *   The services needed for this class.
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this validation class.
    return new static(
    // Load the service required to construct this class.
      $container->get('sparx_user_service'),
      $container->get('smart_ip.smart_ip_location')
    );
  }

  /**
   * Checks to see if access is disabled geolocation-wise.
   *
   * @return bool
   *   Whether or not access should be disabled.
   */
  public function isIpDisabled() {

    if (SPARX_CFG_DISABLE_GEOLOCATION) {
      return FALSE;
    }

    $realIp = \Drupal::request()->getClientIp();

    if (!strlen($realIp) || empty($realIp)) {
      return TRUE;
    }

    if (!filter_var($realIp, FILTER_VALIDATE_IP)) {
      return TRUE;
    }

    // Range to exclude.
    $privateIPs = ["192.168.0.0/16",
      "172.16.0.0/12",
      "10.0.0.0/8",
      "130.216.0.0/16",
    ];

    // Check each IP address is excluded
    foreach ($privateIPs as $privateIP) {
      if (IpUtils::checkIp($realIp, $privateIP)) {
        return FALSE;
      }
    }

    return ($this->smartIpLocation->get('countryCode') != SPARX_CFG_GEOLOCATION_COUNTRY);
  }

  /**
   * Gets the length of the mobile prefix.
   *
   * @return int
   *   The length of the mobile prefix.
   */
  public function getMobilePrefixLength() {
    return self::MOBILE_PREFIX_LEN;
  }

  /**
   * Validates the length of a mobile number.
   *
   * @param string $mobile
   *   The mobile number.
   *
   * @return bool
   *   TRUE if within bounds, FALSE if not.
   */
  public function validateMobileLength($mobile) {
    return !(strlen($mobile) < $this->getMobileMinLength() || strlen($mobile) > $this->getMobileMaxLength());
  }

  /**
   * Gets the minimum length of a mobile number.
   *
   * @return int
   *   Minimum length of mobile number.
   */
  public function getMobileMinLength() {
    return SPARX_CFG_MOBILE_MIN_LENGTH;
  }

  /**
   * Gets the maximum length of a mobile number.
   *
   * @return int
   *   Maximum length of mobile number.
   */
  public function getMobileMaxLength() {
    return SPARX_CFG_MOBILE_MAX_LENGTH;
  }

  /**
   * Validates the username with error string.
   *
   * @param string $username
   *   The username to validate.
   * @param bool $bCompatibilityMode
   *   Whether to use backwards compatibility mode with previous system version.
   *   (Useful for when using the API, it returns a consistent response).
   *
   * @return mixed
   *   NULL if no error, or error string if error.
   */
  public function getValidateUsernameError($username, $bCompatibilityMode = FALSE) {

    // Check for errors via Drupal.
    $error = user_validate_name($username);

    // If no error return NULL.
    if (is_null($error)) {
      return NULL;
    }

    // If not in compatibility can return whatever.
    if (!$bCompatibilityMode) {
      return $error;
    }

    // Check for error consistency.
    if (in_array($error, [
      (string) t('You must enter a username'),
      (string) t('The username cannot contain multiple spaces in a row'),
      (string) t('The username contains an illegal character'),
      (string) t('The username %name is too long: it must be %max characters or less.', [
        '%name' => $username,
        '%max' => USERNAME_MAX_LENGTH,
      ]),
    ])) {
      // Return for backwards compatibility.
      return $error;
    }
    else {
      // Compatibility error.
      return (string) t('The username contains an illegal character.');
    }
  }

  /**
   * Checks if an email address is already taken by another user.
   *
   * (Can be used when changing email address to check not taken).
   *
   * @param string $email
   *   The proposed email address.
   * @param int $currentUserId
   *   The user to compare to (e.g. current user).
   *   NULL if no user.
   *
   * @return bool
   *   TRUE if taken, FALSE if available.
   */
  public function isEmailTaken($email, $currentUserId = NULL) {

    $loadedId = $this->sparxUser->isEmailTaken($email);
    if (!$loadedId) {
      // Nothing found.
      return FALSE;
    }

    // Found and no user signed in, then already taken.
    if (is_null($currentUserId)) {
      return TRUE;
    }

    // Found, and a different user to that logged in, so taken.
    if ($currentUserId != $loadedId) {
      return TRUE;
    }

    // Not taken.
    return FALSE;
  }

  /**
   * Validates and email address.
   *
   * @param string $email
   *   The email address to check.
   *
   * @return bool
   *   TRUE if valid, FALSE if not valid.
   */
  public function validateEmail($email) {
    return \Drupal::service('email.validator')->isValid($email);
  }

  /**
   * Gets the invalid email string got compatibility (with API).
   *
   * (This needs to be transformed after in the API).
   *
   * @param string $email
   *   The email address to insert in the string.
   *
   * @return string
   *   The string with the error message.
   */
  public function invalidEmailCompatibilityStr($email) {
    // For backwards compatibility.
    return t('The e-mail address %mail is not valid.', [
      '%mail' => $email,
    ]);
  }

  /**
   * Validates a mobile number (digits).
   *
   * @param string $mobile
   *   The number number.
   *
   * @return bool
   *   TRUE if valid, FALSE if not.
   */
  public function validateMobile($mobile) {
    return ctype_digit($mobile);
  }

  /**
   * Validates a mobile prefix.
   *
   * @param string $mobilePrefix
   *   The prefix of the mobile number.
   *
   * @return bool
   *   TRUE if valid, FALSE if not.
   */
  public function validateMobilePrefix($mobilePrefix) {
    return in_array($mobilePrefix, self::MOBILE_PREFIX_LIST);
  }

  /**
   * Retrieves the list of valid mobile prefixes.
   *
   * @return string
   *   Comma seperated list of prefixes.
   */
  public function getValidMobilePrefixList() {
    return implode(", ", self::MOBILE_PREFIX_LIST);
  }

  /**
   * Checks to see if a username is taken.
   *
   * @param string $username
   *   The username to check.
   *
   * @return bool
   *   TRUE if taken, FALSE if not.
   */
  public function isUsernameTaken($username) {

    return $this->sparxUser->isUsernameTaken($username);
  }

  /**
   * Validates the Who are you user input data.
   *
   * @param string $whoAreYou
   *   The Who are you value to check.
   *
   * @return bool
   *   TRUE if valid, FALSE if not.
   */
  public function validateWhoAreYou($whoAreYou) {
    return in_array($whoAreYou, [
      'youngperson',
      'familymember',
      'healthprofessional',
      'other',
    ]);
  }

  /**
   * Checks if the Who are you value is "other".
   *
   * @param string $whoAreYou
   *   The Who are you value to check.
   *
   * @return bool
   *   TRUE if "other", FALSE if not.
   */
  public function isWhoAreYouOther($whoAreYou) {
    return (strcmp($whoAreYou, 'other') === 0);
  }

  /**
   * Checks if the Who are you value is a young person.
   *
   * @param string $whoAreYou
   *   The Who are you value to check.
   *
   * @return bool
   *   TRUE if is a young person, FALSE if not.
   */
  public function isYoungPerson($whoAreYou) {
    return (strcmp($whoAreYou, 'youngperson') === 0);
  }

  /**
   * Checks to see if Age if all digits.
   *
   * @param string $age
   *   The age of the user.
   *
   * @return bool
   *   TRUE if valid, FALSE if not.
   */
  public function validateAge($age) {
    return ctype_digit($age);
  }

  /**
   * Validates the ethnicity of the user.
   *
   * @param array $ethnicityGroup
   *   An array of the ethnicity groups.
   * @param bool $bCompatibilityMode
   *   Whether to use compatibility mode
   *   (API arrays are different than normal Drupal webforms).
   *
   * @return bool
   *   TRUE if valid, FALSE if not.
   */
  public function validateEthnicity(array $ethnicityGroup, $bCompatibilityMode = FALSE) {

    // Array of valid ethnicity groups.
    $validGroups = [
      'nzeuropean',
      'maori',
      'samoan',
      'cookislandmaori',
      'tongan',
      'niuean',
      'chinese',
      'indian',
      'other',
    ];

    if ($bCompatibilityMode) {
      // Arrays are slightly different from API as below.
      foreach ($ethnicityGroup as $ethnicity) {
        // Ensure each element is valid.
        if (!in_array($ethnicity, $validGroups)) {
          return FALSE;
        }
      }
      // Check total at least one is selected.
      return count($ethnicityGroup) ? TRUE : FALSE;
    }
    else {
      // Check keys are correct and at least one is selected.
      $isSelected = FALSE;
      foreach ($ethnicityGroup as $ethnicity => $selectedValue) {
        // Ensure each element is valid.
        if (!in_array($ethnicity, $validGroups)) {
          return FALSE;
        }
        // Check total at least one is selected.
        if ($selectedValue !== 0) {
          $isSelected = TRUE;
        }
      }
      // Returns whether we found one selected.
      return $isSelected;
    }
  }

  /**
   * Checks to see if ethnicity is selected as "other".
   *
   * The API doesn't have this as required, but the register form does.
   * (So this is only applicable to the register form).
   *
   * @param string $ethnicityGroup
   *   The ethnicity array of selected variables.
   *
   * @return bool
   *   TRUE if selected, FALSE if not.
   */
  public function isEthnicityOther($ethnicityGroup) {
    return (array_key_exists('other', $ethnicityGroup) && ($ethnicityGroup['other'] !== 0));
  }

  /**
   * Validates the gender of the user (when set).
   *
   * @param string $gender
   *   The gender of the user.
   *
   * @return bool
   *   TRUE if valid, FALSE if not.
   */
  public function validateGender($gender) {
    return in_array($gender, [
      'male',
      'female',
      'transgender',
      'intersex',
      'undisclosed',
    ]);
  }

  /**
   * Validates the region of the user (when set).
   *
   * @param string $region
   *   The region of the user.
   *
   * @return bool
   *   TRUE if valid, FALSE if not.
   */
  public function validateRegion($region) {
    return in_array($region, [
      'northland',
      'auckland',
      'waikato',
      'bay_of_plenty',
      'gisborne',
      'hawkes_bay',
      'taranaki',
      'manawatu_wanganui',
      'wellington',
      'tasman_nelson',
      'marlborough',
      'west_coast',
      'canterbury',
      'otago',
      'southland',
    ]);
  }

  /**
   * Validates the how did you find out user value (when set).
   *
   * @param string $findOut
   *   The find out value of the user.
   *
   * @return bool
   *   TRUE if value, FALSE if not.
   */
  public function validateFindOut($findOut) {
    return in_array($findOut, [
      'counsellor',
      'doctor_nurse',
      'school',
      'youth_worker',
      'friend',
      'facebook',
      'google',
      'advertisement',
      'media',
      'other',
    ]);
  }

  /**
   * Checks to see if the user has "other" as the find out value.
   *
   * @param string $findOut
   *   The find out value of the user.
   *
   * @return bool
   *   TRUE if "other", FALSE if not.
   */
  public function isFindOutOther($findOut) {
    return (strcmp($findOut, 'other') === 0);
  }

  /**
   * Validates the email allow value supplied to the API.
   *
   * @param string $emailAllow
   *   The value supplied by the API user registration.
   *
   * @return bool
   *   TRUE if within valid range, FALSE if not.
   */
  public function validateApiEmailAllow($emailAllow) {
    return $this->validateYesNo($emailAllow);
  }

  /**
   * Checks if email allow flag is set as TRUE.
   *
   * (API uses a different method than the website).
   *
   * @param string $emailAllow
   *   The value supplied by the API user registration.
   *
   * @return bool
   *   TRUE if yes, FALSE if no.
   */
  public function isApiEmailAllow($emailAllow) {
    return $this->confirmYes($emailAllow);
  }

  /**
   * Validates the contact permission allow value supplied to the API.
   *
   * @param string $research
   *   The value supplied by the API user registration.
   *
   * @return bool
   *   TRUE if within valid range, FALSE if not.
   */
  public function validateApiContactPermission($research) {
    return $this->validateYesNo($research);
  }

  /**
   * Checks if contact permission flag is set as TRUE.
   *
   * (API uses a different method than the website).
   *
   * @param string $research
   *   The value supplied by the API user registration.
   *
   * @return bool
   *   TRUE if yes, FALSE if no.
   */
  public function isApiContactPermission($research) {
    return $this->confirmYes($research);
  }

  /**
   * Validates API functions used within this class.
   *
   * @param string $input
   *   The value to check (internally).
   *
   * @return bool
   *   TRUE if within valid range, FALSE if not.
   */
  private function validateYesNo($input) {
    return in_array($input, ['Y', 'N']);
  }

  /**
   * Checks API functions used within this class is TRUE.
   *
   * @param string $input
   *   The value to check as set.
   *
   * @return bool
   *   TRUE if yes, FALSE if no.
   */
  private function confirmYes($input) {
    return (strcmp($input, 'Y') === 0);
  }

  /**
   * Checks if a string doesn't have any length.
   *
   * @param string $str
   *   The string to check.
   *
   * @return bool
   *   TRUE if no length, FALSE if has length.
   */
  public function noLength($str) {
    return (strlen($str) === 0);
  }

  /**
   * Checks if a string has a length.
   *
   * @param string $str
   *   The string to check.
   *
   * @return bool
   *   TRUE if length, FALSE if no length.
   */
  public function hasLength($str) {
    return (strlen($str) > 0);
  }

  /**
   * Checks if an input is an array.
   *
   * @param array $arr
   *   The "array" to verify.
   *
   * @return bool
   *   TRUE if array, FALSE if not.
   */
  public function notArray(array $arr) {
    return !is_array($arr);
  }

  /**
   * Checks if an array is empty.
   *
   * @param array $arr
   *   The "array" to check.
   *
   * @return bool
   *   TRUE if empty, FALSE if not.
   */
  public function arrayEmpty(array $arr) {
    return (count($arr) === 0);
  }

}
