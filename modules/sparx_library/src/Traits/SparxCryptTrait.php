<?php

namespace Drupal\sparx_library\Traits;

use Drupal\encrypt\Entity\EncryptionProfile;

/**
 * Using this trait will add encryption/decryption methods to the class.
 */
trait SparxCryptTrait {

  /**
   * The encryption profile we will use loaded once.
   *
   * @var encryptionProfile
   */
  protected $encryptionProfile;

  /**
   * Load the encryption profile used in the system.
   */
  protected function loadEncryptionProfile() {
    $this->encryptionProfile = EncryptionProfile::load(SPARX_CFG_ENCRYPT_PROFILE);
  }

  /**
   * Decrypts a string using the encryption profile.
   *
   * @param string $string
   *   The string to decrypt.
   *
   * @return string
   *   The decrypted string.
   */
  protected function decrypt($string) {

    if (!$this->encryptionProfile) {
      $this->loadEncryptionProfile();
    }
    return \Drupal::service('encryption')->decrypt($string, $this->encryptionProfile);
  }

  /**
   * Encrypts a string using the encryption profile.
   *
   * @param string $string
   *   The string to encrypt.
   *
   * @return string
   *   The encrypted string.
   */
  protected function encrypt($string) {

    if (!$this->encryptionProfile) {
      $this->loadEncryptionProfile();
    }
    return \Drupal::service('encryption')->encrypt($string, $this->encryptionProfile);
  }

}
