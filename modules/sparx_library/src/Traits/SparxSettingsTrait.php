<?php

namespace Drupal\sparx_library\Traits;

/**
 * Using this trait will add simple settings and functions.
 */
trait SparxSettingsTrait {

  /**
   * Checks to make sure email and SMS overrides are available.
   *
   * @return bool
   *   TRUE if available, FALSE if not.
   */
  protected function checkEmailSmsOverrides() {
    return (!defined('SPARX_CFG_EMAIL_TO_OVERRIDE') || !defined('SPARX_CFG_SMS_TO_OVERRIDE'));
  }

  /**
   * Checks to make sure SMS override are available.
   *
   * @return bool
   *   TRUE if available, FALSE if not.
   */
  protected function checkSmsOverride() {
    return (!defined('SPARX_CFG_SMS_TO_OVERRIDE'));
  }

  /**
   * Checks to make sure SMS disable flag is available.
   *
   * @return bool
   *   TRUE if available, FALSE if not.
   */
  protected function checkSmsDisableFlag() {
    return (!defined('SPARX_CFG_DISABLE_SMS'));
  }

  /**
   * Checks to make sure system alert email is available.
   *
   * @return bool
   *   TRUE if available, FALSE if not.
   */
  protected function checkAlertSysEmail() {
    return (!defined('SPARX_CFG_ALERT_SYS_EMAIL') || !strlen('SPARX_CFG_ALERT_SYS_EMAIL'));
  }

  /**
   * Checks to make sure system application alert email is available.
   *
   * @return bool
   *   TRUE if available, FALSE if not.
   */
  protected function checkAlertAppEmail() {
    return (!defined('SPARX_CFG_ALERT_APP_EMAIL') || !strlen('SPARX_CFG_ALERT_APP_EMAIL'));
  }

  /**
   * Returns a mobile number or override mobile if set.
   *
   * @return string
   *   Mobile number.
   */
  protected function getMobileWithOverride($mobile) {
    return strlen(SPARX_CFG_SMS_TO_OVERRIDE) ? SPARX_CFG_SMS_TO_OVERRIDE : $mobile;
  }

  /**
   * Returns a Sms message modified if override set.
   *
   * @return string
   *   Mobile number.
   */
  protected function getSmsMessageWithOverride($username, $message) {
    return strlen(SPARX_CFG_SMS_TO_OVERRIDE) ? ('[' . $username . '] ' . $message) : $message;
  }

  /**
   * Returns an email address or override email if set.
   *
   * @return string
   *   Email address.
   */
  protected function getEmailWithOverride($email) {

    return strlen(SPARX_CFG_EMAIL_TO_OVERRIDE) ? SPARX_CFG_EMAIL_TO_OVERRIDE : $email;

  }

  /**
   * Takes an existing mobile number and puts the country prefix in front of it.
   *
   * @param string $mobile
   *   The mobile number to convert.
   *
   * @return string
   *   Processed mobile number.
   */
  protected function getMobileWithCountryCode($mobile) {
    return (string) SPARX_CFG_MOBILE_COUNTRY_PREFIX . (string) substr((string) $mobile, 1);
  }

  /**
   * Checks to see if SMS is disabled.
   *
   * @return bool
   *   TRUE if SMS is disabled, FALSE if not.
   */
  protected function isSmsDisabledFlag() {
    return SPARX_CFG_DISABLE_SMS;
  }

  /**
   * Checks to see if emails are disabled.
   *
   * @return bool
   *   TRUE if EMAIL is disabled, FALSE if not.
   */
  protected function isEmailDisabledFlag() {
    return SPARX_CFG_DISABLE_AUTO_EMAIL;
  }

  /**
   * Checks to see if we need to log everything for testing.
   *
   * Mainly used to check if we log sensitive information or not.
   *
   * @return bool
   *   TRUE if log all, FALSE if don't log all.
   */
  protected function isLogAllTestingFlag() {
    return SPARX_CFG_LOG_ALL_TESTING;
  }

  /**
   * Gets system  alert email or override email if set.
   *
   * @return string
   *   Email address to use.
   */
  protected function getSysAlertEmailWithOverride() {
    return strlen(SPARX_CFG_EMAIL_TO_OVERRIDE) ? SPARX_CFG_EMAIL_TO_OVERRIDE : SPARX_CFG_ALERT_SYS_EMAIL;
  }

  /**
   * Gets app alert email or override email if set.
   *
   * @return string
   *   Email address to use.
   */
  protected function getAppAlertEmailWithOverride() {

    return strlen(SPARX_CFG_EMAIL_TO_OVERRIDE) ? SPARX_CFG_EMAIL_TO_OVERRIDE : SPARX_CFG_ALERT_APP_EMAIL;
  }

  /**
   * Gets the username to use for the game or override if set.
   *
   * This is mainly used in testing.
   *
   * @return string
   *   Username to use.
   */
  protected function getGameUsernameWithOverride($username) {

    if ((NULL !== SPARX_CFG_GAME_TEST_USERNAME) && strlen(SPARX_CFG_GAME_TEST_USERNAME)) {
      return SPARX_CFG_GAME_TEST_USERNAME;
    }
    else {
      return $username;
    }
  }

  /**
   * Sends the welcome email to a user.
   *
   * This is used when a new user registers via web or app.
   *
   * @param string $username
   *   The username of the new user.
   * @param string $email
   *   The email of the new user.
   */
  protected function sendWelcomeEmail($username, $email) {

    // Send welcome email.
    $mailManager = \Drupal::service('plugin.manager.mail');

    $mailManager->mail('sparx_email',
      'sparx_template',
      $username . ' <' . $email . '>',
      SPARX_CFG_DEFAULT_LANGUAGE,
      [
        'username' => $username,
        'subject' => 'SPARX registration',
        'content_template' => 'sparx-content-template-registration',
      ]);
  }

}
