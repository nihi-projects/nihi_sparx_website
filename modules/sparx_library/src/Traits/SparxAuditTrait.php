<?php

namespace Drupal\sparx_library\Traits;

/**
 * Using this trait will add Audit log function prefixed with "a" to the class.
 */
trait SparxAuditTrait {

  /**
   * {@inheritdoc}
   */
  protected function aLog($type, $messages) {

    \Drupal::service('sparx_audit_service')->log($type, $messages);
  }

  /**
   * {@inheritdoc}
   */
  protected function aLogJson($type, $data, $description = '') {

    \Drupal::service('sparx_audit_service')->logJson($type, $data, $description);
  }

  /**
   * {@inheritdoc}
   */
  protected function aLogMessageJson($type, $message, $data, $description = '') {

    \Drupal::service('sparx_audit_service')->logMessageJson($type, $message, $data, $description);
  }

  /**
   * {@inheritdoc}
   */
  protected function aSetUserId($uid) {

    \Drupal::service('sparx_audit_service')->setUserId($uid);
  }

  /**
   * {@inheritdoc}
   */
  protected function aSetUsername($username) {

    \Drupal::service('sparx_audit_service')->setUsername($username);
  }

  /**
   * {@inheritdoc}
   */
  protected function aSetUser($uid, $username) {

    \Drupal::service('sparx_audit_service')->setUser($uid, $username);
  }

  /**
   * {@inheritdoc}
   */
  protected function aResetUser() {

    \Drupal::service('sparx_audit_service')->resetUser();
  }

}
