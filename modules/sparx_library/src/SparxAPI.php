<?php

namespace Drupal\sparx_library;

use Drupal\Component\Serialization\Json;

/**
 * Static class for communicating with the back end API.
 */
class SparxAPI {

  /*
   * Constants for responses from the server.
   */
  public const REGISTER_SUCCESS = 'Success';
  public const REGISTER_FAILURE = 'Failed';
  public const REGISTER_NAMETAKEN = 'Name taken';

  /**
   * Get the events from the back end API (mood events).
   *
   * @param int $starttime
   *   The unix timestamp to get events from.
   * @param int $endtime
   *   The unix timestamp to get events to.
   *
   * @return mixed
   *   Returns NULL if error, or array of events if events found.
   */
  public static function getEvents($starttime, $endtime) {

    // Get the logger service.
    $sAudit = \Drupal::service('sparx_audit_service');

    $sAudit->log(SA_INTERNAL_API, t('Requesting events from @starttime to @endtime', [
      '@starttime' => $starttime,
      '@endtime' => $endtime,
    ]));

    // The endpoint for getting events.
    $url = SPARX_CFG_API_ROOT . 'sparxer/moodEvents?apiKey=' . SPARX_CFG_API_KEY . '&starttimestamp=' . $starttime . '&endtimestamp=' . $endtime;

    try {
      // Request events using JSON and turn off errors
      // as we will handle non 200 responses ourselves.
      $response = \Drupal::httpClient()->get($url, [
        'http_errors' => FALSE,
        'headers' => ['Content-Type' => 'application/json; charset=utf-8'],
      ]);

      $sAudit->log(SA_INTERNAL_API, t('Received events response status code @code', ['@code' => $response->getStatusCode()]));

      // Successful response.
      if ($response->getStatusCode() >= 200 || $response->getStatusCode() < 300) {
        // Get response body and decode.
        $data = $response->getBody();
        $received = Json::decode($data);
        // Check if empty - then return empty array.
        if (empty($received)) {
          $sAudit->log(SA_INTERNAL_API, t('No events received'));
          return [];
        }
        // Received events.
        $numReceived = count($received);
        $sAudit->log(SA_INTERNAL_API, t('Received @num_events events', ['@num_events' => $numReceived]));

        // Cycle through the events and extract information we need.
        $events = [];
        for ($event_cnt = 0; $event_cnt < $numReceived; $event_cnt++) {
          $sAudit->logJson(SA_INTERNAL_API, $received[$event_cnt]);
          $events[] = [
            // We need username, answer to Q9 (self harm question)
            // and score (total of responses to questions.
            'username' => $received[$event_cnt]['sparxer'],
            'Q9' => $received[$event_cnt]['answers'][8],
            'score' => array_sum($received[$event_cnt]['answers']),
          ];
        }

        // Finish with cleaned up events array.
        return $events;
      }
      else {
        // Response had a server error.
        $sAudit->log(SA_INTERNAL_API, t('Error getting mood events'));
        $sAudit->logJson(SA_SYS_FAIL_TRACK, $response->getBody());
        return NULL;
      }
    }
    catch (\Exception $e) {
      // Hopefully we don't get this, log so we can figure out what it is.
      $sAudit->log(SA_INTERNAL_API, t('Exception requesting mood events'));
      $sAudit->logJson(SA_SYS_FAIL_TRACK, $e);
      watchdog_exception('sparx_library_sparxapi', $e);
      return NULL;
    }
  }

  /**
   * Registers a user to the back end API.
   *
   * @param string $userID
   *   The username of the user to add to the back end.
   *
   * @return string
   *   Returns one of the error code strings const above.
   */
  public static function registerUser($userID) {

    // Get the logger service.
    $sAudit = \Drupal::service('sparx_audit_service');
    $sAudit->setUsername($userID);

    $sAudit->log(SA_INTERNAL_API, t('Requesting user register'));

    // Format the registration data to send.
    $userArr = ["name" => $userID];

    // Setup endpoint for creating new users.
    $url = SPARX_CFG_API_ROOT . 'sparxer/add?apiKey=' . SPARX_CFG_API_KEY;

    try {
      // Send the data (JSON), turn off http errors as we will handle
      // responses ourselves.
      $response = \Drupal::httpClient()->post($url, [
        'http_errors' => FALSE,
        'body' => Json::encode($userArr),
        'headers' => [
          'Content-Type' => 'application/json; charset=utf-8',
        ],
      ]);

      $sAudit->log(SA_INTERNAL_API, t('Received register response status code @code', [
        '@code' => $response->getStatusCode(),
      ]));

      $sAudit->log(SA_INTERNAL_API, t('Received register user response'));

      // Process the status if we received an error response.
      if ($response->getStatusCode() < 200 || $response->getStatusCode() >= 300) {
        // Extract body from content.
        $data = $response->getBody();
        // Validate it is not empty, if it is then error.
        if (empty($data)) {
          $sAudit->log(SA_INTERNAL_API, t('Received empty response'));
          $sAudit->log(SA_SYS_FAIL_TRACK, t('Received empty register response'));
          return self::REGISTER_FAILURE;
        }
        // Decode the data from JSON.
        $json = Json::decode($data);
        // Check errors decoding JSON, e.g. malformed response.
        if (is_null($json)) {
          $sAudit->log(SA_INTERNAL_API, t('Invalid register response data'));
          $sAudit->logJson(SA_SYS_FAIL_TRACK, $data);
          return self::REGISTER_FAILURE;
        }
        // Get error message if already exists.
        if (isset($json['details']['messages']['name'][0])
          && ($json['details']['messages']['name'][0] == 'That name is already taken.')) {
          $sAudit->log(SA_INTERNAL_API, t('Name already taken'));
          return self::REGISTER_NAMETAKEN;
        }
        else {
          // If anything else then assume malformed.
          $sAudit->log(SA_INTERNAL_API, t('Malformed response'));
          $sAudit->logJson(SA_SYS_FAIL_TRACK, $data);
          return self::REGISTER_FAILURE;
        }
      }
      else {
        // Received OK response so success.
        $sAudit->log(SA_INTERNAL_API, t('Successful registration'));
        return self::REGISTER_SUCCESS;
      }
    }
    catch (\Exception $e) {
      // Something went wrong on our side or the API side.
      $sAudit->log(SA_INTERNAL_API, t('Exception registering user'));
      $sAudit->logJson(SA_SYS_FAIL_TRACK, $e);
      watchdog_exception('sparx_library_sparxapi', $e);
      return self::REGISTER_FAILURE;
    }
  }

  /**
   * Gets the token (session) for the user from the API.
   *
   * @param string $userID
   *   The user ID of the user to get.
   *
   * @return mixed
   *   FALSE if error, otherwise the token (session).
   */
  public static function getUserToken($userID) {

    // Get the logger service.
    $sAudit = \Drupal::service('sparx_audit_service');
    $sAudit->setUsername($userID);

    $sAudit->log(SA_INTERNAL_API, t('Requesting user token'));

    // Setup endpoint for getting a token (session).
    $url = SPARX_CFG_API_ROOT . 'sparxer/' . rawurlencode($userID) . '/getToken?apiKey=' . SPARX_CFG_API_KEY;

    try {
      // Setup HTTP client, either receives success or failure.
      $response = \Drupal::httpClient()
        ->get($url, [
          'http_errors' => FALSE,
          'headers' => [
            'Accept' => 'application/json; charset=utf-8',
          ],
        ]);
      $sAudit->log(SA_INTERNAL_API, t('Received token response status code @code', [
        '@code' => $response->getStatusCode(),
      ]));

      // Extract body from response.
      $data = $response->getBody();
      if (empty($data)) {
        // If empty must be an error.
        $sAudit->log(SA_SYS_FAIL_TRACK, t('No token data returned'));
        return FALSE;
      }

      // Decode the JSON.
      $json = Json::decode($data);
      if (is_null($json)) {
        // If NULL means malformed response.
        $sAudit->log(SA_INTERNAL_API, t('Invalid token response data'));
        $sAudit->logJson(SA_SYS_FAIL_TRACK, $data);

        return FALSE;
      }

      // Return token if we have it.
      if (isset($json['token']['token'])) {
        return $json['token']['token'];
      }
      else {
        // Anything else means we have an error.
        $sAudit->log(SA_INTERNAL_API, t('No token returned'));
        $sAudit->logJson(SA_SYS_FAIL_TRACK, $data);

        return FALSE;
      }
    }
    catch (\Exception $e) {
      // If something else happened failure somewhere so log and return FALSE.
      $sAudit->log(SA_INTERNAL_API, t('Exception requesting token'));
      $sAudit->logJson(SA_SYS_FAIL_TRACK, $e);
      watchdog_exception('sparx_library_sparxapi', $e);

      return FALSE;
    }
  }

  /**
   * Gets the level the user is on from the API.
   *
   * @param string $userID
   *   The username of the user.
   *
   * @return int
   *   FALSE if error, otherwise the level the user is on.
   */
  public static function getUserLevel($userID) {

    // Get logger service.
    $sAudit = \Drupal::service('sparx_audit_service');

    // Setup username which we think we know.
    $sAudit->setUsername($userID);

    $sAudit->log(SA_INTERNAL_API, t('Requesting user level'));

    // Set endpoint for getting the current user level.
    $url = SPARX_CFG_API_ROOT . 'sparxer/' . rawurlencode($userID) . '/getCurrentLevel?apiKey=' . SPARX_CFG_API_KEY;

    try {
      // Setup HTTP client
      // We don't need HTTP errors off as is either success or failure.
      $response = \Drupal::httpClient()
        ->get($url, ['headers' => ['Accept' => 'application/json; charset=utf-8']]);

      $sAudit->log(SA_INTERNAL_API, t('Received user level response status code @code', [
        '@code' => $response->getStatusCode(),
      ]));

      // Get body and check to make sure have something.
      $data = $response->getBody();
      if (empty($data)) {
        $sAudit->log(SA_SYS_FAIL_TRACK, t('No user level data returned'));
        return FALSE;
      }

      // Decode JSON, error if not valid.
      $decoded = Json::decode($data);
      if (is_null($decoded)) {
        $sAudit->log(SA_INTERNAL_API, t('Invalid user level response data'));
        $sAudit->logJson(SA_SYS_FAIL_TRACK, $data);
      }
    }
    catch (\Exception $e) {
      // Any exception likely means server had an error.
      $sAudit->log(SA_INTERNAL_API, t('Exception requesting user level'));
      $sAudit->logJson(SA_SYS_FAIL_TRACK, $response->getBody());
      watchdog_exception('sparx_library_sparxapi', $e);
      return FALSE;
    }

    if (isset($decoded['gameLevel'])) {
      $level = $decoded['gameLevel'];
      $sAudit->log(SA_INTERNAL_API, t('Found game level @level', ['@level' => $level]));
      // Return response if valid JSON.
      return $level;
    }
    else {
      // Must have been an error, so log failure.
      $sAudit->log(SA_INTERNAL_API, t('No level returned'));
      $sAudit->logJson(SA_SYS_FAIL_TRACK, $data);
      return FALSE;
    }
  }

  /**
   * Gets the root URL for the game to access the API directly.
   *
   * @return string
   *   The URL of the game API.
   */
  public static function getGameApiRoot() {
    return SPARX_CFG_API_ROOT . 'game/';
  }

}
