<?php

/**
 * @file
 * Module file for Sparx Messages.
 */

// Initialization values when not set (migration).
define('SPARX_SYSTEM_MESSAGES_NEXT_SPAM_CHECK_TIMESTAMP_INIT', 0);
define('SPARX_SYSTEM_MESSAGES_NEXT_SYSTEM_CHECK_TIMESTAMP_INIT', 0);
define('SPARX_SYSTEM_MESSAGES_NEXT_INTERNAL_CHECK_TIMESTAMP_INIT', 0);

// SPAM.
// Time between spam checks (seconds).
define('SPARX_SYSTEM_MESSAGES_SPAM_CHECK_FREQUENCY', 60 * 15);
// The minimum number of messages to check over past period.
define('SPARX_SYSTEM_MESSAGES_SPAM_FREQUENCY_COUNT', 8);
// define('SPARX_SYSTEM_MESSAGES_SPAM_FREQUENCY_COUNT', 1);
// Period in the past to check over for spam (seconds).
define('SPARX_SYSTEM_MESSAGES_SPAM_CHECK_PAST_PERIOD', 60 * 60 * 24);

// SYSTEM FAILURE.
// Time between system checks (seconds).
define('SPARX_SYSTEM_MESSAGES_FAILURE_CHECK_FREQUENCY', 60 * 15);
// Time between system checks when found error (seconds).
define('SPARX_SYSTEM_MESSAGES_FAILURE_CHECK_ERROR_FREQUENCY', 60 * 60 * 24);
// The minimum number of messages to check over past period.
define('SPARX_SYSTEM_MESSAGES_FAILURE_FREQUENCY_COUNT', 11);
// define('SPARX_SYSTEM_MESSAGES_FAILURE_FREQUENCY_COUNT', 1);
// Period in the past to check over for system failures (seconds).
define('SPARX_SYSTEM_MESSAGES_FAILURE_CHECK_PAST_PERIOD', 60 * 60 * 24);

// INTERNAL ERROR.
// Time between system checks (seconds).
define('SPARX_SYSTEM_MESSAGES_INTERNAL_CHECK_FREQUENCY', 60 * 15);
// Period in the past to check over for internal errors (seconds).
define('SPARX_SYSTEM_MESSAGES_INTERNAL_CHECK_PAST_PERIOD', 60 * 60 * 24);

/**
 * Implements hook_cron().
 */
function sparx_system_messages_cron() {

  // Get logger service.
  $sAudit = \Drupal::service('sparx_audit_service');

  $sAudit->log(SA_SYS_MSG_CRON, t('Cron run'));

  if (!sparx_library_is_cron_enabled()) {
    $sAudit->log(SA_SYS_MSG_CRON, t('Cron disabled'));
    return;
  }

  $currentTimestamp = \Drupal::time()->getRequestTime();

  // Setup service variables.
  $queue = \Drupal::service('queue')->get('sparx_process_system_messages');
  $connection = \Drupal::service('database');

  // Check for spamming.
  $nextSpamRunTimestamp = \Drupal::state()
    ->get('sparx_system_messages.next_spam_check_timestamp',
      SPARX_SYSTEM_MESSAGES_NEXT_SPAM_CHECK_TIMESTAMP_INIT);

  $sAudit->log(SA_SYS_MSG_CRON, t('Read next spam check as @timestamp',
    [
      '@timestamp' => $nextSpamRunTimestamp,
    ]
  ));

  // Only run if we are over the next time set time.
  if ($currentTimestamp >= $nextSpamRunTimestamp) {

    // We go back a certain length of time to check for spam messages.
    $spamStartTime = $currentTimestamp - SPARX_SYSTEM_MESSAGES_SPAM_CHECK_PAST_PERIOD;

    $sAudit->log(SA_SYS_MSG_CRON, t('Searching spam with at least @count records since @starttime', [
      '@count' => SPARX_SYSTEM_MESSAGES_SPAM_FREQUENCY_COUNT,
      '@starttime' => $spamStartTime,
    ]));

    // Find users that are above the threshold over this period.
    $users = $connection->query('SELECT uid, COUNT(*) AS message_cnt FROM sparxaudit WHERE type = :type AND uid IS NOT NULL AND timestamp > :timestamp GROUP BY uid HAVING COUNT(*) >= :count', [
      ':type' => SA_MSG_SEND_TRACK,
      ':timestamp' => $spamStartTime,
      ':count' => SPARX_SYSTEM_MESSAGES_SPAM_FREQUENCY_COUNT,
    ])->fetchAll();

    $numUsers = count($users);

    $sAudit->log(SA_SYS_MSG_CRON, t('Found @count users that received possible spam',
      [
        '@count' => $numUsers,
      ]
    ));

    // Log in the database for each user.
    foreach ($users as $user) {
      $sAudit->setUserId($user->uid);
      $sAudit->log(SA_SYS_MSG_QUEUE, t('Possible excess messaging, received @count messages',
        [
          '@count' => $user->message_cnt,
        ]
      ));
    }

    $sAudit->setUserId(NULL);

    if ($numUsers) {

      // Add the item to the queue for processing this finding.
      $item = [
        'type' => SPARX_QUEUE_SYSTEM_MESSAGE_SPAM,
      ];

      $sAudit->logMessageJson(SA_SYS_MSG_CRON, t('Adding item to queue'), $item);

      $queue->createItem($item);

      // Set next check after 24 hours.
      \Drupal::state()
        ->set('sparx_system_messages.next_spam_check_timestamp',
        $currentTimestamp + SPARX_SYSTEM_MESSAGES_SPAM_CHECK_PAST_PERIOD + 1);
    } else {
      // Set next check.
      \Drupal::state()
        ->set('sparx_system_messages.next_spam_check_timestamp',
        $currentTimestamp + SPARX_SYSTEM_MESSAGES_SPAM_CHECK_FREQUENCY);
    }

  }
  else {

    $sAudit->log(SA_SYS_MSG_CRON, t('Processing spam check not required yet'));
  }

  // Check for system issues.
  $nextSystemRunTimestamp = \Drupal::state()
    ->get('sparx_system_messages.next_system_check_timestamp',
      SPARX_SYSTEM_MESSAGES_NEXT_SYSTEM_CHECK_TIMESTAMP_INIT);

  $sAudit->log(SA_SYS_MSG_CRON, t('Read next system check as @timestamp',
    [
      '@timestamp' => $nextSystemRunTimestamp,
    ]
  ));

  // Only run if we are over the next time set time.
  if ($currentTimestamp >= $nextSystemRunTimestamp) {

    // Time we go back to when searching for records.
    $systemStartTime = $currentTimestamp - SPARX_SYSTEM_MESSAGES_FAILURE_CHECK_PAST_PERIOD;

    $sAudit->log(SA_SYS_MSG_CRON, t('Starting for system issues since @timestamp',
      [
        '@timestamp' => $systemStartTime,
      ]
    ));

    $errors = $connection->query('SELECT COUNT(*) AS message_cnt FROM sparxaudit WHERE type = :type AND timestamp > :timestamp', [
      ':type' => SA_SYS_FAIL_TRACK,
      ':timestamp' => $systemStartTime,
    ])->fetchAssoc();

    // Obtains "message_cnt" from SQL.
    $errorCount = reset($errors);

    $sAudit->log(SA_SYS_MSG_CRON, t('Found @count system issues since @timestamp',
      [
        '@count' => $errorCount,
        '@timestamp' => $systemStartTime,
      ]
    ));

    // Only process if we are higher than the frequency found.
    if ($errorCount >= SPARX_SYSTEM_MESSAGES_FAILURE_FREQUENCY_COUNT) {

      $sAudit->log(SA_SYS_MSG_CRON, t('Possible system issues'));

      // Add item to queue as found over threshold.
      $item = [
        'type' => SPARX_QUEUE_SYSTEM_MESSAGE_FAILURE,
      ];

      $sAudit->logMessageJson(SA_SYS_MSG_CRON, t('Adding item to queue'), $item);

      $queue->createItem($item);

      // Different time to check in the future because found an error.
      $futureTimestampDiff = SPARX_SYSTEM_MESSAGES_FAILURE_CHECK_ERROR_FREQUENCY;
    }
    else {
      // Set normal check time in the future.
      $futureTimestampDiff = SPARX_SYSTEM_MESSAGES_FAILURE_CHECK_FREQUENCY;
    }

    // Set the time to check in the state API.
    \Drupal::state()
      ->set('sparx_system_messages.next_system_check_timestamp',
        $currentTimestamp + $futureTimestampDiff);
  }
  else {

    $sAudit->log(SA_SYS_MSG_CRON, t('Processing system check not required yet'));
  }

  // Check for internal errors.
  $nextInternalRunTimestamp = \Drupal::state()
    ->get('sparx_system_messages.next_internal_check_timestamp',
      SPARX_SYSTEM_MESSAGES_NEXT_INTERNAL_CHECK_TIMESTAMP_INIT);

  $sAudit->log(SA_SYS_MSG_CRON, t('Read next internal check as @timestamp',
    [
      '@timestamp' => $nextInternalRunTimestamp,
    ]
  ));

  // Only run if we are over the next time set time.
  if ($currentTimestamp >= $nextInternalRunTimestamp) {

    // Time we go back to when searching for records.
    $internalStartTime = $currentTimestamp - SPARX_SYSTEM_MESSAGES_INTERNAL_CHECK_PAST_PERIOD;

    $sAudit->log(SA_SYS_MSG_CRON, t('Starting for system issues since @timestamp',
      [
        '@timestamp' => $internalStartTime,
      ]
    ));

    // Find the number of messages with internal errors.
    $errors = $connection->query('SELECT COUNT(*) AS message_cnt FROM sparxaudit WHERE type = :type AND timestamp > :timestamp', [
      ':type' => SA_USR_FAIL_TRACK,
      ':timestamp' => $internalStartTime,
    ])->fetchAssoc();

    // Obtains "message_cnt" from SQL.
    $errorCount = reset($errors);

    $sAudit->log(SA_SYS_MSG_CRON, t('Found @count internal issues since @timestamp',
      [
        '@count' => $errorCount,
        '@timestamp' => $internalStartTime,
      ]
    ));

    if ($errorCount > 0) {

      // Add item to queue as found records.
      $sAudit->log(SA_SYS_MSG_QUEUE, t('Possible application issues'));

      $item = [
        'type' => SPARX_QUEUE_SYSTEM_MESSAGE_INTERNAL,
      ];

      $sAudit->logMessageJson(SA_SYS_MSG_CRON, t('Adding item to queue'), $item);

      $queue->createItem($item);
    }

    // Setup time for next check.
    \Drupal::state()
      ->set('sparx_system_messages.next_system_check_timestamp',
        $currentTimestamp + SPARX_SYSTEM_MESSAGES_INTERNAL_CHECK_FREQUENCY);
  }
  else {

    $sAudit->log(SA_SYS_MSG_CRON, t('Processing system check not required yet'));
  }
}
