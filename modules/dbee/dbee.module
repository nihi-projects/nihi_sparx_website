<?php

/**
 * @file
 * Hook and main functions for the dbee module.
 */

use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Database\Query\AlterableInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\encrypt\Entity\EncryptionProfile;
use Drupal\Core\Access\AccessResult;

define('DBEE_ENCRYPT_NAME', 'dbee');
define('DBEE_PREV_ENCRYPT_NAME', 'dbee_prev');
define('DBEE_DEFAULT_KEY_NAME', 'dbee');
define('DBEE_PREV_KEY_NAME', 'dbee_prev');
define('DBEE_DEFAULT_ENCRYPTION_METHOD', 'real_aes');
define('DBEE_DEFAULT_KEY_FILENAME', 'dbee.key');
define('DBEE_DEFAULT_KEY_BYTES', 32);

/**
 * Test if the email value need to be altered.
 *
 * The dbee module will only encrypt valid emails addresses. Invalid datas will
 * be stored without encryption.
 *
 * @param string $email
 *   The user email value (mail, init).
 *
 * @return bool
 *   True if the data is a valid email address and should be encrypted.
 */
function dbee_email_to_alter($email) {
  $email = trim($email);
  return (!empty($email) && is_string($email) && \Drupal::service('email.validator')->isValid($email));
}

/**
 * Provide the stored database value of an email address.
 *
 * Returns the email encrypted value of an email. it is used, for example, on
 * operations inspecting or updating email values into the database user table.
 * It can provide either the encrypted value of the lowercase email address or
 * the encrypted value of the sensitive case email address. This function make
 * sure to not alter data if not needed, preserving informations. Always use it
 * for encrypting datas.
 *
 * @param string $string
 *   A string, the uncrypted (readable) email address to encrypt.
 *
 * @return string
 *   A string corresponding to the serialized encrypted email value (lowercase
 *   or sensitive case) or the data value if encryption is not needed.
 */
function dbee_encrypt($string) {
  if (dbee_email_to_alter($string)) {
    // This is a valid email address.
    $encryption_profile = EncryptionProfile::load(DBEE_ENCRYPT_NAME);
    $encrypted = utf8_encode(Drupal::service('encryption')->encrypt($string, $encryption_profile));
    // Make sure the encrypted value is correct, ie is not a valid email
    // address anymore.
    $decrypt = dbee_decrypt($encrypted);
    if (!dbee_email_to_alter($encrypted) && dbee_email_to_alter($decrypt)) {
      return $encrypted;
    }
  }
  // This data is empty is not a valid email address or is already encrypted
  // or encryption fails : do not alter data.
  return $string;
}

/**
 * Decrypts an email address if needed.
 *
 * Returns the uncrypted provided value or the exact provided value if not
 * encrypted or encryption fails. This function make sure to not alter data if
 * not needed, preserving informations. Always use it for decrypting datas.
 *
 * @param string $string
 *   A string, the encrypted (non readable) email address to decrypt.
 * @param bool $prev_encrypt
 *   A boolean, If TRUE, use a previous dbee encryption profile to decrypt,
 *   used on changing encryption settings. Default to FALSE.
 *
 * @return string
 *   A string corresponding to the email decrypted value or the provided value
 *   if decryption is not needed.
 */
function dbee_decrypt($string, $prev_encrypt = FALSE) {
  if (!dbee_email_to_alter($string)) {
    // This is not a valid email address.
    $encrypt_profile = (!$prev_encrypt) ? DBEE_ENCRYPT_NAME : DBEE_PREV_ENCRYPT_NAME;
    $encryption_profile = EncryptionProfile::load($encrypt_profile);
    $uncrypted_mail = utf8_encode(Drupal::service('encryption')->decrypt(utf8_decode($string), $encryption_profile));
    // Check if the decrypted email address is valid before returning it :
    if ($uncrypted_mail && dbee_email_to_alter($uncrypted_mail)) {
      // The decrypted value is a valid email address, return it.
      return $uncrypted_mail;
    }
  }
  // This email address is empty or is already decrypted or decryption fails :
  // do not alter data.
  return $string;
}

/**
 * Provide the encrypted mails values to store into the database.
 *
 * Provide the encrypted values for 2 fields of the {users_field_data} table
 * (the 'mail', 'init' fields).
 *
 * @param array $mails
 *   An array of values to encrypt, keys are 'mail' and 'init'.
 *
 * @return array
 *   An array, keyed by 'mail', 'init'. Values are the encrypted mails.
 */
function dbee_store(array $mails) {
  if (!is_array($mails)) {
    return [];
  }

  $to_store = $edited_value = [];
  $mail_edited_value = FALSE;
  foreach (['mail', 'init'] as $dbee_field) {
    // $edit value overwrites the $account value.
    $edited_value = $mails[$dbee_field];
    // For performance raisons, does not decrypt the value twice.
    // If mail and init are identical, return the same encryption value.
    $mail_init_equal = ($mail_edited_value !== FALSE && $edited_value == $mail_edited_value);
    // Here : store the encrypted values.
    $to_store[$dbee_field] = ($mail_edited_value === FALSE || !$mail_init_equal) ? dbee_encrypt($edited_value) : $to_store['mail'];
    // Save the 'mail' value in order to compare it to the 'init' value.
    $mail_edited_value = $edited_value;
  }
  return $to_store;
}

/**
 * Provide the uncrypted dbee mails values.
 *
 * Provide the encrypted values for 2 fields of the {users_field_data} table
 * (the 'mail', 'init' fields).
 *
 * @param array $encrypted
 *   An array of crypted mails, keys are 'mail', 'init'.
 * @param bool $prev_encrypt
 *   A boolean, If TRUE, use a previous dbee encryption profile to decrypt,
 *   used on changing encryption settings. Default to FALSE.
 *
 * @return array
 *   An array, keyed by 'mail', 'init'. Values are the encrypted mails.
 */
function dbee_unstore(array $encrypted, $prev_encrypt = FALSE) {
  if (!is_array($encrypted)) {
    return [];
  }

  $to_unstore = [];
  $mail_value = FALSE;
  foreach (['mail', 'init'] as $dbee_field) {
    $field_value = (!empty($encrypted[$dbee_field])) ? $encrypted[$dbee_field] : '';
    // For performance raisons, does not decrypt the value twice.
    $mail_init_equal = ($mail_value !== FALSE && $field_value == $mail_value);
    // Here : returns the uncrypted values.
    $to_unstore[$dbee_field] = ($mail_value === FALSE || !$mail_init_equal) ? dbee_decrypt($field_value, $prev_encrypt) : $to_unstore['mail'];
    // Save the 'mail' value in order to compare it to the 'init' value.
    $mail_value = $field_value;
  }
  return $to_unstore;
}

/**
 * Fix the encrypted mail and init values on an user object.
 *
 * Decrypt 'mail' and 'init' variables from the user object. Note that the user
 * object is passed by reference.
 *
 * @param object $account
 *   An user object, only $account->mail and $account->init will be used.
 */
function dbee_extract(&$account) {
  if (!empty($account->id())) {
    $crypted = [];
    foreach (['mail', 'init'] as $dbee_field) {
      if (!empty($account->$dbee_field->value)) {
        $crypted[$dbee_field] = $account->$dbee_field->value;
      }
    }
    if (!empty($crypted)) {
      $decrypted = dbee_unstore($crypted);
      foreach (['mail', 'init'] as $dbee_field) {
        if (!empty($decrypted[$dbee_field])) {
          // Apply decryption to the user entity.
          $account->$dbee_field->value = $decrypted[$dbee_field];
        }
      }
    }
  }
}

/**
 * Retrieve the {users_field_data} database encrypted values.
 *
 * By using a untagged query, the dbee_query_alter() function is ignored. So
 * this function returns non altered values from database.
 *
 * @param int $uid
 *   Integer, the user uid to retrieve. Default is NULL returning all users.
 * @param int $offset
 *   Integer, started result. Default is NULL.
 * @param int $limit
 *   Integer, the maximum number to return. Default is NULL, returning all rows.
 *
 * @return array
 *   An array, keys are the user ID (uid) value and values are an object with
 *   the following properties : 'uid', 'mail', 'init'.
 */
function dbee_stored_users($uid = NULL, $offset = NULL, $limit = NULL) {
  $query = \Drupal::database()->select('users_field_data', 'ufd')
    ->fields('ufd', ['uid', 'name', 'mail', 'init'])
    ->condition('ufd.uid', 0, '<>');

  if (!empty($uid) && is_numeric($uid) && $uid > 0) {
    $query->condition('ufd.uid', $uid, '=');
  }
  if (empty($offset)) {
    $offset = 0;
  }

  if ($offset > 0 || (!empty($limit) && is_numeric($limit) && $limit > 0)) {
    $query->range($offset, $limit);
  }
  $query->orderBy('ufd.uid');
  $users_objects = $query->execute();
  // Returns as a multidimensionnal array.
  $users = [];
  foreach ($users_objects as $record) {
    foreach (['uid', 'name', 'mail', 'init'] as $field) {
      $users[$record->uid][$field] = $record->$field;
    }
  }
  return $users;
}

/**
 * Implements hook_entity_load().
 *
 * Decrypted the mail and init addresses loading a user object.
 * hook_entity_load() is called before hook_ENTITY_TYPE_load().
 */
function dbee_entity_load(array $entities, $entity_type_id) {

  if ($entity_type_id == 'user') {
    // Decrypt email address when a user is loaded, it makes the email address
    // available for the system.
    foreach ($entities as $record) {
      dbee_extract($record);
    }
  }
}

/**
 * Implements hook_entity_presave().
 *
 * Encrypt the email address on saving a user account. hook_entity_presave()
 * is called after hook_ENTITY_TYPE_presave().
 */
function dbee_entity_presave(EntityInterface $entity) {

  $type = $entity->getEntityTypeId();
  if ($type == 'user') {
    // Set a variable containing uncrypted emails values to encrypt.
    $uncrypted = [];
    foreach (['mail', 'init'] as $dbee_field) {
      if (!empty($entity->$dbee_field->value)) {
        $uncrypted[$dbee_field] = $entity->$dbee_field->value;
      }
      else {
        $uncrypted[$dbee_field] = NULL;
      }
    }
    // Encrypt emails values.
    $to_update = dbee_store($uncrypted);
    // Replace uncrypted mails by encrypted emails value in the User object.
    foreach ($to_update as $field => $value) {
      // Mail and init.
      if ($value) {
        $entity->$field->value = $value;
      }
    }
  }
  elseif (!$entity->isNew() && (($type == 'key' && $entity->getOriginalId() == dbee_current_key_id()) || ($type == 'encryption_profile' && $entity->getOriginalId() == DBEE_ENCRYPT_NAME)) && dbee_entity_change($entity)) {
    // The encryption parameters are going to change.
    $set_prev_encrypt = FALSE;
    $set_prev_key = FALSE;
    $reencrypt_all = FALSE;
    if ($type == 'key' && $entity->getOriginalId() == dbee_current_key_id()) {
      if (!\Drupal::service('key.repository')->getKey(DBEE_PREV_KEY_NAME)) {
        $copy = $entity->original->createDuplicate();
        $copy->set('name', DBEE_PREV_KEY_NAME);
        $copy->set('id', DBEE_PREV_KEY_NAME);
        $copy->set('label', 'Previous DataBase Email Encryption key');
        $copy->set('description', 'Previous Dbee key for decrypting users email addresses.');
        if ($copy->save() && \Drupal::service('key.repository')->getKey(DBEE_PREV_KEY_NAME)) {
          \Drupal::logger('dbee')->info('Dbee key is going to change, previous key is saved.');
          $set_prev_encrypt = $set_prev_key = TRUE;
        }
        else {
          \Drupal::logger('dbee')->critical('Dbee key is going to change, and the previous key is not saved !');
        }
      }
      else {
        $set_prev_encrypt = TRUE;
      }
    }
    elseif ($type == 'encryption_profile' && $entity->getOriginalId() == DBEE_ENCRYPT_NAME) {
      $set_prev_encrypt = TRUE;
    }

    if ($set_prev_encrypt) {
      $dbee_encrypt = ($type != 'encryption_profile') ? EncryptionProfile::load(DBEE_ENCRYPT_NAME) : $entity->original;
      $prev_encrypt_profile = EncryptionProfile::load(DBEE_PREV_ENCRYPT_NAME);
      if ($dbee_encrypt && !$prev_encrypt_profile) {
        $copy = $dbee_encrypt->createDuplicate();
        $copy->set('name', DBEE_PREV_ENCRYPT_NAME);
        $copy->set('id', DBEE_PREV_ENCRYPT_NAME);
        $copy->set('label', "Previous Dbee {$dbee_encrypt->getEncryptionMethodId()}");
        if ($set_prev_key) {
          $copy->set('encryption_key', DBEE_PREV_KEY_NAME);
        }
        if ($copy->save() && EncryptionProfile::load(DBEE_PREV_ENCRYPT_NAME)) {
          \Drupal::logger('dbee')->info('Dbee key is going to change, encrypt profile saved with prev key values.');
          // Now Save the new settings, then decrypt all users email addresses,
          // then re-encrypt then with the new parameters.
          $reencrypt_all = TRUE;
        }
        else {
          \Drupal::logger('dbee')->critical('Dbee key is going to change, encrypt profile is not saved with prev key values. !');
        }
      }
      elseif ($dbee_encrypt && $prev_encrypt_profile) {
        $reencrypt_all = TRUE;
      }
    }

    if ($reencrypt_all) {
      // Wait for the save to be completed. Flag to re-encrypt.
      $entity->dbee_change = TRUE;
    }
  }
}

/**
 * Implements hook_entity_update().
 */
function dbee_entity_update(EntityInterface $entity) {

  if (($entity->getEntityTypeId() == 'key' || ($entity->getEntityTypeId() == 'encryption_profile' && $entity->getOriginalId() == DBEE_ENCRYPT_NAME)) && !empty($entity->dbee_change)) {
    // Batch for decrypting all users email addresses, then re-encrypt then
    // with the new parameters.
    module_load_include('inc', 'dbee', 'dbee.users');
    dbee_update_crypt_all('change', 'change', TRUE);
    dbee_update_crypt_all('encrypt', 'change', TRUE);
  }
}

/**
 * Implements hook_ENTITY_TYPE_update().
 *
 * Decrypt the emails ater saving.
 */
function dbee_user_update(EntityInterface $entity) {
  // Decrypt the emails ater saving : user object will provide uncrypted emails
  // values.
  dbee_extract($entity);
}

/**
 * Implements hook_ENTITY_TYPE_insert().
 *
 * Decrypt the emails ater saving.
 *
 * @see dbee_user_update()
 */
function dbee_user_insert(EntityInterface $entity) {
  dbee_user_update($entity);
}

/**
 * Implements hook_ENTITY_TYPE_view().
 *
 * This function display an explicit message on the user profile page,
 * concerning the email encryption. Displayed only for users with the
 * 'administer dbee' permission.
 */
function dbee_user_view(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display, $view_mode) {
  if ($display->getComponent('dbee')) {
    if (\Drupal::currentUser()->hasPermission('administer dbee')) {
      // Specific field, it explicits security about email address.
      // Use static query to bypass dbee_query_alter() function.
      $uid = $entity->id();
      $stored_account = dbee_stored_users($uid);
      $stored_mail = $stored_account[$uid]['mail'];

      $arg = ['%name' => $entity->getUsername()];
      // Handle output message about email address encryption.
      if ($stored_mail == '') {
        $email_status = t('No contact email address registered for %name.', $arg);
      }
      elseif (!\Drupal::service('email.validator')->isValid($stored_mail)) {
        $stored_mail_decrypted = dbee_decrypt($stored_mail);
        if ($stored_mail_decrypted != $stored_mail) {
          $email_status = t("The %name's contact email address is encrypted.", $arg);
        }
        else {
          $email_status = t("The %name's contact email address is encrypted but corrupted. Sending email to this user will fail.", $arg);
        }
      }
      else {
        $email_status = t("The %name's contact email address is not encrypted.", $arg);
      }

      // Display new field on the user profile page. Use same theming as
      // 'member_for', from user_user_view().
      $build['dbee'] = [
        '#type' => 'item',
        '#markup' => '<h4 class="label">' . t('Security') . '</h4> ' . $email_status,
      ];
    }
  }
}

/**
 * Implements hook_entity_extra_field_info().
 *
 * View extra field for encryption status.
 */
function dbee_entity_extra_field_info() {
  $fields['user']['user']['display']['dbee'] = [
    'label' => t('Email encryption'),
    'description' => t('Display the email encryption status.'),
    'weight' => 15,
    // member_for weight is 5, dbee will appears below to the member_for markup.
  ];

  return $fields;
}

/**
 * Implements hook_module_implements_alter().
 *
 * Change the hook order. The purpose is to improve the compatibility with
 * custom modules : call the dbee module on an early stage for decrypting and
 * on a ultimate stage for encrypting.
 */
function dbee_module_implements_alter(&$implementations, $hook) {

  // In order to improve compatibility with custom modules, set the decryption
  // on an early stage.
  $hook_decrypt = [
    'entity_load',
    'user_insert',
    'user_update',
  ];
  if (in_array($hook, $hook_decrypt) && isset($implementations['dbee'])) {
    $group = $implementations['dbee'];
    unset($implementations['dbee']);
    $implementations = array_merge(['dbee' => $group], $implementations);
  }

  // In order to improve compatibility with custom modules, set the encryption
  // on an ultimate stage.
  $hook_encrypt = [
    'entity_presave',
    'query_alter',
    'query_user_load_multiple_alter',
    'query_pager_alter',
    'entity_query_alter',
  ];
  if (in_array($hook, $hook_encrypt) && isset($implementations['dbee'])) {
    $group = $implementations['dbee'];
    unset($implementations['dbee']);
    $implementations['dbee'] = $group;
  }
}

/**
 * Implements hook_query_alter().
 *
 * Encrypt the email on queries. This hook is only called on dynamic tagged
 * queries. The 'dbee' tag is not required. As an example, the query from the
 * user_load_by_mail() function is going to be altered thanks to the
 * 'user_load_multiple' tag. On the contrary, the user_account_form_validate()
 * is not tagged, the hook_query_alter() function is not called, that why I
 * replace this function by the custom function
 * (dbee_user_account_form_validate()). The dbee_query_alter() is going to
 * encrypt any dynamic tagged query that contains the mail or init field into
 * the where clause. Acting on tags :
 * - if the query contains the tag 'dbee_disabled' no alteration will occure.
 * - if the query contains the tags 'dbee_mail_disable_insensitive_case' or
 *   'dbee_init_disable_insensitive_case' : the search will be sensitive case
 *   (it is not the default behavior on mysql, read code and issue below).
 * Tags added by the dbee module :
 * - 'dbee_mail' if the query contains users.mail field on where clause.
 * - 'dbee_init' if the query contains users.init field on where clause.
 */
function dbee_query_alter(AlterableInterface $query) {

  $users_alias = FALSE;
  $tables = &$query->getTables();
  foreach ($tables as $table_alias => $table_properties) {
    if ($table_properties['table'] == 'users_field_data') {
      // The users table is queryed.
      $users_alias = $table_properties['alias'];
      break;
    }
  }

  if ($users_alias) {
    // The {users_field_data} table is queried.
    $dbee_fields = ['mail', 'init'];
    foreach ($dbee_fields as $dbee_field) {
      $field_selected[$dbee_field] = FALSE;
      // Set a default mail alias :
      $dbee_alias[$dbee_field] = $dbee_field;
    }
    // Check if all fields are loaded.
    if (is_array($tables[$users_alias]) && array_key_exists('all_fields', $tables[$users_alias]) && $tables[$users_alias]['all_fields']) {
      foreach ($dbee_fields as $dbee_field) {
        $field_selected[$dbee_field] = TRUE;
        // Optional, the mail alias is the same as default but its value is
        // confirmed.
        $dbee_alias[$dbee_field] = $dbee_field;
      }
    }
    else {
      // Or at least the mail or init fields.
      $fields = &$query->getFields();
      foreach ($fields as $field_alias => $field_properties) {
        if ($field_properties['table'] == $users_alias) {
          foreach ($dbee_fields as $dbee_field) {
            if ($field_properties['field'] == $dbee_field) {
              $field_selected[$dbee_field] = TRUE;
              $dbee_alias[$dbee_field] = $field_properties['alias'];
            }
          }
        }
      }
    }

    foreach ($dbee_fields as $dbee_field) {
      if ($field_selected[$dbee_field]) {
        // The 'mail' or the 'init' field from the table 'users_field_data' is
        // queryed. The 'dbee_mail' or 'dbee_init' tags inform that the result
        // set will problably return encrypted mail values and should go
        // throught the dbee_decrypt() function !
        $query->addTag('dbee_' . $dbee_field);
      }
    }

    // Now take care of the WHERE clause.
    // The 'dbee_disabled' tag disable all dbee stuff on queries.
    if (empty($query->alterTags['dbee_disabled'])) {
      _dbee_where_clause($query, $users_alias, $dbee_alias);
    }

  }
}

/**
 * Helper reformating database queries regarding encryption.
 *
 * Regarding field, operator and value on the where clause, returns the
 * corresponding changed values. As an example : replace "WHERE mail LIKE
 * 'john@example.com'" by  "WHERE uid IN (2, 7)" or "WHERE uid = 3".
 */
function _dbee_where_clause(&$query, $users_alias, $dbee_alias) {
  // Now take care of the WHERE clause.
  $dbee_fields = ['mail', 'init'];
  $where = &$query->conditions();

  foreach ($where as $placeholder => $where_properties) {
    if (is_array($where_properties) && array_key_exists('field', $where_properties) && array_key_exists('value', $where_properties)) {
      if (is_object($where_properties['field'])) {
        // For nested conditions (db_or()). Used by user_search_execute().
        _dbee_where_clause($where[$placeholder]['field'], $users_alias, $dbee_alias);
      }
      elseif (is_string($where_properties['field'])) {
        // Used by the load_by_mail_user() core function.
        foreach ($dbee_fields as $dbee_field) {
          // Alias : 'users.mail' or 'mail' in most cases.
          $where_alias = _dbee_build_alias($dbee_field, 'users_field_data', $dbee_alias[$dbee_field], $users_alias);
          if (in_array($where_properties['field'], $where_alias)) {
            // The where clause does contain the mail or init fields !
            // First handle simple or multiple values.
            $where_strings = [];
            if (is_string($where_properties['value'])) {
              // The user_load_by_mail() core function needs the code below.
              $where_strings[] = $where_properties['value'];
            }
            elseif (is_array($where_properties['value'])) {
              $where_strings = $where_properties['value'];
            }
            else {
              // This case should never happen.
              continue;
            }

            if (count($where_strings) == 1 && empty($where_strings[0])) {
              continue;
            }

            // Initialisation :
            $need_rewrite = FALSE;
            $new_values = [];
            $new_field = FALSE;
            $operator = mb_strtoupper($where_properties['operator']);
            $where_match = in_array($operator, ['LIKE', '=', 'IN']);

            $insensitive_search = (in_array($operator, ['LIKE', 'NOT LIKE']));
            // Special behavior regarding issues #2324701 and #2616264
            // (https://www.drupal.org/node/2616264#comment-10576582)
            // Due to MySQL collation, queries are executed as insensitive case.
            // current issue for D8 : #2490294
            // Provide a tag to disabling this behavior, needed for testing or
            // for custom modules : dbee_mail_disable_insensitive_case and
            // dbee_init_disable_insensitive_case.
            if (empty($query->alterTags["dbee_{$dbee_field}_disable_insensitive_case"])) {
              $insensitive_search = 'TRUE';
            }

            // The where clause needs to decrypt all users on complex searchs
            // with wildcards ('_' and '%').
            // Look for unescaped wildcards :
            // find active '%' and '_'mysql wildcards.
            $pattern_wildcards = '[^\\\\]%|^%|[^\\\\]_|^_';
            // The custom method is not the default one. It is faster but less
            // powerfull because it will return the user only if the whole mail
            // string is provided.
            $wildcard_search = FALSE;
            if (in_array($operator, ['LIKE', 'NOT LIKE']) && preg_match("!$pattern_wildcards!", $where[$placeholder]['value']) === 1) {
              $wildcard_search = TRUE;
            }

            $no_encrypt_value = [];
            if (!$wildcard_search) {
              // Check if the value need encryption.
              foreach ($where_strings as $index => $string) {
                $no_encrypt_value[] = (!dbee_email_to_alter($string));
              }
              $no_encrypt_value[] = array_values(array_unique($no_encrypt_value));
              if (count($no_encrypt_value) == 1 && $no_encrypt_value[0]) {
                $need_rewrite = FALSE;
              }
            }

            if ($wildcard_search || TRUE) {
              $need_rewrite = TRUE;
              $result_uids = _dbee_where_mailfields2uid($where_strings, $dbee_field, $operator, $insensitive_search);
              $n_matching_user = count($result_uids);
              if ($n_matching_user == 0) {
                // No users match, returns "WHERE 0 = '1'".
                $new_field = 1;
                $new_values[0] = 0;
              }
              else {
                $new_field = $users_alias . '.uid';
                $new_values = $result_uids;
              }
            }

            if ($need_rewrite) {

              if (!$where_match && $new_field == $users_alias . '.uid') {
                // Add to ignore empty values values.
                // Retreive empty values.
                // @TODO fix depreciated db_or by orConditionGroup().
                $empty_users = \Drupal::database()->select('users_field_data', 'ufd')
                  ->fields('ufd', ['uid'])
                  ->condition(db_or()->condition("ufd.{$dbee_field}", '', '=')->isNull("ufd.{$dbee_field}"))
                  ->execute();
                foreach ($empty_users as $account) {
                  if (array_search($account->uid, $new_values) === FALSE) {
                    $new_values[] = $account->uid;
                  }
                }
              }

              // At last : edit the Where condition.
              if (count($new_values) == 1) {
                $where[$placeholder]['operator'] = ($where_match) ? '=' : '<>';
                $where[$placeholder]['value'] = $new_values[0];
              }
              else {
                $where[$placeholder]['operator'] = ($where_match) ? 'IN' : 'NOT IN';
                $where[$placeholder]['value'] = $new_values;
              }
              if (!empty($new_field)) {
                $where[$placeholder]['field'] = $new_field;
              }
            }
          }
        }
      }
    }
  }
}

/**
 * Provide corresponding user ID to a dabase WHERE clause, decrypting all users.
 *
 * Convert database wilcards to php regex patterns. Use cache if called several
 * times during the script.
 *
 * @param array $keys
 *   An array of research strings, using database wilcards ('%' and '_').
 * @param string $field
 *   A string, the field into the {users_field_data} table : can be 'mail'
 *   (default) or 'init'.
 * @param string $operator
 *   A string, the operator on the WHERE clause. Default is 'LIKE'.
 *
 * @return array
 *   An array, the user uids that match the keys (no index).
 */
function _dbee_where_mailfields2uid(array $keys, $field = 'mail', $operator = 'LIKE', $insensitive_search = FALSE) {

  if (empty($keys) || (!is_array($keys)) || !in_array($field, ['mail', 'init'])) {
    return [];
  }

  // Prepare the regex patterns corresponding to the key(s).
  $patterns = [];
  // Look for unescaped wildcards :
  // find active '%' mysql wildcard, replaced by php regex '.*'.
  $pattern_percent = '[^\\\\]%|^%';
  // Find active '_' mysql wildcard, replaced by php regex '.'.
  $pattern_underscore = '[^\\\\]_|^_';
  $pattern_wildcards = "$pattern_percent|$pattern_underscore";
  $like = in_array($operator, ['LIKE', 'NOT LIKE']);
  foreach ($keys as $key) {
    // Prepare the key.
    $php_value = ($like && preg_match("!^%!", $key) === 1) ? '#' : '#^';
    if ($like) {

      $split_value = preg_split("!($pattern_wildcards)!", $key, -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
      $index_max = count($split_value) - 1;
      foreach ($split_value as $index => $string) {
        if (mb_strlen($string) <= 2) {
          $substring = $string;
          $replace_by = '';
          if (preg_match("!([^\\\\])%|()^%!", $string, $previous_char) === 1) {
            $replace_by = ($index > 0 && $index < $index_max) ? '.*' : '';
            $substring = $previous_char[1];
          }
          elseif (preg_match("!([^\\\\])_|()^_!", $string, $previous_char) === 1) {
            $replace_by = '.';
            $substring = $previous_char[1];
          }
          // db_like() add addcslashes() fonction.
          $php_value .= quotemeta(stripcslashes($substring)) . $replace_by;
        }
        else {
          $php_value .= quotemeta(stripcslashes($string));
        }
      }
    }
    else {
      $php_value .= quotemeta($key);
    }
    if ($insensitive_search) {
      $php_value = mb_strtolower($php_value);
    }

    $php_value .= ($like && preg_match("!%$!", $key) === 1) ? '#' : '$#';
    $patterns[] = $php_value;
  }

  // Use static cache.
  static $users = [];
  // @TODO : reset cache on editing users.

  // Uncrypt all users, and save the result in a static variable.
  if (empty($users)) {
    $all_mails = dbee_stored_users();
    foreach ($all_mails as $uid => $crypted) {
      $decrypted_fields = dbee_unstore($crypted);
      $users[$uid] = $decrypted_fields;
    }
  }

  $result = [];
  foreach ($users as $uid => $datas) {
    $uncrypted_field = $datas[$field];
    if ($insensitive_search) {
      $uncrypted_field = mb_strtolower($uncrypted_field);
    }
    // Search the key(s).
    foreach ($patterns as $pattern) {
      // Like or Not like never return for null values.
      if (!empty($uncrypted_field) && preg_match($pattern, $uncrypted_field)) {
        // Avoid doublon thanks to the index.
        $result[$uid] = $uid;
      }
    }
  }
  return array_values($result);
}

/**
 * Provide list of possible mysql alias for a field in a table.
 *
 * Considere alias values for field and table, handling if the field alias
 * include the table name (case : $field_alias = "users.mail").
 *
 * @param string $field
 *   A string, the exact field name in the database (ex : "mail").
 * @param string $table
 *   A string, the table name in the database (ex : "drupal_user"). Optional,
 *   default is empty.
 * @param string $field_alias
 *   A string, the alias field name in the query (ex : "user_mail" or
 *   "user.mail"). Optional, default is empty.
 * @param string $table_alias
 *   A string, the alias table name in the query (ex : "user_table"). Optional,
 *   default is empty.
 *
 * @return array
 *   All the possible values that will match the field according to the query
 *   (ex : "mail", "user_mail", "user.mail", "user_table.mail",
 *   "user.user_mail", "user_table.user_mail").
 */
function _dbee_build_alias($field, $table = '', $field_alias = '', $table_alias = '') {
  // If there is no fied, there is no alias.
  if (empty($field)) {
    return [];
  }
  // Handle Field.
  $alias = [$field];

  // Handle fied alias.
  if (empty($field_alias)) {
    $field_alias = $field;
  }
  $alias[] = $field_alias;

  // If table is provided.
  if (!empty($table)) {
    $alias[] = "{$table}.{$field}";

    // Handle table alias.
    if (empty($table_alias)) {
      $table_alias = $table;
    }
    $alias[] = "{$table_alias}.{$field}";

    // Handle field alias with table.
    if (strpos($field_alias, '.') === FALSE) {
      // $field_alias does not contain the table.
      $alias[] = "{$table}.{$field_alias}";
      $alias[] = "{$table_alias}.{$field_alias}";
    }
  }

  // Format the result.
  $alias = array_unique($alias);
  $alias = array_values($alias);
  return $alias;
}

/**
 * Implements hook_entity_access().
 *
 * Prevent deleting dbee encrypt and key entities, Use the dbee module
 * permission for update.
 */
function dbee_entity_access(EntityInterface $entity, $operation, AccountInterface $account) {
  if (in_array($entity->getEntityTypeId(), ['encryption_profile', 'key'])) {
    $id = $entity->getOriginalId();
    $protected_encrypt_ids = [DBEE_ENCRYPT_NAME, DBEE_PREV_ENCRYPT_NAME];
    $protected_key_ids = [DBEE_DEFAULT_KEY_NAME, DBEE_PREV_KEY_NAME];
    $dbee_current_key_id = dbee_current_key_id();
    if (!in_array($dbee_current_key_id, $protected_key_ids)) {
      $protected_key_ids[] = $dbee_current_key_id;
    }
    if (($entity->getEntityTypeId() == 'encryption_profile' && in_array($id, $protected_encrypt_ids)) || ($entity->getEntityTypeId() == 'key' && in_array($id, $protected_key_ids))) {
      if ($operation == 'delete') {
        return AccessResult::forbidden();
      }
      elseif (in_array($operation, ['view', 'update'])) {
        return ($operation == 'update' && in_array($id, [DBEE_PREV_KEY_NAME, DBEE_PREV_KEY_NAME])) ? AccessResult::forbidden() : AccessResult::allowedIfHasPermission($account, 'administer dbee');
      }
    }
  }
}

/**
 * Provide de key name used by the dbee encryption profile.
 *
 * On install it is the dbee key but it can be changed manually by a user with
 * the 'administer dbee' permission.
 */
function dbee_current_key_id() {
  $dbee_current_key_id = FALSE;
  if ($dbee_encrypt = EncryptionProfile::load(DBEE_ENCRYPT_NAME)) {
    if (!empty($dbee_encrypt->getEncryptionKeyId()) && \Drupal::service('key.repository')->getKey($dbee_encrypt->getEncryptionKeyId())) {
      $dbee_current_key_id = $dbee_encrypt->getEncryptionKeyId();
    }
  }
  return $dbee_current_key_id;
}

/**
 * Returns TRUE if the entity as changed during presave.
 */
function dbee_entity_change($entity) {
  if (empty($entity->original)) {
    return TRUE;
  }
  $original = $entity->original;
  // Compare.
  $equal = TRUE;
  $ignore = ['original', 'submit', 'form_build_id', 'form_token', 'form_id'];
  $private_methods = [];
  if ($entity->getEntityTypeId() == 'encryption_profile') {
    $private_methods = [
      'getPluginCollections',
      'getEncryptionMethod',
      'getEncryptionMethodId',
      'getEncryptionKey',
      'getEncryptionKeyId',
    ];
  }
  elseif ($entity->getEntityTypeId() == 'key') {
    // @TODO : Unfornatelly, each method return the private property
    // 'stringTranslation' to the $entity that is not present in $original.
    // As a consequence, comparaison of similar keys will fail, returning as
    // different. That as no consequences on data, it simply fire
    // reincrypting on updating key with no change (consume ressources).
    $private_methods = [
      'getKeyType',
      'getKeyProvider',
      'getKeyInput',
      'getKeyValue',
      'getKeyValues',
    ];
  }
  $diff = [];
  foreach ($entity as $key => $value) {
    if (!in_array($key, $ignore) && $entity->$key != $original->$key) {
      $equal = FALSE;
      $diff[$key]['old'] = $original->$key;
      $diff[$key]['new'] = $entity->$key;
    }
  }
  foreach ($private_methods as $method) {
    if ($entity->$method() != $original->$method()) {
      $equal = FALSE;
      $diff[$method]['old'] = $original->$method();
      $diff[$method]['new'] = $entity->$method();
    }
  }
  return (!$equal);
}

/**
 * Implements hook_entity_type_alter().
 */
function dbee_entity_type_alter(array &$entity_types) {
  if (isset($entity_types['user'])) {
    $entity_types['user']->setClass('Drupal\dbee\Entity\DbeeUser');
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function dbee_form_system_modules_uninstall_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state, $form_id) {
  $form_state->loadInclude('dbee', 'inc', 'dbee.update');
  $form['#validate'][] = 'dbee_prevent_uninstall_validate';
}
