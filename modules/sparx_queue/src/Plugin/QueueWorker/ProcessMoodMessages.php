<?php

namespace Drupal\sparx_queue\Plugin\QueueWorker;

use Drupal\Core\Database\Connection;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Mail\MailManagerInterface;

use Drupal\sparx_library\SparxUser;
use Drupal\sparx_library\SparxContent;
use Drupal\sparx_library\SparxAudit;
use Drupal\sparx_library\SparxValidation;

use Drupal\sparx_library\Traits\SparxAuditTrait;
use Drupal\sparx_library\Traits\SparxSettingsTrait;

/**
 * A worker plugin that processes mood messages.
 *
 * @QueueWorker(
 *   id = "sparx_process_mood_messages",
 *   title = @Translation("SPARX Process Mood Messages"),
 *   cron = {"time" = 10}
 * )
 */
class ProcessMoodMessages extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  use SparxAuditTrait;
  use SparxSettingsTrait;

  protected $database;
  protected $sContent;
  protected $mailManager;
  protected $sUser;
  protected $moodEmailContent;
  protected $moodSMSContent;
  protected $sAudit;
  protected $sValid;

  /**
   * Constructs a ProcessMoodMessages worker.
   *
   * @param array $configuration
   *   Configuration.
   * @param string $plugin_id
   *   Plugin Id.
   * @param mixed $plugin_definition
   *   Plugin definition.
   * @param \Drupal\Core\Database\Connection $database
   *   Database object.
   * @param \Drupal\Core\Mail\MailManagerInterface $mailManager
   *   Mail object.
   * @param \Drupal\sparx_library\SparxContent $sparxContent
   *   Sparx content service.
   * @param \Drupal\sparx_library\SparxAudit $sparxAudit
   *   Sparx audit service.
   * @param \Drupal\sparx_library\SparxUser $sparxUser
   *   Sparx user service.
   * @param \Drupal\sparx_library\SparxValidation $sparxValidation
   *   Sparx validation service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Connection $database, MailManagerInterface $mailManager, SparxContent $sparxContent, SparxAudit $sparxAudit, SparxUser $sparxUser, SparxValidation $sparxValidation) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->database = $database;
    $this->mailManager = $mailManager;
    $this->sContent = $sparxContent;
    $this->sAudit = $sparxAudit;
    $this->sValid = $sparxValidation;
    $this->sUser = $sparxUser;

    // Preload mood content.
    $this->moodEmailContent = $this->sContent->getMoodEmail();
    $this->moodSMSContent = $this->sContent->getMoodTxt();
  }

  /**
   * Creates this class.
   *
   * @return mixed
   *   Container for this class.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('database'),
      $container->get('plugin.manager.mail'),
      $container->get('sparx_content_service'),
      $container->get('sparx_audit_service'),
      $container->get('sparx_user_service'),
      $container->get('sparx_validation_service')
    );
  }

  /**
   * Checks to see if item data passes sending rules.
   *
   * @param array $data
   *   The item data, "Q9" is the last question answer.
   *   "Score" is the total score for all the questions.
   *
   * @return bool
   *   TRUE if passes rules, FALSE if not.
   */
  private function passesMoodSendingRules(array $data) {

    return (($data['Q9'] > 0) || ($data['score'] > 14));
  }

  /**
   * Processes the item on the mood queue.
   *
   * @param mixed $data
   *   An array with the "Q9", "score", and "username" set.
   */
  public function processItem($data) {

    // Safety checking for sending email.
    // Needs to be defined, if it is set it will send email to that email.
    if ($this->checkEmailSmsOverrides()) {
      $this->aLog(SA_MOOD_MSG_QUEUE, t('Skipping item due to overrides not set'));
      return;
    }

    $this->aLogMessageJson(SA_MOOD_MSG_QUEUE, 'Processing item', $data);
    $this->aSetUsername($data['username']);

    // Only process if item passes sending rules.
    if ($this->passesMoodSendingRules($data)) {
      $this->aLog(SA_MOOD_MSG_QUEUE, t('Passes sending rules'));

      // Load the user in the queue item.
      $sUser = $this->sUser->getByUsername($data['username']);
      if (is_null($sUser) || is_null($sUser->getId())) {
        $this->aLog(SA_MOOD_MSG_QUEUE, t('Unable to load user'));
      }
      // We skip the user if they are disabled.
      // As they could be disabled and subsequently sent a message.
      elseif (!$sUser->isActive()) {
        // Set note that skipping this user.
        $this->aSetUser($sUser->getId(), $sUser->getUsername());
        $this->aLog(SA_MOOD_MSG_QUEUE, t('Skipping processing of user as is disabled'));
        $this->aResetUser();
      }
      else {

        // Whether we are allowed to send something to the user (preferences).
        $bAllowedToSendMessage = FALSE;

        // Set user ID for logger.
        $this->aSetUserId($sUser->getId());

        if (!$this->sValid->validateEmail($sUser->getEmail())) {

          // Detect invalid email addresses in database.
          $this->aLog(SA_USR_FAIL_TRACK, t("ProcessMoodMessages - User loaded with invalid email address"));
        }
        // Send email to user if permitted.
        elseif ($sUser->getEmailAllow()) {

          // Whether we are allowed to send something to the user (preferences).
          $bAllowedToSendMessage = TRUE;

          // Used to detect whether we could be sending too many messages.
          $this->aLog(SA_MSG_SEND_TRACK, t('Attempt to send mood email message'));

          $this->aLog(SA_MOOD_MSG_QUEUE, t('Sending mail'));

          if ($this->isEmailDisabledFlag()) {

            $this->aLog(SA_MOOD_MSG_QUEUE, t('Not sending mail as sending automatic emails are disabled'));

          }
          else {

            // Send mood mail to user.
            $this->mailManager->mail('sparx_email',
              'sparx_template',
              $sUser->getUsername() . ' <' . $this->getEmailWithOverride($sUser->getEmail()) . '>',
              SPARX_CFG_DEFAULT_LANGUAGE,
              [
                'username' => $sUser->getUsername(),
                'content' => $this->moodEmailContent,
              ]);
          }
        }
        else {
          $this->aLog(SA_MOOD_MSG_QUEUE, t('Not sending mail as user has requested not to be sent emails'));
        }

        // Send sms to user if permitted.
        if ($sUser->getTxtAllow() && strlen($sUser->getMobile())) {

          // Whether we are allowed to send something to the user (preferences).
          $bAllowedToSendMessage = TRUE;

          // Used to detect whether we could be sending too many messages.
          $this->aLog(SA_MSG_SEND_TRACK, t('Attempt to send mood SMS message'));

          $this->aLog(SA_MOOD_MSG_QUEUE, t('Processing sending of SMS'));

          // Send mood SMS.
          if ($sUser->sendSms($this->moodSMSContent)) {
            $sentStatus = t('SMS sent');
          }
          else {
            $sentStatus = t('SMS not sent');
          }
          $this->aLog(SA_MOOD_MSG_QUEUE, $sentStatus);
        }
        else {
          $this->aLog(SA_MOOD_MSG_QUEUE, t("Not sending SMS as user has requested not to be sent SMS's"));
        }

        if (!$bAllowedToSendMessage) {
          // Used to detect whether we could be sending too many messages.
          $this->aLog(SA_MSG_SEND_TRACK, t('Could not send mood message as user has requested neither email or SMS'));
        }
      }
    }
    else {
      $this->aLog(SA_MOOD_MSG_QUEUE, t('Does not pass sending rules'));
    }

    // Reset username and ID stream.
    $this->aResetUser();
  }

}
