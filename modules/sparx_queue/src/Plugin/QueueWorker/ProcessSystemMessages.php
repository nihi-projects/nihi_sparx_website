<?php

namespace Drupal\sparx_queue\Plugin\QueueWorker;

use Drupal\Core\Database\Connection;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Mail\MailManagerInterface;

use Drupal\sparx_library\SparxContent;
use Drupal\sparx_library\SparxAudit;

use Drupal\sparx_library\Traits\SparxAuditTrait;
use Drupal\sparx_library\Traits\SparxSettingsTrait;

/**
 * A worker plugin that processes system messages.
 *
 * @QueueWorker(
 *   id = "sparx_process_system_messages",
 *   title = @Translation("SPARX Process System Messages"),
 *   cron = {"time" = 10}
 * )
 */
class ProcessSystemMessages extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  use SparxAuditTrait;
  use SparxSettingsTrait;

  protected $database;
  protected $sContent;
  protected $mailManager;
  protected $sUser;
  protected $sAudit;

  /**
   * Constructs a ProcessSystemMessages worker.
   *
   * @param array $configuration
   *   Configuration.
   * @param string $plugin_id
   *   Plugin Id.
   * @param mixed $plugin_definition
   *   Plugin definition.
   * @param \Drupal\Core\Database\Connection $database
   *   Database object.
   * @param \Drupal\Core\Mail\MailManagerInterface $mailManager
   *   Mail object.
   * @param \Drupal\sparx_library\SparxContent $sparxContent
   *   Sparx content service.
   * @param \Drupal\sparx_library\SparxAudit $sparxAudit
   *   Sparx audit service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Connection $database, MailManagerInterface $mailManager, SparxContent $sparxContent, SparxAudit $sparxAudit) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->database = $database;
    $this->mailManager = $mailManager;
    $this->sContent = $sparxContent;
    $this->sAudit = $sparxAudit;
  }

  /**
   * Creates this class.
   *
   * @return mixed
   *   Container for this class.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('database'),
      $container->get('plugin.manager.mail'),
      $container->get('sparx_content_service'),
      $container->get('sparx_audit_service')
    );
  }

  /**
   * Sends alert email (simple template).
   *
   * @param string $content
   *   The content to have in the email.
   * @param string $email
   *   The email address to send to.
   */
  private function sendAlertEmail($content, $email) {

    if ($this->isEmailDisabledFlag()) {

      $this->aLog(SA_SYS_MSG_QUEUE, t('Not sending mail as sending automatic emails are disabled'));

    }
    else {

      $this->mailManager->mail('sparx_email',
        'sparx_plain',
        $email,
        SPARX_CFG_DEFAULT_LANGUAGE,
        [
          'content' => $content,
        ]
          );
    }
  }

  /**
   * Processes the item on the system queue.
   *
   * @param mixed $data
   *   An array with the "type" set.
   */
  public function processItem($data) {

    // Safety checking for sending email (global override).
    if ($this->checkEmailSmsOverrides()) {
      $this->aLog(SA_SYS_MSG_QUEUE, t('Skipping item due to overrides not set'));
      return;
    }

    // Checking for sending email and check is defined for system alert emails.
    if ($this->checkAlertSysEmail()) {
      $this->aLog(SA_SYS_MSG_QUEUE, t('Skipping due to alert system email not set'));
      return;
    }

    // Checking for sending email and check is defined for app alert emails.
    if ($this->checkAlertAppEmail()) {
      $this->aLog(SA_SYS_MSG_QUEUE, t('Skipping due to alert app email not set'));
      return;
    }

    $this->aLogMessageJson(SA_SYS_MSG_QUEUE, t('Processing item'), $data);

    // Check the type of the message.
    switch ($data['type']) {

      // Alert for spam.
      case SPARX_QUEUE_SYSTEM_MESSAGE_SPAM:

        $this->aLog(SA_SYS_MSG_QUEUE, t('Sending spam email alert'));
        $this->sendAlertEmail($this->sContent->getSpamSystemMessage(), $this->getSysAlertEmailWithOverride());
        break;

      // Alert for system messages (e.g. API failures).
      case SPARX_QUEUE_SYSTEM_MESSAGE_FAILURE:

        $this->aLog(SA_SYS_MSG_QUEUE, t('Sending system email alert'));
        $this->sendAlertEmail($this->sContent->getFailureSystemMessage(), $this->getSysAlertEmailWithOverride());
        break;

      // Alert for internal application errors.
      case SPARX_QUEUE_SYSTEM_MESSAGE_INTERNAL:

        $this->aLog(SA_SYS_MSG_QUEUE, t('Sending internal error email alert'));
        $this->sendAlertEmail($this->sContent->getFailureInternalMessage(), $this->getAppAlertEmailWithOverride());
        break;

      default:
        $this->aLog(SA_USR_FAIL_TRACK, t('ProcessSystemMessages - Message type not detected'));
        break;
    }
  }

}
