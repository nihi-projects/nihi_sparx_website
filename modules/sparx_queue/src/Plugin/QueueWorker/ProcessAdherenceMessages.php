<?php

namespace Drupal\sparx_queue\Plugin\QueueWorker;

use Drupal\Core\Database\Connection;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\sparx_library\SparxValidation;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Mail\MailManagerInterface;

use Drupal\sparx_library\SparxUser;
use Drupal\sparx_library\SparxContent;
use Drupal\sparx_library\SparxAudit;

use Drupal\sparx_library\Traits\SparxAuditTrait;
use Drupal\sparx_library\Traits\SparxSettingsTrait;

/**
 * A worker plugin that processes adherence messages.
 *
 * @QueueWorker(
 *   id = "sparx_process_adherence_messages",
 *   title = @Translation("SPARX Process Adherence Messages"),
 *   cron = {"time" = 10}
 * )
 */
class ProcessAdherenceMessages extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  use SparxAuditTrait;
  use SparxSettingsTrait;

  protected $database;
  protected $sContent;
  protected $mailManager;
  protected $sUser;
  protected $sAudit;
  protected $sValid;

  protected $adherenceEmailContent;
  protected $adherenceSMSContent;

  /**
   * Constructs a ProcessAdherenceMessages worker.
   *
   * @param array $configuration
   *   Configuration.
   * @param string $plugin_id
   *   Plugin Id.
   * @param mixed $plugin_definition
   *   Plugin definition.
   * @param \Drupal\Core\Database\Connection $database
   *   Database object.
   * @param \Drupal\Core\Mail\MailManagerInterface $mailManager
   *   Mail object.
   * @param \Drupal\sparx_library\SparxContent $sparxContent
   *   Sparx content service.
   * @param \Drupal\sparx_library\SparxAudit $sparxAudit
   *   Sparx audit service.
   * @param \Drupal\sparx_library\SparxUser $sparxUser
   *   Sparx user service.
   * @param \Drupal\sparx_library\SparxValidation $sparxValidation
   *   Sparx validation service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Connection $database, MailManagerInterface $mailManager, SparxContent $sparxContent, SparxAudit $sparxAudit, SparxUser $sparxUser, SparxValidation $sparxValidation) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->database = $database;
    $this->mailManager = $mailManager;
    $this->sContent = $sparxContent;
    $this->sAudit = $sparxAudit;
    $this->sUser = $sparxUser;
    $this->sValid = $sparxValidation;

    // Load up content from content service.
    $periods = $this->sContent->getAdherencePeriods();
    $this->adherenceEmailContent = $this->adherenceSMSContent = [];
    foreach ($periods as $period) {
      $this->adherenceEmailContent[$period] = $this->sContent->getAdherenceEmail($period);
      $this->adherenceSMSContent[$period] = $this->sContent->getAdherenceTxt($period);
    }
  }

  /**
   * Creates this class.
   *
   * @return mixed
   *   Container for this class.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('database'),
      $container->get('plugin.manager.mail'),
      $container->get('sparx_content_service'),
      $container->get('sparx_audit_service'),
      $container->get('sparx_user_service'),
      $container->get('sparx_validation_service')
    );
  }

  /**
   * Processes the item on the adherence queue.
   *
   * @param mixed $data
   *   An array with the "uid" and "period" set.
   */
  public function processItem($data) {

    // Safety checking for sending email.
    // Needs to be defined, if it is set it will send email to that email.
    if ($this->checkEmailSmsOverrides()) {
      $this->aLog(SA_ADH_MSG_QUEUE, t('Skipping item due to overrides not set'));
      return;
    }

    $this->aLogMessageJson(SA_ADH_MSG_QUEUE, 'Processing adherence message item', $data);

    // Load the user in the queue item.
    $sUser = $this->sUser->getById($data['uid']);
    if (is_null($sUser) || is_null($sUser->getId())) {
      $this->aLog(SA_ADH_MSG_QUEUE, t('Unable to load user'));
    }
    // We skip the user if they are disabled.
    // As they could be disabled and subsequently sent a message.
    elseif (!$sUser->isActive()) {
      // Set note that skipping this user.
      $this->aSetUser($sUser->getId(), $sUser->getUsername());
      $this->aLog(SA_ADH_MSG_QUEUE, t('Skipping processing of user as is disabled'));
      $this->aResetUser();
    }
    else {
      // Set user ID for logger.
      $this->aSetUser($sUser->getId(), $sUser->getUsername());

      // Whether we are allowed to send something to the user (preferences).
      $bAllowedToSendMessage = FALSE;

      if (!$this->sValid->validateEmail($sUser->getEmail())) {
        // Detect invalid email addresses in database.
        $this->aLog(SA_USR_FAIL_TRACK, t("ProcessAdherenceMessages - User loaded with invalid email address"));
      }
      // Send email to user if permitted.
      elseif ($sUser->getEmailAllow()) {
        // Allowed at least email.
        $bAllowedToSendMessage = TRUE;

        // This is used to detect whether we could be sending too many messages.
        $this->aLog(SA_MSG_SEND_TRACK, t('Attempt to send adherence email message for period @period',
          [
            '@period' => $data['period'],
          ]
        ));

        $this->aLog(SA_ADH_MSG_QUEUE, t('Sending mail'));

        if ($this->isEmailDisabledFlag()) {

          $this->aLog(SA_ADH_MSG_QUEUE, t('Not sending mail as sending automatic emails are disabled'));

        }
        else {

          $this->mailManager->mail('sparx_email',
            'sparx_template',
            $sUser->getUsername() . ' <' . $this->getEmailWithOverride($sUser->getEmail()) . '>',
            SPARX_CFG_DEFAULT_LANGUAGE,
            [
              'username' => $sUser->getUsername(),
              'content' => $this->adherenceEmailContent[$data['period']],
            ]);
        }
      }
      else {
        $this->aLog(SA_ADH_MSG_QUEUE, t('Not sending mail as user has requested not to be sent emails'));
      }

      // Send sms to user if permitted.
      if ($sUser->getTxtAllow() && strlen($sUser->getMobile())) {
        // Allowed at least SMS.
        $bAllowedToSendMessage = TRUE;

        // This is used to detect whether we could be sending too many messages.
        $this->aLog(SA_MSG_SEND_TRACK, t('Attempt to send adherence SMS message for period @period',
          [
            '@period' => $data['period'],
          ]
        ));

        $this->aLog(SA_ADH_MSG_QUEUE, t('Processing sending of SMS'));

        // Send SMS for the period.
        if ($sUser->sendSms($this->adherenceSMSContent[$data['period']])) {
          $sentStatus = t('SMS sent');
        }
        else {
          $sentStatus = t('SMS not sent');
        }

        $this->aLog(SA_ADH_MSG_QUEUE, $sentStatus);
      }
      else {
        $this->aLog(SA_ADH_MSG_QUEUE, t("Not sending SMS as user has requested not to be sent SMS's"));
      }
    }

    if (!$bAllowedToSendMessage) {
      // This is used to detect whether we could be sending too many messages.
      $this->aLog(SA_MSG_SEND_TRACK, t('Could not send adherence message for period @period as user has requested neither email or SMS', ['@period' => $data['period']]));
    }

    // Reset username and ID stream.
    $this->aResetUser();
  }

}
