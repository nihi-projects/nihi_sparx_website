<?php

namespace Drupal\sparx_popup\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'Message for International Visitors' block.
 *
 * @Block(
 *   id = "sparx_international_visitors_popup",
 *   admin_label = @Translation("SPARX International Visitors Popup")
 * )
 */
class InternationalPopupConfigurableTextBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'block_display_string' => $this->t('Support for registering on this website is unavailable in your region, please contact us.'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['block_display_string_text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Block contents'),
      '#description' => $this->t('This text will appear in the international popup block.'),
      '#default_value' => $this->configuration['block_display_string'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['block_display_string']
      = $form_state->getValue('block_display_string_text');
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $sValid = \Drupal::service('sparx_validation_service');

    if ($sValid->isIpDisabled()) {
      return [
        '#markup' => $this->configuration['block_display_string'],
      ];
    }
    else {
      return [];
    }
  }

}
